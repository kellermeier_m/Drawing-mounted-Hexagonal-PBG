// Library of helper functions written for the laser writing process 
// 06.07.2017
// Max Kellermeier



unit helperTLFS;
{$mode objfpc}{$H+}

interface
uses sysutils; 

Type
    PMAC = class
    public
        constructor create();
        procedure Add(output: AnsiString);
        procedure MotionStyle(b1, b2 : boolean);
    end;
    
var
  { global variables}
  x_l : double;
  y_l : double;
  z_l : double;
  pol_l : integer;
  status_l : integer;
  Z_correct : double;
  DEBUG : boolean;
  PMAC_Buffer : PMAC;

    {--- from 'Cylinder-w-circles.pas' ---}
    procedure mrel(x,y,z: double);
    procedure LOn(dummy : double);
    procedure LOff(dummy : double);
    procedure Line(x,y,z : double);
    procedure RepeatWhile(n : integer);
    procedure EndRepeat();
    procedure ChangePol(angle: integer);
    
implementation
procedure outputState();
begin
    Writeln( format('%f, %f, %f, %d, %d', [x_l, y_l, z_l, status_l, pol_l]) );
end;
{---- begin dummy prodecures ----}
procedure mrel(x,y,z: double);
begin
    IF DEBUG then
    begin
        Writeln('Move laser beam ...');
        Writeln( format('From: ( %f, %f, %f )', [x_l, y_l, z_l]) );
    end;
    
    x_l := x_l + x;
    y_l := y_l + y;
    z_l := z_l + z;
    If DEBUG then
        Writeln( format('To: ( %f, %f, %f )', [x_l, y_l, z_l]) )
    else
        outputState();
end;  
procedure LOn(dummy : double);
begin
    status_l := 1;
    If DEBUG then
        Writeln('Laser switched on. Start writing ...')
    else 
        outputState();
end;
procedure LOff(dummy : double);
begin
    status_l := 0;
    If DEBUG then
        Writeln('Laser switched off.')
    else
        outputState();
end;
procedure Line(x,y,z: double);
begin
    mrel(x,y,z);
end;
procedure RepeatWhile(n : integer);
begin
    Writeln('Start REPEAT for '+IntToStr(n)+' times');
end;
procedure EndRepeat();
begin
    Writeln('End REPEAT');
end;
procedure ChangePol(angle: integer);
begin
    pol_l := angle;
end;
{---- end dummy prodecures ----}

{ Class PMAC_Buffer }

{ PMAC_Buffer = class(TObject) }
       { private }
       { public }
          { procedure Add(); }
       { end; }
       
procedure PMAC.Add(output : AnsiString);
begin
    Writeln(output);
end;       
constructor PMAC.create();
begin
end;
procedure PMAC.MotionStyle(b1, b2 : boolean);
begin
end;


end.
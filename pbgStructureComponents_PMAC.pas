// Library of functions written for the laser writing process 
// 06.07.2017
// Max Kellermeier
// TODO when copying: use default argument

unit pbgStructureComponents_PMAC;

interface
uses math, helperTLFS, sysutils;          // exponent operator **

    {--- from 'Cylinder-w-circles.pas' ---}
    { function step_angle_ellipse(current_pos, a, b, radius, step : double ): double; }
    { procedure Circle_in_plane(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; startFromTop : boolean = False); }
    { procedure Circle_in_plane(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; startFromTop : boolean; initialLOn : boolean); }
    procedure Circle_in_plane(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; initialLOn : boolean );
    procedure cylinder(length, radius, a_halfaxis, halfaxis_b : double; angular_res,  y_resolution : integer);
    {---}
    {--- from 'Mounted_hexagonal_wvg_w_circles.pas' ---}
    procedure draw_Hexagonal_Wvg(lattice_const, radius : double; num_cells: integer; halfaxis_a, halfaxis_b: double; angular_resolution: integer);
    { temporary helper function, without mounting}
    procedure hexagon_of_cylinders(length, lattice_const, radius, a_halfaxis, halfaxis_b : double; num_cells: integer;angular_res,  y_resolution : integer);
    {no finished yet: }
    { procedure draw_Mounted_Hexagonal_Wvg(lattice_const, radius: double; num_cells : integer; halfaxis_a, halfaxis_b, mounting_dist, mounting_thickness: double; num_mountings, angular_res, y_resolution: integer);               }
    {---}
    {--- new function for circular hole ---}
    function step_angle_ellipse_for_hole(current_pos, a, b, radius, step : double ): double;
    { procedure Circular_Hole_in_plane(radius, halfaxis_a, halfaxis_b : double; angular_res: integer); }
    procedure Circular_Hole_in_plane(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; initialLOn : boolean);
    
    {---Planes---}
    { procedure horizontalPlane(width_x, width_y : double; resolution_x: integer); }
    procedure horizontalPlane(width_x, width_y : double; resolution_x: integer; sign_x_dir : integer);
    procedure horizontalPlane_w_QVars(width_x, width_y : AnsiString; resolution_x: integer; sign_x_dir : AnsiString);
    procedure verticalPlane(length, height : double; orientationX, orientationY: double; vertical_res: integer);
    
    {--- Procedures with y direction move first and afterwards stepping over xz-plane}
    procedure cylinder2(length, radius, halfaxis_a, halfaxis_b : double;
    angular_res: integer);
    procedure halfcylinder(length, radius, halfaxis_a, halfaxis_b : double; angular_res : integer; circleSide : integer);
    procedure halfCylinder_in_plane_w_bulk_etching(length: double; lattice_const, radius : double; halfaxis_a, halfaxis_b: double; angular_resolution: integer; plane_resolution : integer; sign_x_dir: integer);
    procedure Cylindrical_Hole_in_plane_filled(length, radius, halfaxis_a, halfaxis_b : double; angular_res, plane_res: integer);
    
    procedure bulk_etching_between_bottom_half_cylinders(length: double; lattice_const, radius : double; halfaxis_a, halfaxis_b: double; angular_resolution: integer; plane_resolution : integer; sign_x_dir: integer; location : integer);
    
implementation

{ Draw a pilar in a direction perpendicular to the laser }
{ Added 15/06/2017 / Project Max Kellermeier }

{
    Function for writing a circle oriented with its normal vector perpendicular to the writing axis. This means the laser direction is "in plane" w.r.t. the written circle.
    The writing axis is the direction of the incoming laser beam, denoted as z here.
    Initially, the laser's focus has to be positioned on the bottom of the resulting circle. 
     It will start writting following the circle's half arc.
    REMARK: The original function started from the top and repositioned to the bottom as a first step. This is also included in the pascal version of this function as an option via a parameter, but is droped in the PMAC version here.
    
    Parameters:
        radius: Radius of the resulting circle
        halfaxis_a: size of ellipsoidal laser beam in transverse plane (x and y components)
        halfaxis_b: size of ellipsoidal laser beam in longitudinal direction (z component)
        angular_res: resolution in angular direction -> Steps in x-z-plane
        initialLOn: Describes whether the Laser is on or off when starting the function. The function takes care of switching on and off the laser. The final status of the laser is equal to the initial one.
       
        
    Since no correction for the discreteness of the steps is considered the effective radius may be larger (average of distance from the center point to the surface)
}
{ procedure Circle_in_plane(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; startFromTop : boolean = False); }
procedure Circle_in_plane(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; initialLOn : boolean );
var
    { local declarations}
    { M_x : double;      // center coordinates of the ellipse  }
    { M_z : double; }
    angular_step : double;  //
    cur_angle : double;
    i : integer;    // loop variable

begin
    {procedure body}
    // Logic structure
    // (1. place ellipse at bottom, so at (0, -2)*radius )
    // 2. Switch on laser
    // 3. step along the full circular arc counterclockwise
    // 4. repeat steps from [-pi/2, pi/2] (inclusively) via phi=0
    // 5. switch off laser
    // 6. place ellipse at bottom
    // 7. switch on laser
    // 8. step along circular arc clockwise
    // 9. repeat steps over [pi 3/2, pi/2[ (exclusively)
    // 10. Stop at point in front of top point
    // 11. Switch off laser
    // 12. Move to top point, initial position

    // z_fused_silica := 0.6576;

    with PMAC_Buffer do
    begin
      angular_step := Pi/angular_res;
      { The parametrization of a ellipse is (a*cos(s) , b*sin(s) ) where the parameter s goes from 0 to 2 pi.
        In each step a parameter is needed to store the ellipse parameter s at the contact point
        between ellipse and cirlce. This will be the PMAC variable Q220.
          }
      Add('Q215='+ FloatToStr(halfaxis_a));   {Q215: halfaxis a of the writing ellipse}
      Add('Q216='+ FloatToStr(halfaxis_b));   {Q216: halfaxis b of the writing ellipse}
      Add('Q217='+ FloatToStr(radius));       {Q217: radius of the resulting, written circle}
      Add('Q218='+ FloatToStr(angular_step)); {Q218: angular step size, depends on resolution}
      Add('Q219='+ FloatToStr(-PI/2));        {Q219: current angular position w.r.r. circle center, changed in each step}
      Add('Q220=0');                          {Q220: ellipse parameter s, read at the beginning of the secion}
      Add('Q221=0');                          {Q221: x comp. of new ellipse center w.r.t. circle center}
      Add('Q222=0');                          {Q221: y comp. of new ellipse center w.r.t. circle center}
      Add('Q223=0');                          {Q223: x comp. of previous ellipse center w.r.t. circle center, called Kx}
      Add('Q224=0');                          {Q224: y comp. of previous ellipse center w.r.t. circle center, called Ky}
      Add('Q225=0');                          {Q225: Dummy variable to store a value temporarily in a different variable }
      Add('Q226='+ FloatToStr(Z_correct)); {Q226: Correction factor for the material when moving in z-direction}
      
      // 1.
        if not(initialLOn) then    // if starting at the bottom and the laser is switched off, switch it on
        LOn(5);   

      Add('I15=1'); {Switch from degree to radian}
      // Set initial ellipse parameter
      // Add('Q220=ATAN2(Q216*SIN(Q219),Q215*COS(Q219))');         {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
      Add('Q0=Q215*COS(Q219)');
      Add('Q220=ATAN2(Q216*SIN(Q219))');         {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
      // Set Kx and Ky to initial value
      Add('Q223=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
      Add('Q224=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
      Add('P223=Q223');
      Add('P224=Q224');

      // 4.
      Add('Q211=0'); { Q211 is an Iterative variable }
      { start loop for tube here }
      // Add('DWELL4000;');
      Add('WHILE(Q211<'+IntToStr(angular_res)+')');
        // new angle position: current_pos + step
        Add('Q225=Q219+Q218');
        Add('Q219=Q225');
        // Set new ellipse parameter
        {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
        Add('Q0=Q215*COS(Q219)');
        Add('Q220=ATAN2(Q216*SIN(Q219))');
        // Calculate new (Kx,Ky) values for new angle position
        Add('Q221=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
        Add('Q222=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}

        // TODO
        // 3.
        MotionStyle(True, True);  { Motion is set to linear relative }
        add('X(Q221-Q223) Y0 Z(-Q226*(Q222-Q224));');        {Q226 is the correction due to material effect}
        // Add('DWELL4000;');
        // set new (Kx,Ky) to old (Kx, Ky)
        Add('Q223=Q221');
        Add('Q224=Q222');
        Add('P223=Q223');
        Add('P224=Q224');
  //      cur_angle := step_angle_ellipse(cur_angle, halfaxis_a, halfaxis_b , radius,  angular_step);
        Add('Q211=Q211+1;'); { increment counter }
      Add('ENDWHILE;'); { end while loop }
    // end;

      // 5.
      LOff(0);
      // 6., mrel(0,0, - 2 * radius - 2* halfaxis_b)
      MotionStyle(True, True);  { Motion is set to linear relative }
      add('X0 Y0 Z(2*Q226*(Q216+Q217) );');       {Q226 is the correction due to material effect}
      // 7.
      LOn(0);

//    with PMAC_Buffer do
//    begin
      cur_angle := -PI/2;
      angular_step := Pi/angular_res;
      { The parametrization of a ellipse is (a*cos(s) , b*sin(s) ) where the parameter s goes from 0 to 2 pi.
        In each step a parameter is needed to store the ellipse parameter s at the contact point
        between ellipse and cirlce. This will be the PMAC variable Q220.
          }
    
      Add('Q218=-'+ FloatToStr(angular_step)); {Q218: angular step size, depends on resolution}
      Add('Q219='+ FloatToStr(-PI/2));        {Q219: current angular position w.r.r. circle center, changed in each step}
      Add('Q220=0');                          {Q220: ellipse parameter s, read at the beginning of the secion}

      Add('I15=1'); {Switch from degree to radian}
      // Set initial ellipse parameter
      {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
      Add('Q0=Q215*COS(Q219)');
      Add('Q220=ATAN2(Q216*SIN(Q219))');
      // Set Kx and Ky to initial value
      Add('Q223=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
      Add('Q224=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
      
     // 9.
      Add('Q211=0'); { Q211 is an Iterative variable }
    { start loop for tube here }
      Add('WHILE(Q211<'+IntToStr(angular_res)+')');
        // new angle position: current_pos + step
        Add('Q225=Q219+Q218');
        Add('Q219=Q225');
        // Set new ellipse parameter
        
        Add('Q0=Q215*COS(Q219)');
        Add('Q220=ATAN2(Q216*SIN(Q219))');      {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
        // Calculate new (Kx,Ky) values for new angle position
        Add('Q221=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
        Add('Q222=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}

        // 8.
        MotionStyle(True, True);  { Motion is set to linear relative }
        add('X(Q221-Q223) Y0 Z(-Q226*(Q222-Q224));');       {Q226 is the correction due to material effect}
       //  Add('DWELL4000;');
        // set new (Kx,Ky) to old (Kx, Ky)
        Add('Q223=Q221');
        Add('Q224=Q222');
        Add('Q211=Q211+1;'); { increment counter }
     // 10.
     Add('ENDWHILE;'); { end while loop }
    end;

    // 11.
    if not(initialLOn) then LOff(5);
    mrel(0,0, - 2 * radius - 2* halfaxis_b);    // move to bottom
    // 12., reposition to initial point
    // new angle position: current_pos + step

    // PMAC_Buffer.Add('Q225=Q219+Q218');
//    PMAC_Buffer.Add('Q219=Q225');
    // Set new ellipse parameter
//    PMAC_Buffer.Add('Q220=ARCTAN2(Q216* SIN(Q219), Q215*COS(Q219) )');         {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
    // Calculate new (Kx,Ky) values for new angle position
//    PMAC_Buffer.Add('Q221=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
//    PMAC_Buffer.Add('Q222=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
//    MotionStyle(True, True);  { Motion is set to linear relative }
//    PMAC_Buffer.add('X(Q221-Q223) Y0 Z(Q222-Q224);');

end;

{
    Make a cylinder with its axis perpendicular to the laser beam. The cylinder axis is oriented
    along the y-direction while the laser beam goes along the z-axis.
    The laser beam has an ellipsoidal shape.
    Make sure, the laser beam is correctly positioned at the beginning.

    Parameters:
        length: Length of the cylinder along y
        radius: Radius of the base circle

        halfaxis_a: halfaxis of ellipsoidal laser beam in transverse plane (x and y components)
        halfaxis_b: halfaxis of ellipsoidal laser beam in longitudinal direction (z component)
        angular_res: angular resolution for a half circle. The interval [-Pi/, Pi/2 ] is partitioned in
            "angular_res" subintervals. The laser beam moves linearly from one angle to the other.
        y_resolution: Number of written circles aligned along the y direction. To have at least
            a result disconnected from the bulk, y_resolution should be at minimum
            length/(2* a_halfaxis). For a real cylindrical shape a higher resolution is recommended.
}

procedure cylinder(length, radius, a_halfaxis, halfaxis_b : double;
                                  angular_res,  y_resolution : integer);
var
    step_y: double;     // step size between neighboring circles; should be at least as large as the "transverse diameter" of the ellipse
begin
  step_y := length/y_resolution;
  PMAC_Buffer.Add('Q210=0'); { Q210 is an Iterative variable }
  with PMAC_Buffer do
    begin
    { start loop for tube here }
     Add('WHILE(Q210<'+IntToStr(y_resolution+1)+')');
     Circle_in_plane(radius, a_halfaxis, halfaxis_b, angular_res, True);
     mrel(0, step_y, 0);

     Add('Q210=Q210+1;'); { increment counter }
     Add('ENDWHILE;'); { end while loop }
    end;
end;

{ 
  current_pos : angular position of the focal point w.r.t. the resulting circle
  a: halfaxis of ellipsoidal laser beam in transverse plane (x and y components)
  b: halfaxis of ellipsoidal laser beam in longitudinal direction (z component)   
  radius: radius of the resulting circle
  step: change of the angular position of the focal point
}
function step_angle_ellipse_for_hole(current_pos, a, b, radius, step : double ): double;
var
   (* local variable declaration *)
   dx : double;
   dz : double;
   
   {--- helper functions---}
   { The parametrization of a ellipse is (a*cos(s) , b*sin(s) ) where the parameter s goes from 0 to 2 pi. 
    This function returns the parameter s at the contact point between circle and ellipse which is used to 
    determine the center points K_x(s) and K_y(s) of the ellipse
    Parameter:
        phi: angle of the vector pointing from the origin/center of the circle to the contact point
   }
   function ellipse_s(phi: double) : double;
   begin ellipse_s := Arctan2(b* Sin(phi), a* Cos(phi) ) end;
   
   {x component of the ellipse's center. Expressed in terms of the angle to the contact point phi}
   function K_x(phi : double) : double ;
   begin K_x := -a*Cos( ellipse_s(phi) ) + radius * Cos(phi) end;
   
   {y component of the ellipse's center. Expressed in terms of the angle to the contact point phi}
   function K_y(phi : double) : double ;
   begin K_y := -b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;
   {--- end helper ---} 
begin
   dx := K_x(current_pos + step) - K_x(current_pos);
   dz := K_y(current_pos + step) - K_y(current_pos);
   mrel(dx, 0, dz);
   
   step_angle_ellipse_for_hole := current_pos + step;
end;


{
    Function for writing a circular hole oriented with its normal vector perpendicular to the writing axis. This means the laser direction is "in plane" w.r.t. the written circle.
    The writing axis is the direction of the incoming laser beam, denoted as z here.
    Initially, the laser's focus has to be positioned on the bottom of the resulting circle. 
     It will start writting following the circle's half arc.
    REMARK: The original function started from the top and repositioned to the bottom as a first step. This is also included in the pascal version of this function as an option via a parameter, but is droped in the PMAC version here.
    
    Parameters:
        radius: Radius of the resulting circle
        halfaxis_a: size of ellipsoidal laser beam in transverse plane (x and y components)
        halfaxis_b: size of ellipsoidal laser beam in longitudinal direction (z component)
        angular_res: resolution in angular direction -> Steps in x-z-plane
        initialLOn: Describes whether the Laser is on or off when starting the function. The function takes care of switching on and off the laser. The final status of the laser is equal to the initial one.
    
    Since no correction for the discreteness of the steps is considered the effective radius may be larger (average of distance from the center point to the surface)
}
procedure Circular_Hole_in_plane(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; initialLOn : boolean);
var
    { local declarations}
    { M_x : double;      // center coordinates of the ellipse  }
    { M_z : double; }
    angular_step : double;  // 
    cur_angle : double;
    i : integer;    // loop variable
    
begin
    {procedure body}
    // Logic structure
    // 1. Initially, the spot is positioned outside the circle. Move it inside
    // 2. Switch on laser
    // 3. step along the full circular arc counterclockwise
    // 4. repeat steps from [-pi/2, pi/2] (inclusively) via phi=0
    // 5. switch off laser
    // 6. place ellipse at bottom
    // 7. switch on laser
    // 8. step along circular arc clockwise
    // 9. repeat steps over [pi 3/2, pi/2[ (exclusively)
    // 10. Stop at point in front of top point
    // 11. Switch off laser
    // 12. Move to top point, initial position 
    
    with PMAC_Buffer do
    begin
      cur_angle := -PI/2;
      angular_step := Pi/angular_res;
      { The parametrization of a ellipse is (a*cos(s) , b*sin(s) ) where the parameter s goes from 0 to 2 pi. In each step a parameter is needed to store the ellipse parameter s at the contact point between ellipse and cirlce. This will be the PMAC variable Q220.}
      Add('Q215='+ FloatToStr(halfaxis_a));   {Q215: halfaxis a of the writing ellipse}
      Add('Q216='+ FloatToStr(halfaxis_b));   {Q216: halfaxis b of the writing ellipse}
      Add('Q217='+ FloatToStr(radius));       {Q217: radius of the resulting, written circle}
      Add('Q218='+ FloatToStr(angular_step)); {Q218: angular step size, depends on resolution}
      Add('Q219='+ FloatToStr(-PI/2));        {Q219: current angular position w.r.r. circle center, changed in each step}
      Add('Q220=0');                          {Q220: ellipse parameter s, read at the beginning of the secion}
      Add('Q221=0');                          {Q221: x comp. of new ellipse center w.r.t. circle center}
      Add('Q222=0');                          {Q221: y comp. of new ellipse center w.r.t. circle center}
      Add('Q223=0');                          {Q223: x comp. of previous ellipse center w.r.t. circle center, called Kx}
      Add('Q224=0');                          {Q224: y comp. of previous ellipse center w.r.t. circle center, called Ky}
      Add('Q225=0');                          {Q225: Dummy variable to store a value temporarily in a different variable }
      Add('Q226='+ FloatToStr(Z_correct)); {Q226: Correction factor for the material when moving in z-direction}
      
        // 1. 
        mrel(0,0, +2*halfaxis_b);  
        // 2.
        if not(initialLOn) then LOn(5);
            

      Add('I15=1'); {Switch from degree to radian}
      // Set initial ellipse parameter
      // Add('Q220=ATAN2(Q216*SIN(Q219),Q215*COS(Q219))');         {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
      Add('Q0=Q215*COS(Q219)');
      Add('Q220=ATAN2(Q216*SIN(Q219))');         {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
      // Set Kx and Ky to initial value
      Add('Q223=-Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
      Add('Q224=-Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
      Add('P223=Q223');
      Add('P224=Q224');

      // 4.
      Add('Q211=0'); { Q211 is an Iterative variable }
      { start loop for tube here }
      // Add('DWELL4000;');
      Add('WHILE(Q211<'+IntToStr(angular_res)+')');
        // new angle position: current_pos + step
        Add('Q225=Q219+Q218');
        Add('Q219=Q225');
        // Set new ellipse parameter
        {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
        Add('Q0=Q215*COS(Q219)');
        Add('Q220=ATAN2(Q216*SIN(Q219))');
        // Calculate new (Kx,Ky) values for new angle position
        Add('Q221=-Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
        Add('Q222=-Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}

        // 3.
        MotionStyle(True, True);  { Motion is set to linear relative }
        add('X(Q221-Q223) Y0 Z(-Q226*(Q222-Q224));');        {Q226 is the correction due to material effect}
        // Add('DWELL4000;');
        // set new (Kx,Ky) to old (Kx, Ky)
        Add('Q223=Q221');
        Add('Q224=Q222');
        Add('P223=Q223');
        Add('P224=Q224');
  //      cur_angle := step_angle_ellipse(cur_angle, halfaxis_a, halfaxis_b , radius,  angular_step);
        Add('Q211=Q211+1;'); { increment counter }
      Add('ENDWHILE;'); { end while loop }
    // end;

      // 5.
      LOff(0);
      // 6., mrel(0,0, - 2 * radius - 2* halfaxis_b)
      MotionStyle(True, True);  { Motion is set to linear relative }
      add('X0 Y0 Z(2*Q226*(Q216+Q217) );');       {Q226 is the correction due to material effect}
      // 7.
      LOn(0);

//    with PMAC_Buffer do
//    begin
      cur_angle := -PI/2;
      angular_step := Pi/angular_res;
      { The parametrization of a ellipse is (a*cos(s) , b*sin(s) ) where the parameter s goes from 0 to 2 pi.
        In each step a parameter is needed to store the ellipse parameter s at the contact point
        between ellipse and cirlce. This will be the PMAC variable Q220.
          }
    
      Add('Q218=-'+ FloatToStr(angular_step)); {Q218: angular step size, depends on resolution}
      Add('Q219='+ FloatToStr(-PI/2));        {Q219: current angular position w.r.r. circle center, changed in each step}
      Add('Q220=0');                          {Q220: ellipse parameter s, read at the beginning of the secion}

      Add('I15=1'); {Switch from degree to radian}
      // Set initial ellipse parameter
      {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
      Add('Q0=Q215*COS(Q219)');
      Add('Q220=ATAN2(Q216*SIN(Q219))');
      // Set Kx and Ky to initial value
      Add('Q223=-Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
      Add('Q224=-Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
      
     // 9.
      Add('Q211=0'); { Q211 is an Iterative variable }
    { start loop for tube here }
      Add('WHILE(Q211<'+IntToStr(angular_res)+')');
        // new angle position: current_pos + step
        Add('Q225=Q219+Q218');
        Add('Q219=Q225');
        // Set new ellipse parameter
        
        Add('Q0=Q215*COS(Q219)');
        Add('Q220=ATAN2(Q216*SIN(Q219))');      {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
        // Calculate new (Kx,Ky) values for new angle position
        Add('Q221=-Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
        Add('Q222=-Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}

        // 8.
        MotionStyle(True, True);  { Motion is set to linear relative }
        add('X(Q221-Q223) Y0 Z(-Q226*(Q222-Q224));');       {Q226 is the correction due to material effect}
       //  Add('DWELL4000;');
        // set new (Kx,Ky) to old (Kx, Ky)
        Add('Q223=Q221');
        Add('Q224=Q222');
        Add('Q211=Q211+1;'); { increment counter }
     // 10.
     Add('ENDWHILE;'); { end while loop }
    end;    
    if not(initialLOn) then LOff(5);
    // reposition to initial point
    mrel( 0, 0, -2*radius);
end; 

{
    Make a cylinder with its axis perpendicular to the laser beam. The cylinder axis is oriented     along the y-direction while the laser beam goes along the z-axis.
    The laser beam has an ellipsoidal shape.
    Make sure, the laser beam is correctly positioned at the beginning. It should be positioned on the bottom and one end of the cylinder. 
    
    Parameters:
        length: Length of the cylinder along y
        radius: Radius of the base circle
        y_resolution: Number of written circles aligned along the y direction. To have at least 
            a result disconnected from the bulk, y_resolution should be at minimum
            length/(2* a_halfaxis). For a real cylindrical shape a higher resolution is recommended.
        a_halfaxis: halfaxis of ellipsoidal laser beam in transverse plane (x and y components)
        halfaxis_b: halfaxis of ellipsoidal laser beam in longitudinal direction (z component)
        angular_res: angular resolution for a half circle. The interval [-Pi/, Pi/2 ] is partitioned in 
            "angular_res" subintervals. The laser beam moves linearly from one angle to the other.
    
    REMARK: In comparison to the other cylinder function this one does one step over the circle arc and draw a line along y. The other function steps over the y direction and then draws a circle.
    
    Used Q-Variables:
    Q0   - for Arctan2 function
    Q211 - loop iterator
    Q215-Q217 - constant Q variables, don't change while running the loops 
    Q218-Q227 - dynamic Q variables, change while stepping over the arc
        variables for calculating the next step along the circle
}
procedure cylinder2(length, radius, halfaxis_a, halfaxis_b : double;
    angular_res : integer);
var
    step_y: double;     // step size between neighboring circles; should be at least as large as the "transverse diameter" of the ellipse
    sign_y_side : integer;
    angular_step : double;
begin
    // Logic structure
    // 1. Initialize Q-Variables. Those initialized with 0 are dynamic variables, meaning that they change during the execution of the function
    // 2. Switch on laser
    // 3. Set the dynamic Q-Variables to their initial values
    // 4. Start loop for stepping over [-pi/2, pi/2] in positive direction until the top point is reached. Depending whether the angular resolution is odd or even the laser is positioned on the initial side of the cylinder or the opposite.
    // In the loop:
        // 5. draw a line along y-direction
        // 6. Calculate new laser position on the circle
        // 7. Move laser by difference of new and old laser point
        // 8. Set new position to old position
    // 9. Switch off Laser, move to bottom, switch on again
    // 10. Set the dynamic Q-Variables to their initial values for second loop
    // 11. Start looping over [-pi/2, pi/2] in negative direction until top point is reached
    // In the loop:
        // 12. Calculate new laser position on the circle
        // 13. Move laser by difference of new and old laser point
        // 14. Set new position to old position
        // 15. draw a line along y-direction
    // 16. Switch off laser
    // (17. ensure that the laser is positioned at the end and at the bottom of the cylinder) TBD
           
    with PMAC_Buffer do
    begin
      angular_step := Pi/angular_res;
      { The parametrization of a ellipse is (a*cos(s) , b*sin(s) ) where the parameter s goes from 0 to 2 pi.
        In each step a parameter is needed to store the ellipse parameter s at the contact point
        between ellipse and cirlce. This will be the PMAC variable Q220.
          }
      // 1.
      Add('Q215='+ FloatToStr(halfaxis_a));   {Q215: halfaxis a of the writing ellipse}
      Add('Q216='+ FloatToStr(halfaxis_b));   {Q216: halfaxis b of the writing ellipse}
      Add('Q217='+ FloatToStr(radius));       {Q217: radius of the resulting, written circle}
      Add('Q218='+ FloatToStr(angular_step)); {Q218: angular step size, depends on resolution}
      Add('Q219='+ FloatToStr(-PI/2));        {Q219: current angular position w.r.r. circle center, changed in each step}
      Add('Q220=0');                          {Q220: ellipse parameter s, read at the beginning of the secion}
      Add('Q221=0');                          {Q221: x comp. of new ellipse center w.r.t. circle center}
      Add('Q222=0');                          {Q221: y comp. of new ellipse center w.r.t. circle center}
      Add('Q223=0');                          {Q223: x comp. of previous ellipse center w.r.t. circle center, called Kx}
      Add('Q224=0');                          {Q224: y comp. of previous ellipse center w.r.t. circle center, called Ky}
      Add('Q225=0');                          {Q225: Dummy variable to store a value temporarily in a different variable }
      Add('Q226='+ FloatToStr(Z_correct));    {Q226: Correction factor for the material when moving in z-direction}
      Add('Q227=1');                           {Q227: Sign for specifying the current direction of the line along y. }
      //2.
      LOn(5);   
      //3.
      Add('I15=1'); {Switch from degree to radian}
      // Set initial ellipse parameter
      Add('Q0=Q215*COS(Q219)');
      Add('Q220=ATAN2(Q216*SIN(Q219))');         {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
      // Set Kx and Ky to initial value
      Add('Q223=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
      Add('Q224=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}

      // 4.
      Add('Q211=0'); { Q211 is an Iterative variable }
      Add('WHILE(Q211<'+IntToStr(angular_res)+')');
        //5.
        Add('X0 Y(Q227*'+FloatToStr(length)+') Z0;' );
        // 6.  new angle position: current_pos + step
        Add('Q225=Q219+Q218');
        Add('Q219=Q225');
        // Set new ellipse parameter, s = Arctan2(b* Sin(phi), a* Cos(phi) )
        Add('Q0=Q215*COS(Q219)');
        Add('Q220=ATAN2(Q216*SIN(Q219))');
        // Calculate new (Kx,Ky) values for new angle position
        Add('Q221=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
        Add('Q222=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
        // 7. 
        MotionStyle(True, True);  { Motion is set to linear relative }
        add('X(Q221-Q223) Y0 Z(-Q226*(Q222-Q224));');        {Q226 is the correction due to material effect}
        // 8. set new (Kx,Ky) to old (Kx, Ky)
        Add('Q223=Q221');
        Add('Q224=Q222');
        Add('Q211=Q211+1;'); { increment counter }
        Add('Q227=-Q227');  {switch direction of writing along y}
      Add('ENDWHILE;'); { end while loop }

      // 9.
      LOff(0);
      MotionStyle(True, True);  { Motion is set to linear relative }
      add('X0 Y0 Z(2*Q226*(Q216+Q217) );');       {Q226 is the correction due to material effect}
      LOn(0);

      // 10.
      Add('Q218=-'+ FloatToStr(angular_step)); {Q218: angular step size, depends on resolution}
      Add('Q219='+ FloatToStr(-PI/2));        {Q219: current angular position w.r.r. circle center, changed in each step}
      Add('Q220=0');                          {Q220: ellipse parameter s, read at the beginning of the secion}
      Add('I15=1'); {Switch from degree to radian}
      // Set initial ellipse parameter
      {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
      Add('Q0=Q215*COS(Q219)');
      Add('Q220=ATAN2(Q216*SIN(Q219))');
      // Set Kx and Ky to initial value
      Add('Q223=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
      Add('Q224=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
      
     // 11.
      Add('Q211=0'); { Q211 is an Iterative variable }
      Add('WHILE(Q211<'+IntToStr(angular_res)+')');
        //12.
        // new angle position: current_pos + step
        Add('Q225=Q219+Q218');
        Add('Q219=Q225');
        // Set new ellipse parameter
        Add('Q0=Q215*COS(Q219)');
        Add('Q220=ATAN2(Q216*SIN(Q219))');      {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
        // Calculate new (Kx,Ky) values for new angle position
        Add('Q221=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
        Add('Q222=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}

        // 13.
        MotionStyle(True, True);  { Motion is set to linear relative }
        add('X(Q221-Q223) Y0 Z(-Q226*(Q222-Q224));');       {Q226 is the correction due to material effect}
        // 14.
        Add('Q223=Q221');
        Add('Q224=Q222');
        // 15.
        Add('X0 Y(Q227*'+FloatToStr(length)+') Z0;' );
        Add('Q211=Q211+1;'); { increment counter }
        Add('Q227=-Q227');  {switch direction of writing along y}
    Add('ENDWHILE;'); { end while loop }
    end;

    // 16.
    LOff(5);
    // 17.
    mrel(0,0, - 2 * radius - 2* halfaxis_b);    // move to bottom
    // TODO: Position at end
    
end;

procedure draw_Hexagonal_Wvg(lattice_const, radius : double; num_cells: integer; halfaxis_a, halfaxis_b: double; angular_resolution: integer);
var 
    n_layers : integer;
    x_dist : double;
    y_dist : double;
    sign : integer;
    i : integer; 
    
    procedure x_step_with_etching(sign : integer; initialLOn : boolean);
    {   Instead of stepping one cell along the x direction a 
        1) half line is written
        2) a line as high as the layer heigth is written from the layer below to the current height
        3) the second half of the line is written to complete the cell step
         -----WARNING ----
        Keep in mind, that it only works correctly if the circles are drawn starting from bottom.
        
        Before, it was just:
        mrel(sign*x_dist, 0, 0);          
    }
    begin
        if not(initialLOn) then LOn(0);
        mrel(0.5*sign*x_dist, 0, 0);          
        mrel(0,0, y_dist);
        LOff(0);
        mrel(0,0, -y_dist);
        LOn(0);
        mrel(0.5*sign*x_dist, 0, 0);
        if not(initialLOn) then LOff(0);
    end;
    
    procedure process_layer(i: integer; direction: integer);
    var
    	j : integer;
    	sign : integer;
    begin
        if direction > 0 then sign := 1 
        else if direction < 0 then sign := -1
        else Writeln('Direction can not take the value "0".');
        
        LOn(0);
        // TODO: Draw first circle
        Circle_in_plane(radius, halfaxis_a, halfaxis_b, angular_resolution, True);
        // step out of the hexagon to connect to the outer etching paths
        x_step_with_etching(-sign, True);
        //step back
        mrel(sign*x_dist, 0,0);
        
        // not in the central layer
        if i <> 0 then
        begin
            for j:=1 to ( n_layers-abs(i) - 1) do    // minus 2 due to inclusive loop
            begin
                { mrel(sign*x_dist, 0, 0); }
                x_step_with_etching(sign, True); // step along the layer with etch paths
                Circle_in_plane(radius, halfaxis_a, halfaxis_b, angular_resolution, True);
            end;
        end
        // central layer
        else
        begin
            for j:=1 to num_cells - 1 do
            begin
                { mrel(sign*x_dist, 0, 0); }
                x_step_with_etching(sign, True); // step along the layer with etch paths
                Circle_in_plane(radius, halfaxis_a, halfaxis_b, angular_resolution, True);

            end;
            LOff(0);
            mrel(sign*x_dist, 0, 0);
            // TODO: draw the waveguide hole
            //      move over two lattice cells will be adjusted
            // 1. Correct position to bottom of large circle
            // 2. Write the Circular Hole
            // 3. Correct position back to the bottom of the glass circles
            // 4. step aside along x for the position of the next glass circle
            // TODO: draw_circle
            mrel(0,0, 2*radius-lattice_const);  //1.
	        Circle_in_plane(lattice_const - radius, halfaxis_a, halfaxis_b, angular_resolution, False); //2.
            mrel(0,0, lattice_const - 2*radius); //3.
            mrel(sign*x_dist, 0, 0); //4.
            LOn(0);
            for j:=1 to num_cells - 1 do
            begin
                { mrel(sign*x_dist, 0, 0); }
                x_step_with_etching(sign, True); // step along the layer with etch paths
                // TODO: draw_circle
                Circle_in_plane(radius, halfaxis_a, halfaxis_b, angular_resolution, True);
            end;
        end;
        x_step_with_etching(sign, True);
        LOff(0);
        mrel(-sign*x_dist, 0,0);
    end;
begin
     // Initialize variable
    n_layers := num_cells*2 +1;
    x_dist := lattice_const;
    y_dist := lattice_const*sqrt(3.0)/2;
    // sign = 1 if num_cells%2==0 else -1 
    if num_cells mod 2 =0 then sign := 1 else sign := -1;
    
    
    // 1.
    mrel(-sign*x_dist*num_cells* 0.5 , 0, - num_cells*y_dist );

    // 5.
    for i:=-num_cells to num_cells do
    begin
        if i mod 2 =0 then sign := 1 else sign := -1;
        // 2. + 4.
        process_layer(i,sign);
        // 3.
        if i < 0 then 
            mrel(sign*0.5*x_dist ,0, y_dist)
         else
            mrel(-sign*0.5*x_dist ,0, y_dist);
    end;
   
   	// 6. In the end, place at the center of the waveguie
    if num_cells mod 2 =0 then sign := 1 else sign := -1;
    mrel(-sign*x_dist*(num_cells-1)* 0.5 , 0, - (num_cells+1) *y_dist );
end;

{ This function draws a structure of parallel cylinders which are arranged in a hexagonal lattice in the transverse plane.
 This structure obtains a photonic band gap for TM modes.
To get a vacuum waveguide the central cylinder is removed. In a regular distance along the cylinder axis round plates are added to mount the freestanding cylinders.
The structure has several parameters: (all spatial parameters are given in um)
    lattice_const: distance between the cylinders. Due to the reference of a photonic crystal this gives the periodicity of such a lattice
    radius: radius of the cylinder
    num_cells: number of lattice periods. For details see function "draw_Hexagonal_Wvg"
    halfaxis_a: transverse halfaxis of the ellipsoid of the laser's focal spot, perpendicular to the beam axis
    halfaxis_b: longitudinal halfaxis of the ellipsoid of the laser's focal spot, parallel to the beam axis
    
    mounting_dist: distance between the mounting plates
    mounting_thickness: thickness of the mounting plates along the cylinder axis. The radius of the mounting plates is already defined via the number of lattice cells and the lattice constant
    num_mountings: number of mounting plates
        
    angular_res: Discretization of the half circle arc
    y_resolution: discretization along the cylinder axis    
    
    
    ----------------------------- NOT  FINISHED YET ----------------------------------
}
{
procedure draw_Mounted_Hexagonal_Wvg(lattice_const, radius: double; num_cells : integer; halfaxis_a, halfaxis_b, mounting_dist, mounting_thickness: double; num_mountings, angular_res, y_resolution: integer);
var
    step_y: double;     // step size between neighboring circles; should be at least as large as the "transverse diameter" of the ellipse
    k : integer;     // loop variable for stepping along y-direction
    j : integer;
    length: double;
    length_section : double;
    mounting_radius: double;
begin
    // Logic
    // TODO: Rewrite 'draw_Hexagonal_Wvg' such that it starts with the initial focus on the top instead of the center
    // 1. Start loop for first section of rods (2. - 5.)
    // 2. draw the hexagonal structure in the x-z plane
    // 3. Make sure the laser beam is correctly positioned
    // 4. Make a Step in y-direction while Laser is turned off
    // 5. Repeat from 2. until the first section is finished. This means when y has changed by 'mounting_dist - mounting_dist'
    // 6. Draw a cylinder of height 'mounting_thickness' and radius '( num_cells + 1) * lattice-const'
    // 7. Draw second section of rods by repeating from 2. until the second section is finished and draw the mounting from 6. again
    // 8. Repeat from 2. to 7. 'num_mountings' times
    // 9. Draw last section of rods by finally doing steps 2.-5.
    
    // TODO: make sure the routine "draw_Hexagonal_Wvg" starts from the surface and not from the center
    // TODO: Replace llop with PMAC Code
    // TODO: Check lengths
    length_section :=  mounting_dist - mounting_thickness;
    length := (num_mountings + 1)*mounting_dist;    
    mounting_radius := ( num_cells + 1) * lattice_const;
    
    step_y := length/y_resolution;
    num_steps_section := Int(length_section/step_y); // TODO
    num_steps_mounting := Int(mounting_thickness/step_y);  //TODO
    for k:= 1 to num_mountings do
    begin  // 7. / 8.
        // 1. 
        for j:= 1 to num_steps_section do
        begin
            // 2.
            // draw_Hexagonal_Wvg_plane(lattice_const: integer; radius : double; num_cells: integer; halfaxis_a, halfaxis_b: double; angular_resolution: integer)
            draw_Hexagonal_Wvg(lattice_const, radius, num_cells, a_halfaxis, halfaxis_b, angular_res);
            If DEBUG then
                Writeln('Circle finished. Moving to position along cylinder axis.');    
            // 4.
            mrel(0, step_y, 0);
        // 5.
        end;
        // 6. 
        for j:= 1 to num_steps_mounting do
        begin
            // signature: Circle_in_plane(radius, halfaxis_a, halfaxis_b, angular_res)
            Circle_in_plane(mounting_radius, a_halfaxis, halfaxis_b, angular_res);
            If DEBUG then
                Writeln('Circle finished. Moving to position along cylinder axis.');
                
            mrel(0, step_y, 0);
        end;
    // 8.
    end; 
    // 9.
    // TODO
end;
}

{ Temporary structure, cylinders arragned in a hexagon. Mounting is missing. PMAC based}
procedure hexagon_of_cylinders(length, lattice_const, radius, a_halfaxis, halfaxis_b : double; num_cells: integer;
                                  angular_res,  y_resolution : integer);
var
    step_y: double;     // step size between neighboring circles; should be at least as large as the "transverse diameter" of the ellipse
  //  j : integer;     // loop variable for stepping along y-direction
    // TODO
begin
    step_y := length/y_resolution;

    PMAC_Buffer.Add('Q210=0'); { Q210 is an Iterative variable }

    with PMAC_Buffer do
    begin
        { start loop for tube here }

        Add('WHILE(Q210<'+IntToStr(y_resolution)+')');

        // draw_Hexagonal_Wvg(lattice_const, radius : double; num_cells: integer; halfaxis_a, halfaxis_b: double; angular_resolution: integer);
        draw_Hexagonal_Wvg(lattice_const, radius, num_cells, a_halfaxis, halfaxis_b, angular_res);
        // Circle_in_plane(radius, a_halfaxis, halfaxis_b, angular_res);
        mrel(0, step_y, 0);

        Add('Q210=Q210+1;'); { increment counter }
        Add('ENDWHILE;'); { end while loop }
    end;
end;

{   With the function 'draw_Hexagonal_Wvg' the hexagonal array of circles is printed, but it requires a connected boundary to make it accessible for the etchant. 
    The initial position is on top of the final hexagonal structure
    REMARK: The pascal version of this function includes a boolean parameter for adjusting the hexagon based on the lasers initial position when drawing circles. This is dropped in this PMAC Version.
    
    Parameters:
        Most are the same as from the function of a hexagonal waveguide. They are needed for computing the correct dimensions of the boundary paths
        boolBottom: If True the etch path is drawn on the bottom, if false it is drawn on top off the hexagon. Both together form a closed hexagonal path
}
procedure Hexagonal_wvg_half_etch_boundary(lattice_const, radius : double; num_cells: integer; halfaxis_a, halfaxis_b: double; boolBottom : boolean);
var 
    { n_layers : integer; }
    x_dist : double;
    y_dist : double;
    sign : integer;
    halfheight : double;    
begin
    { n_layers := num_cells*2 +1; }
    x_dist := lattice_const;
    y_dist := lattice_const*sqrt(3.0)/2;
    if boolBottom then sign := 1 else sign := -1; // The sign determines whether the upper or lower half is drawn
    { if circlesStartedFromTop then signTop := 1 else signTop := -1; }
    halfheight := (num_cells + 1 ) *y_dist;
   
    mrel(-(num_cells+1)*x_dist, 0, - halfheight); // move to left corner of center layer
    { mrel(0, 0, -signTop*(radius+ halfaxis_b)); }
    LOn(0);
    mrel(0.5*x_dist*(num_cells + 1) ,0, -sign*y_dist*(num_cells + 1) ); // move along one side towards bottom
    mrel(x_dist*(num_cells+1), 0, 0);  // move along the bottom side to the right side 
    mrel(0.5*x_dist*(num_cells + 1) ,0, sign*y_dist*(num_cells + 1) ); // move to the right corner of the center layer
    LOff(0);
    mrel(-(num_cells+1)*x_dist, 0, halfheight ); // move back to initial position
    { mrel(0,0, signTop*(radius+ halfaxis_b) ); }
end;

{ Draw a vertical plane, parallel to z. The second direction of the plane is specified as a parameter.
Max Kellermeier, maxk@student.ethz.ch

    It is assumed that the initial laser position is the bottom left corner. Of course this becomes the bottom right corner if the orientation is 180 degree. But it's important to keep in mind. 

    Parameters: 
        length: length of the plane in um. This is the length of the intersection line of this plane with the xy-plane
        height: height of the plane in the z-direction
        orientationX, orientationY: a 2d vector parallel to the plane. Only its direction is considered, not its magnitude. 
        vertical_res: number of lines in the vertical direction.
        
}
procedure verticalPlane(length, height : double; orientationX, orientationY: double; vertical_res: integer);
var
    sign : integer;     {used for specifying the current writing direction}
    angle_in_xy : double;
    stepsize_z : double;
begin
    angle_in_xy := Arctan2(orientationY, orientationX );
    sign := 1; //TODO
    stepsize_z := height/vertical_res;
    
    ChangePol( Round( radtodeg(angle_in_xy) ) + 90);
    //first line
    Line(1*length*cos(angle_in_xy), length*sin(angle_in_xy), 0);
    mrel(-length*cos(angle_in_xy), -length*sin(angle_in_xy), 0);
    
    //loop
    RepeatWhile(vertical_res);
        mrel(0,0,stepsize_z);
        Line(1*length*cos(angle_in_xy), length*sin(angle_in_xy), 0);
        mrel(-length*cos(angle_in_xy), -length*sin(angle_in_xy), 0);
    EndRepeat;
    
    //move back to initial position
    mrel(0,0, -stepsize_z*vertical_res);
end;

{ Draw a horizontal plane in the xy-plane. The sides are parallel to the axes.
Max Kellermeier, maxk@student.ethz.ch

    It is assumed that the laser is initially positioned at one of the bottom corners. Whether this point will be the left or the right corner depends on the writing direction along x, specified by 'sign_x_dir'. From this point the plane is written in positive y direction. The di

    Parameters: 
        width_x: length of the plane in x direction
        width_y: length of the plane in y direction
        height: height of the plane in the z-direction
        orientationX, orientationY: a 2d vector parallel to the plane. Only its direction is considered, not its magnitude. 
        resolution_x: number of lines in the x-direction.
        
}
procedure horizontalPlane(width_x, width_y : double; resolution_x: integer; sign_x_dir : integer);
var
    sign : integer;     {used for specifying the current writing direction}
    stepsize_x : double;
begin
    // TODO: Check that sign_x_dir only +1 or -1 is
    stepsize_x := sign_x_dir*width_x/resolution_x;
    ChangePol(90); // parallel polarization
    
    //first line
    Line(0,width_y,0);
    mrel(0, -width_y,0);
    
    //loop
    RepeatWhile(resolution_x);
    mrel(stepsize_x, 0, 0);
    Line(0,width_y,0);
    mrel(0, -width_y, 0);
    EndRepeat;
    //move back to initial position
    mrel(-resolution_x*stepsize_x, 0, 0)
end;

{Max Kellermeier, 26.7.2017

Same as the function horizontalPlane(...), but instead of pascal variables the Q-Variables from PMAC are used. In this way the size of the planes can be adjusted during the run.

Example:
    horizontalPlane_w_QVars("Q225", "Q226", 10 , "Q227")
    where Q225 is the width in x direction, Q226 the width in y direction and Q227 is either +1 or -1 and specifies the direction of the writing.
    
    Additional Q-variables are Q240, used for the stepsize, and Q241 for the loop.
}
procedure horizontalPlane_w_QVars(width_x, width_y : AnsiString; resolution_x: integer; sign_x_dir : AnsiString);
var
    sign : integer;     {used for specifying the current writing direction}
    stepsize_x : double;
begin
    // TODO: Check that sign_x_dir only +1 or -1 is
    { stepsize_x := sign_x_dir*width_x/resolution_x; }
    ChangePol(90); // parallel polarization
    
    Add('Q240=' + sign_x_dir + '*'+ width_x + '/' + IntToStr(resolution_x) + ";"); 
    
    With PMAC_Buffer do
    begin
    LOn(5);
    MotionStyle(True, True);
    //first line
    Add('X0 Y('+ width_y +') Z0 ; ' ); 
    Add('X0 Y(-1* ('+ width_y +')) Z0 ; ' ); 
    //loop
    Add('Q241=0'); { Q211 is an Iterative variable }
    Add('WHILE(Q241<'+IntToStr(resolution_x)+')');
        Add('X(Q240) Y0 Z0 ; ' ); 
        { mrel(stepsize_x, 0, 0); }
        Add('X0 Y('+ width_y +') Z0 ; ' ); 
        Add('X0 Y(-1* ('+ width_y +')) Z0 ; ' ); 
        Add('Q241=Q241+1;'); 
    Add('ENDWHILE;'); 
    LOff(0);
    //move back to initial position
    Add('X(-' + IntToStr(resolution_x) + '* Q240) Y0 Z0;');
    { mrel(-resolution_x*stepsize_x, 0, 0); }
    end;
end;



{   Added on 13.07.2017 by Max Kellermeier, ETHZ/PSI, maxk@student.ethz.ch

    Code is hosted on https://gitlab.psi.ch/kellermeier_m/Drawing-mounted-Hexagonal-PBG


    Make a half cylinder with its axis perpendicular to the laser beam. Only the arc is considered not the intersection along the center. The cylinder axis is oriented along the y-direction while the laser beam goes along the z-axis.
    The laser beam has an ellipsoidal shape.
    Make sure, the laser beam is correctly positioned at the beginning. It should be positioned on the bottom and one end of the cylinder.

    Parameters:
        length: Length of the cylinder along y
        radius: Radius of the base circle
        y_resolution: Number of written circles aligned along the y direction. To have at least
            a result disconnected from the bulk, y_resolution should be at minimum
            length/(2* a_halfaxis). For a real cylindrical shape a higher resolution is recommended.
        halfaxis_a: halfaxis of ellipsoidal laser beam in transverse plane (x and y components)
        halfaxis_b: halfaxis of ellipsoidal laser beam in longitudinal direction (z component)
        angular_res: angular resolution for a half circle. The interval [-Pi/, Pi/2 ] is partitioned in
            "angular_res" subintervals. The laser beam moves linearly from one angle to the other.
        circleSide: either +1 or -1, specfifying which half of the cylinder is drawn. If 'circleSide' is +1, the arc is drawn over [-pi/2, pi/2] in mathematically positive direction. If 'circleSide' is -1 it is drawn in mathematically negative direction, meaning that the half circle covers the angular range from [pi/2, 3/2 pi].

    REMARK: In comparison to the other cylinder function this one does one step over the circle arc and draw a line along y. The other function steps over the y direction and then draws a circle.

    Used Q-Variables:
    Q0   - for Arctan2 function
    Q211 - loop iterator
    Q215-Q217 - constant Q variables, don't change while running the loops
    Q218-Q227 - dynamic Q variables, change while stepping over the arc
        variables for calculating the next step along the circle
}
procedure halfcylinder(length, radius, halfaxis_a, halfaxis_b : double; angular_res : integer; circleSide : integer);
var
    step_y: double;     // step size between neighboring circles; should be at least as large as the "transverse diameter" of the ellipse
    sign_y_side: double;
    cur_angle : double;
    angular_step : double;

begin
    // Logic structure
    // 1. Initialize Q-Variables. Those initialized with 0 are dynamic variables, meaning that they change during the execution of the function
    // 2. Switch on laser
    // 3. Set the dynamic Q-Variables to their initial values
    // 4. Start loop for stepping over [-pi/2, pi/2] in positive direction until the top point is reached. Depending whether the angular resolution is odd or even the laser is positioned on the initial side of the cylinder or the opposite.
    // In the loop:
        // 5. draw a line along y-direction
        // 6. Calculate new laser position on the circle
        // 7. Move laser by difference of new and old laser point
        // 8. Set new position to old position
    // 9. Switch off Laser, move to bottom, switch on again
    // (17. ensure that the laser is positioned at the end and at the bottom of the cylinder) TBD

    with PMAC_Buffer do
    begin
      cur_angle := -PI/2;
      angular_step := Pi/angular_res;
      { The parametrization of a ellipse is (a*cos(s) , b*sin(s) ) where the parameter s goes from 0 to 2 pi.
        In each step a parameter is needed to store the ellipse parameter s at the contact point
        between ellipse and cirlce. This will be the PMAC variable Q220.
          }
      // 1.
      Add('Q215='+ FloatToStr(halfaxis_a));   {Q215: halfaxis a of the writing ellipse}
      Add('Q216='+ FloatToStr(halfaxis_b));   {Q216: halfaxis b of the writing ellipse}
      Add('Q217='+ FloatToStr(radius));       {Q217: radius of the resulting, written circle}
      Add('Q218='+ FloatToStr(circleSide*angular_step)); {Q218: angular step size, depends on resolution}
      Add('Q219='+ FloatToStr(-PI/2));        {Q219: current angular position w.r.r. circle center, changed in each step}
      Add('Q220=0');                          {Q220: ellipse parameter s, read at the beginning of the secion}
      Add('Q221=0');                          {Q221: x comp. of new ellipse center w.r.t. circle center}
      Add('Q222=0');                          {Q221: y comp. of new ellipse center w.r.t. circle center}
      Add('Q223=0');                          {Q223: x comp. of previous ellipse center w.r.t. circle center, called Kx}
      Add('Q224=0');                          {Q224: y comp. of previous ellipse center w.r.t. circle center, called Ky}
      Add('Q225=0');                          {Q225: Dummy variable to store a value temporarily in a different variable }
      Add('Q226='+ FloatToStr(Z_correct));    {Q226: Correction factor for the material when moving in z-direction}
      Add('Q227=1');                           {Q227: Sign for specifying the current direction of the line along y. }
      //2.
      LOn(5);
      //3.
      Add('I15=1'); {Switch from degree to radian}
      // Set initial ellipse parameter
      Add('Q0=Q215*COS(Q219)');
      Add('Q220=ATAN2(Q216*SIN(Q219))');         {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
      // Set Kx and Ky to initial value
      Add('Q223=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
      Add('Q224=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}

      // 4.
      Add('Q211=0'); { Q211 is an Iterative variable }
      Add('WHILE(Q211<'+IntToStr(angular_res)+')');
        //5.
        Add('X0 Y(Q227*'+FloatToStr(length)+') Z0' );
        // 6.  new angle position: current_pos + step
        Add('Q225=Q219+Q218');
        Add('Q219=Q225');
        // Set new ellipse parameter, s = Arctan2(b* Sin(phi), a* Cos(phi) )
        Add('Q0=Q215*COS(Q219)');
        Add('Q220=ATAN2(Q216*SIN(Q219))');
        // Calculate new (Kx,Ky) values for new angle position
        Add('Q221=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
        Add('Q222=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
        // 7.
        MotionStyle(True, True);  { Motion is set to linear relative }
        add('X(Q221-Q223) Y0 Z(-Q226*(Q222-Q224));');        {Q226 is the correction due to material effect}
        // 8. set new (Kx,Ky) to old (Kx, Ky)
        Add('Q223=Q221');
        Add('Q224=Q222');
        Add('Q211=Q211+1;'); { increment counter }
        Add('Q227=-Q227');  {switch direction of writing along y}
      Add('ENDWHILE;'); { end while loop }
      
      // Top point
      Add('X0 Y(Q227*'+FloatToStr(length)+') Z0' );
      // 9.
      LOff(0);
      MotionStyle(True, True);  { Motion is set to linear relative }
      add('X0 Y0 Z(2*Q226*(Q216+Q217) );');       {Q226 is the correction due to material effect}
      end;
    // TODO: Position at end

end;

{ Max Kellermeier, 18.07.2017

   The function "Circle_in_plane" writes the contour of a circle in the x-z-plane. But for detaching the surrounding bulk from the substrate a larger area has to be written. This function fills the area between the circle and an enveloping rectangle. Since the y coordinate remains unchanged it's an effectively two-dimensional routine. The resulting rectangle is 2*(radius+halfaxis_a) wide in x-direction and 2*(radius+halfaxis_b) tall in z-direction.
   REMARK: Make sure that the laser is initially placed correctly,  namely at the bottom of the circle.
   
   The Parameters are the same as for "Circle_in_plane".
   
   This is an effectively 2d functions. Stepping over the third dimension and looping over this function is time consuming
}
procedure rectangle_bounding_circle(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; startFromTop : boolean; initialLOn : boolean);
var
    cur_angle : double;
    angular_step : double;
    rect_bound : double;
begin
    // Logic structure:
    // Start from bottom
    // 1. Move to the bottom left corner of the rectangle
    // 2. Draw bottom line and update dynammic variables
    // 3. Start iterating over angles of the circle
    // 4. Calculate new K coordinates
    // 5. Step in z-dir by difference between new and old K_z
    // 6. Draw line on one side of the circle, switch off laser, move across the circle, switch on laser, and draw the line on the other side of the circle
    // 7. change sign for the drawing direction along x and save old K coordinates
    // 8. Loop until the top end is reached
    // 9. Switch off laser and move back to initial position 
    
    with PMAC_Buffer do
    begin
      
      // Initialize static variables
      Add('Q215='+ FloatToStr(halfaxis_a));   {Q215: halfaxis a of the writing ellipse}
      Add('Q216='+ FloatToStr(halfaxis_b));   {Q216: halfaxis b of the writing ellipse}
      Add('Q217='+ FloatToStr(radius));       {Q217: radius of the resulting, written circle}
      Add('Q218='+ FloatToStr(Pi/angular_res)); {Q218: angular step size, depends on resolution}
      Add('Q219='+ FloatToStr(-PI/2));        {Q219: current angular position w.r.r. circle center, changed in each step}
      Add('Q220=0');                          {Q220: ellipse parameter s, read at the beginning of the secion}
      Add('Q221=0');                          {Q221: x comp. of new ellipse center w.r.t. circle center}
      Add('Q222=0');                          {Q221: y comp. of new ellipse center w.r.t. circle center}
      Add('Q223=0');                          {Q223: x comp. of previous ellipse center w.r.t. circle center, called Kx}
      Add('Q224=0');                          {Q224: y comp. of previous ellipse center w.r.t. circle center, called Ky}
      Add('Q225=0');                          {Q225: Dummy variable to store a value temporarily in a different variable }
      Add('Q226='+ FormatFloat('0.000',Z_correct));    {Q226: Correction factor for the material when moving in z-direction}
      Add('Q227=1');                           {Q227: Sign for specifying the current direction of the line along x. }
      Add('Q228='+FormatFloat('0.000', radius+halfaxis_a ));     {Q228: half length of the enclosing rectangle}
      
      // initialize dynamic variables
      Add('I15=1'); {Switch from degree to radian}
      // Set initial ellipse parameter
      Add('Q0=Q215*COS(Q219)');
      Add('Q220=ATAN2(Q216*SIN(Q219))');         {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
      // Set Kx and Ky to initial value
      Add('Q223=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
      Add('Q224=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}

      // 1.
      MotionStyle(True, True);      { Motion is set to linear relative }
      Add('X(-Q228) Y0 Z0;');       { mrel(-rect_bound, 0, 0); }

      //2.
      LOn(5);
      Add('X(Q227*(Q228-Q223) ) Y0 Z0;');    { mrel(sign * (rect_bound-K_x_cur), 0, 0); }
      LOff(5);
      Add('X( Q227*2*Q223) ) Y0 Z0;');
      LOn(5);
      Add('X(Q227*(Q228-Q223)) Y0 Z0;');
      
      Add('Q227=-Q227;');       { sign := -sign; }
      
      // 3.
      Add('Q211=0'); { Q211 is an Iterative variable }
      Add('WHILE(Q211<'+IntToStr(angular_res)+')');
        //4.
        { Add('X0 Y(Q227*'+FloatToStr(length)+') Z0' ); }
        // new angle position: current_pos + step
        Add('Q219=Q219+Q218;');
        // Set new ellipse parameter, s = Arctan2(b* Sin(phi), a* Cos(phi) )
        Add('Q0=Q215*COS(Q219)');
        Add('Q220=ATAN2(Q216*SIN(Q219))');
        // Calculate new (Kx,Ky) values for new angle position
        Add('Q221=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
        Add('Q222=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
        //5.
        Add('X0 Y0 Z(-Q226*(Q222-Q224));');        {Q226 is the correction due to material effect}
        //6.
        Add('X( Q227*(Q228 - Q221) ) Y0 Z0;');
        LOff(0);
        Add('X( Q227*2* Q221) ) Y0 Z0;');
        LOn(0);
        Add('X( Q227*(Q228 - Q221) ) Y0 Z0;');
        //7.
        Add('Q227=-Q227;');  {switch direction of writing along y}
        Add('Q223=Q221;');
        Add('Q224=Q222;');
        // 8. set new (Kx,Ky) to old (Kx, Ky)
        Add('Q211=Q211+1;'); { increment counter }
      Add('ENDWHILE;'); { end while loop }
      
      // 9.
      LOff(0);
      MotionStyle(True, True);  { Motion is set to linear relative }
      Add('X(Q227* Q228) Y0 Z(2*Q226*(Q216+Q217) );');       {Q226 is the correction due to material effect} 
      end;

      {--- TODO ---}

end;

{ Max Kellermeier, 21.07.2017,
    Desc. needed
    Sets the variables Q215-Q227
}
procedure initializeQVariable(halfaxis_a, halfaxis_b, radius : double; angular_res: integer);
begin
    with PMAC_Buffer do
    begin
      Add('Q215='+ FloatToStr(halfaxis_a));   {Q215: halfaxis a of the writing ellipse}
      Add('Q216='+ FloatToStr(halfaxis_b));   {Q216: halfaxis b of the writing ellipse}
      Add('Q217='+ FloatToStr(radius));       {Q217: radius of the resulting, written circle}
      Add('Q218='+ FloatToStr(Pi/angular_res)); {Q218: angular step size, depends on resolution}
      Add('Q219='+ FloatToStr(-PI/2));        {Q219: current angular position w.r.r. circle center, changed in each step}
      Add('Q220=0');                          {Q220: ellipse parameter s, read at the beginning of the secion}
      Add('Q221=0');                          {Q221: x comp. of new ellipse center w.r.t. circle center}
      Add('Q222=0');                          {Q221: y comp. of new ellipse center w.r.t. circle center}
      Add('Q223=0');                          {Q223: x comp. of previous ellipse center w.r.t. circle center, called Kx}
      Add('Q224=0');                          {Q224: y comp. of previous ellipse center w.r.t. circle center, called Ky}
      Add('Q225=0');                          {Q225: Dummy variable to store a value temporarily in a different variable }
      Add('Q226='+ FloatToStr(Z_correct));    {Q226: Correction factor for the material when moving in z-direction}
      Add('Q227=1;');                         {Q227: sign, specifying whether on pos. or neg. side along the x-axis}   
    end;
end;

{ Max Kellermeier, 21.7.2017 
    This function is based on "Circular_Hole_in_plane_filled". While "Circular_Hole_in_plane_filled" writes a two-dimensional object, the circle, this function creates a cylinder, including the third dimension right from the beginning.
    The parameters are similar to those from "cylinder2" since this is basically a cylinder of air instead of glass.
    
    Assumptions: 
        The initial position is at the bottom of the resulting cylinder.
        The laser is off before start.
    
    Parameters:
        ...
        plane_res: resolution of the plane along the x-axis, the stepping direction in the xy-plane. If the resolution is too low the result will be a lattice of parallel lines along the y-axis
    
    Q211, Q212: loop variables
    Q228: Number of steps along x for the plane in xy at a fixed height (Maybe changed to M49)
    Q229: Residual to step to achieve the plane width after stepping by step_size_x. Used at a fixed angle.
        -> plane_width = Q228*step_size_x + Q229
    
}
procedure Cylindrical_Hole_in_plane_filled(length, radius, halfaxis_a, halfaxis_b : double; angular_res : integer; step_size_x: double);
begin
{procedure body}
    {--- TODO ---}
    // Logic structure
    // 1. Initially, the spot is positioned outside the circle. Move it inside
    // 2. Switch on laser
    // 3. step along the full circular arc counterclockwise
    // 4. repeat steps from [-pi/2, pi/2] (inclusively) via phi=0
    // 5. switch off laser
    // 6. place ellipse at bottom
    // 7. switch on laser
    // 8. step along circular arc clockwise
    // 9. repeat steps over [pi 3/2, pi/2[ (exclusively)
    // 10. Stop at point in front of top point
    // 11. Switch off laser
    // 12. Move to top point, initial position 
    
    with PMAC_Buffer do
    begin
      { angular_step := Pi/angular_res; }
      { The parametrization of a ellipse is (a*cos(s) , b*sin(s) ) where the parameter s goes from 0 to 2 pi. In each step a parameter is needed to store the ellipse parameter s at the contact point between ellipse and cirlce. This will be the PMAC variable Q220.}
      
      initializeQVariable(halfaxis_a, halfaxis_b, radius, angular_res);
      Add('Q228=0;');        {plane_res: number of points to step along x of step width 'step_size_x' to construct the plane in xy}
      Add('Q229=0;');        {residual step to achieve the requested plane width}
      MotionStyle(True, True);  { Motion is set to linear relative }

      // 1. 
      { if initialLOn then LOff(5); }
      mrel(0,0, +2*halfaxis_b);  
      // 2.
      LOn(5);

      Add('I15=1'); {Switch from degree to radian}
      Add('X0 Y'+ FloatToStr(length) +  ' Z0;');
      Add('X0 Y(-'+ FloatToStr(length) +  ') Z0;');
      // Set initial ellipse parameter
      Add('Q0=Q215*COS(Q219)');
      Add('Q220=ATAN2(Q216*SIN(Q219))');         {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
      // Set Kx and Ky to initial value
      Add('Q223=-Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
      Add('Q224=-Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
      
      Add('Q225=Q219+Q218');
      Add('Q219=Q225');             {cur_angle := cur_angle + angular_step}
      Add('Q0=Q215*COS(Q219)');
      Add('Q220=ATAN2(Q216*SIN(Q219))');         {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
      // Set new Kx and Ky to initial value
      Add('Q221=-Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x_cur := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
      Add('Q222=-Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y_cur := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
      Add('X(Q227*(Q221-Q223)) Y0 Z(-Q226*(Q222-Q224));');        {mrel(sign*(K_x_cur - K_x_old), 0, K_y_cur - K_y_old );}
      Add('Q227=-Q227;') ;                                {sign := -sign}  

      // 4.
      Add('Q211=1'); { Q211 is an Iterative variable }
      { start loop for tube here }
      Add('WHILE(Q211<'+IntToStr(angular_res)+')');
        Add('Q229=(2*Q221)%' + FormatFloat('0.000', step_size_x) + ';');
        // Add('Q228=FTOI(2*Q229/'+ FormatFloat('0.000', step_size_x) + ');');
        Add('M49=(2*Q221)/'+ FormatFloat('0.000', step_size_x)+';');
        Add('IF(Q229!< ('+ FormatFloat('0.000', step_size_x)+'/2) )');
        //Add('Q228=Q228-1;')
        Add('M49=M49-1;');
        Add('ENDIF;');
        
        { horizontalPlane(2* K_x_cur, length, plane_res, -sign ); }
        { -for horizontalPlane- }
        //first line
        Add('X0 Y('+ FloatToStr(length) +  ') Z0;');
        Add('X0 Y(-'+ FloatToStr(length) +  ') Z0;');  
        Add('Q212=0;');
        Add('WHILE(Q212<M49)');
        { RepeatWhile(plane_res); }
            // stepsize_x := -sign *2* K_x_cur/resolution_x
            Add('X(Q227*'+ FormatFloat('0.000', step_size_x) +' ) Y0 Z0;');
            { mrel(stepsize_x, 0, 0); }
            Add('X0 Y'+ FloatToStr(length) +  ' Z0;');
            Add('X0 Y(-'+ FloatToStr(length) +  ') Z0;');  
            Add('Q212=Q212+1;');
        Add('ENDWHILE;');
        Add('X(Q227*Q229) Y0 Z0;');
        Add('X0 Y'+ FloatToStr(length) +  ' Z0;');
        Add('X0 Y(-'+ FloatToStr(length) +  ') Z0;');  
        { -end horizontalPlane- }
        
        // set new (Kx,Ky) to old (Kx, Ky)
        Add('Q223=Q221');
        Add('Q224=Q222');
        
        Add('Q225=Q219+Q218');                              { cur_angle := cur_angle + angular_step; }
        Add('Q219=Q225');
        // Set new ellipse parameter
        Add('Q0=Q215*COS(Q219)');
        Add('Q220=ATAN2(Q216*SIN(Q219))');                  {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
        // Calculate new (Kx,Ky) values for new angle position
        Add('Q221=-Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
        Add('Q222=-Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) }

        // 3.
        Add('X(Q227*(Q221-Q223)) Y0 Z(-Q226*(Q222-Q224));');        {mrel(sign*(K_x_cur - K_x_old), 0, K_y_cur - K_y_old );}
        Add('Q211=Q211+1;'); { increment counter }
        Add('Q227=-Q227;') ;                                {sign := -sign}        
      Add('ENDWHILE;'); { end while loop }
      Add('X0 Y'+ FloatToStr(length) +  ' Z0;');
      Add('X0 Y(-'+ FloatToStr(length) +  ') Z0;'); 
      // 5.
      LOff(0);
      mrel(0,0, +2*halfaxis_b);
      mrel(0,0, -2*(radius+halfaxis_b)); 
    end;
end;

{ Max Kellermeier, 26.7.2017 }
procedure halfCylinder_in_plane_w_bulk_etching(length: double; lattice_const, radius : double; halfaxis_a, halfaxis_b: double; angular_resolution: integer; sign_x_dir: integer);
var
    cur_plane_x_width : double;
    cur_angle : double;
    
    function ellipse_s(phi: double) : double;
    begin ellipse_s := Arctan2(halfaxis_b* Sin(phi), halfaxis_a* Cos(phi) ) end;

    {x component of the ellipse's center. Expressed in terms of the angle to the contact point phi}
    function K_x(phi : double) : double ;
    begin K_x := halfaxis_a*Cos( ellipse_s(phi) ) + radius * Cos(phi) end;

    {y component of the ellipse's center. Expressed in terms of the angle to the contact point phi}
    function K_y(phi : double) : double ;
    begin K_y := halfaxis_b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;
begin
// Logic structure
    // 1. Start from bottom of one circle
    // 2. Switch on laser
    // 3. step along circular arc in positive direction over [-pi/2, pi/2]
    // 4. Calculate the size of the plane between cylinder at the current height
    // 5. draw plane between the cylinders: pos. x, pos. y, neg. x, neg. y
    // 6. repeat until the top is reached
    // 7. Switch off laser
    // 8. Move to bottom
    // 9. Switch on laser
    //
    
    with PMAC_Buffer do
    begin
      { The parametrization of a ellipse is (a*cos(s) , b*sin(s) ) where the parameter s goes from 0 to 2 pi.
        In each step a parameter is needed to store the ellipse parameter s at the contact point
        between ellipse and cirlce. This will be the PMAC variable Q220.
          }
    initializeQVariable(halfaxis_a, halfaxis_b, radius, angular_res);          MotionStyle(True, True);  { Motion is set to linear relative }
    
    
    Add('Q228=1;');                         {Q228: cur_plane_x_width, plane width of the current layer in x direction }
    Add('Q229='+ FormatFloat('0.000', lattice_const) + ';' );   {Q229: lattice_const, distance between the rods}
         
      //2.
      LOn(5);
      Add('I15=1'); {Switch from degree to radian}
      // Set initial ellipse parameter
      Add('Q0=Q215*COS(Q219)');
      Add('Q220=ATAN2(Q216*SIN(Q219))');         {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
      // Set Kx and Ky to initial value
      Add('Q223=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
      Add('Q224=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}

      // 3.
      Add('Q211=0'); { Q211 is an Iterative variable }
      Add('WHILE(Q211<'+IntToStr(angular_resolution)+')');
        // 4.
        { cur_plane_x_width := lattice_const - 2* Abs(K_x(cur_angle) ) ; }
        // In loop
        Add('Q228=Q229 - 2*ABS(Q223);');        
        // 5.
        { horizontalPlane(cur_plane_x_width, length, plane_resolution , sign_x_dir ); }
        horizontalPlane_w_QVars('Q228', FormatFloat('0.000',length), plane_resolution,  IntToStr(sign_x_dir) );
        
        
        // 6.  new angle position: current_pos + step
        Add('Q225=Q219+Q218');
        Add('Q219=Q225');
        // Set new ellipse parameter, s = Arctan2(b* Sin(phi), a* Cos(phi) )
        Add('Q0=Q215*COS(Q219)');
        Add('Q220=ATAN2(Q216*SIN(Q219))');
        // Calculate new (Kx,Ky) values for new angle position
        Add('Q221=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
        Add('Q222=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
        
        MotionStyle(True, True);  { Motion is set to linear relative }
        add('X(' + IntToStr(sign_x_dir) + '(Q221-Q223)) Y0 Z(-Q226*(Q222-Q224));');        {Q226 is the correction due to material effect}
        // 8. set new (Kx,Ky) to old (Kx, Ky)
        Add('Q223=Q221');
        Add('Q224=Q222');
        Add('Q211=Q211+1;'); { increment counter }
      Add('ENDWHILE;'); { end while loop }
      
      // Top point
      Add('Q228=Q229 - 2*ABS(Q223);');        
      horizontalPlane_w_QVars('Q228', FormatFloat('0.000',length), plane_resolution,  IntToStr(sign_x_dir) );
            
      LOff(0);
      // initial position
      MotionStyle(True, True);  { Motion is set to linear relative }
      add('X0 Y0 Z(2*Q226*(Q216+Q217) );');       {Q226 is the correction due to material effect}
      end;
    
    
    end;
end;


{ Max Kellermeier, 30.7.2017
  location : integer
    either +1 or -1. specifying whether the bottom half or the top half should be written.
  sign_x_dir: integer
  either +1 or -1. Specifies whether to write from left to right or from right to left, assuming to be positioned at the bottom of one cylinder
}
procedure TLFS_Converter.bulk_etching_between_bottom_half_cylinders(length: double; lattice_const, radius : double; halfaxis_a, halfaxis_b: double; angular_resolution: integer; plane_resolution : integer; sign_x_dir: integer; location : integer);
var
    cur_plane_x_width : double;
    cur_angle : double;
    cur_wall_height : double;

    function ellipse_s(phi: double) : double;
    begin ellipse_s := Arctan2(halfaxis_b* Sin(phi), halfaxis_a* Cos(phi) ) end;

    {x component of the ellipse's center. Expressed in terms of the angle to the contact point phi}
    function K_x(phi : double) : double ;
    begin K_x := halfaxis_a*Cos( ellipse_s(phi) ) + radius * Cos(phi) end;

    {y component of the ellipse's center. Expressed in terms of the angle to the contact point phi}
    function K_y(phi : double) : double ;
    begin K_y := halfaxis_b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;
begin
    // Logic structure
    // 1. Start from bottom of one circle
    // 2. Switch on laser
    // 3. step along circular arc in positive direction over [-pi/2, pi/2]
    // 4. Calculate the size of the plane between cylinder at the current height
    // 5. draw plane between the cylinders: pos. x, pos. y, neg. x, neg. y
    // 6. repeat until the top is reached
    // 7. Switch off laser
    // 8. Move to bottom
    // 9. Switch on laser
    //

    initializeQVariable(halfaxis_a, halfaxis_b, radius, angular_resolution);
    MotionStyle(True, True);  { Motion is set to linear relative }
    // PMAC_Buffer.Add('LINEAR;');


    with PMAC_Buffer do
    begin
      if location=1 then
      begin
        {Print bottom half}
        Add('Q219='+ FormatFloat('0.000',-PI/2)+';');        {Q219: current angular position w.r.r. circle center, changed in each step}
      end
      else if location=-1 then
      begin
        {Print top half}
        if sign_x_dir=1 then Add('Q219='+FormatFloat('0.000',0)+';')      {Q219: current angular position w.r.r. circle center, changed in each step}
          else Add('Q219='+ FormatFloat('0.000',PI)+';');
        mrel(sign_x_dir*(radius + halfaxis_a), 0, radius+halfaxis_b);
      end;

      if sign_x_dir=1 then Add('Q218='+ FloatToStr(0.5* Pi/angular_resolution)) {Q218: angular step size, depends on resolution}
      else if sign_x_dir=-1 then Add('Q218='+ FloatToStr(-0.5* Pi/angular_resolution));

    Add('Q228=1;');                         {Q228: cur_wall_height, wall height of the current layer in z direction }
    Add('Q229='+ FormatFloat('0.000', lattice_const) + ';' );   {Q229: lattice_const, distance between the rods}

      //2.
      LOn(5);
      Add('I15=1'); {Switch from degree to radian}
      // Set initial ellipse parameter
      Add('Q0=Q215*COS(Q219)');
      Add('Q220=ATAN2(Q216*SIN(Q219))');         {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
      // Set Kx and Ky to initial value
      Add('Q223=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
      Add('Q224=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}

      // 3.
      // ChangePol(180);
      Add('Q211=0'); { Q211 is an Iterative variable }
      Add('WHILE(Q211<'+IntToStr(angular_resolution)+')');
        // 4.
        // In loop
        { cur_wall_height := (radius+halfaxis_b) - Abs(K_y(cur_angle)); }
        Add('Q228= Q217+Q216 - ABS(Q224);');
        // 5.
        { if location = 'bottom' then mrel(0,0,-cur_wall_height); }
        if location = 1 then add('X0 Y0 Z(Q226*Q228);');
         // verticalPlaneByStep---
        // step size= 2*halfaxis_b, 2*Q216
        // residual_step :=height - stepsize_z*vertical_res;
        Add('Q230= Q228 % (2*Q216) ;');
        // vertical_res := Floor(height/stepsize_z );
        Add('M49=Q228/(2*Q216);');
        Add('IF(Q230!< Q216 )');
        Add('M49=M49-1');
        Add('ENDIF;');
        LOn(0);
        add('X0 Y('+FormatFloat('0.000',length)+') Z0;');
        add('X0 Y(-'+FormatFloat('0.000',length)+') Z0;');
        Add('Q231=0'); { Q231 is an Iterative variable }
        Add('WHILE(Q231< M49)');
            add('X0 Y0 Z(-Q226*2*Q216);');
            add('X0 Y('+FormatFloat('0.000',length)+') Z0;');
            add('X0 Y(-'+FormatFloat('0.000',length)+') Z0;');

            Add('Q231=Q231+1;'); { increment counter }
        Add('ENDWHILE;'); { end while loop }
        add('X0 Y0 Z(-Q226*Q230);') ;
        add('X0 Y('+FormatFloat('0.000',length)+') Z0;');
        add('X0 Y(-'+FormatFloat('0.000',length)+') Z0;');
        LOff(0);
        { mrel(0,0, -stepsize_z*vertical_res- residual_step); }
        add('X0 Y0 Z(Q226*(2*Q216*M49 + Q230));');
        // verticalPlaneByStep--- end
        { if location = 'bottom' then mrel(0,0,cur_wall_height); }
        if location = 1 then add('X0 Y0 Z(-Q226*Q228);');



        // 6.  new angle position: current_pos + step
        Add('Q225=Q219+Q218');
        Add('Q219=Q225');
        // Set new ellipse parameter, s = Arctan2(b* Sin(phi), a* Cos(phi) )
        Add('Q0=Q215*COS(Q219)');
        Add('Q220=ATAN2(Q216*SIN(Q219))');
        // Calculate new (Kx,Ky) values for new angle position
        Add('Q221=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
        Add('Q222=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}



        add('X((Q221-Q223)) Y0 Z(-Q226*(Q222-Q224));');        {Q226 is the correction due to material effect}
        // 8. set new (Kx,Ky) to old (Kx, Ky)
        Add('Q223=Q221');
        Add('Q224=Q222');
        Add('Q211=Q211+1;'); { increment counter }
      Add('ENDWHILE;'); { end while loop }

      // Top point
      // Top point
      Add('Q228= Q217+Q216 - ABS(Q224);');
        // 5.
        { if location = 'bottom' then mrel(0,0,-cur_wall_height); }
        if location = 1 then add('X0 Y0 Z(Q226*Q228);');
        // verticalPlaneByStep---
        // step size= 2*halfaxis_b, 2*Q216
        // residual_step :=height - stepsize_z*vertical_res;
        Add('Q230= Q228 % (2*Q216) ;');
        // vertical_res := Floor(height/stepsize_z );
        Add('M49=Q228/ (2*Q216);');
        Add('IF(Q230!< Q216 )');
        Add('M49=M49-1');
        Add('ENDIF;');
        LOn(0);

        add('X0 Y('+FormatFloat('0.000',length)+') Z0;');
        add('X0 Y(-'+FormatFloat('0.000',length)+') Z0;');

        Add('Q231=0'); { Q231 is an Iterative variable }
        Add('WHILE(Q231<M49)');
            add('X0 Y0 Z(-Q226*2*Q216);');
            add('X0 Y('+FormatFloat('0.000',length)+') Z0;');
            add('X0 Y(-'+FormatFloat('0.000',length)+') Z0;');

            Add('Q231=Q231+1;'); { increment counter }
        Add('ENDWHILE;'); { end while loop }
        add('X0 Y0 Z(-Q226*Q230);');
        add('X0 Y('+FormatFloat('0.000',length)+') Z0;');
        add('X0 Y(-'+FormatFloat('0.000',length)+') Z0;');


        LOff(0);
        { mrel(0,0, -stepsize_z*vertical_res- residual_step); }
        add('X0 Y0 Z(Q226*(2*Q216*M49 + Q230));');
        // verticalPlaneByStep--- end
        { if location = 'bottom' then mrel(0,0,cur_wall_height); }
        if location = 1 then add('X0 Y0 Z(-Q226*Q228);');

      LOff(0);
      // initial position

      if location=1 then
      begin
        add('X0 Y0 Z(Q226*(Q216+Q217) );');       {Q226 is the correction due to material effect}
        mrel(-sign_x_dir*(radius + halfaxis_a), 0, 0);
      end
      else if location=-1 then
      begin
         add('X0 Y0 Z(2*Q226*(Q216+Q217) );');       {Q226 is the correction due to material effect}
      end;

    end;
end;




{Copied from Converter_LFS_PMAC}
{
    Draw a Rectangle with size 
            sqrt(VectX^2 + VectY^2)
         x (m*stepW) 
         x (n*stepH)
    The vector (VectX, VectY) gives also the orientation. It describes the side from the bottom left corner to the bottom right corner.
    planeOrientation: integer
        Only the sign matters. If positive, the planes are drawn horizontally. If negative, they are vertically aligned. 0 is not a valid parameter.
}
procedure RectRel_2(VectX, VectY:double; m : integer; stepW : double; n :integer; stepH: double; planeOrientation : integer);
var
  i,j     : integer;
 X,Y : double;
 alpha   : double;
 horizontal: boolean;
 outerLoop : integer; 
 innerLoop: integer;;

    procedure doLineAndStep();
    begin
        mrel(VectX,VectY,0);      { Each lines is double-exposed } 
        mrel(-VectX,-VectY,0);
        if horizontal then mrel(X,Y,0) else mrel(0,0,stepH);
    end;
    procedure endSectionOfPlane();
    begin
        mrel(VectX,VectY,0);      // Each lines is double-exposed 
        mrel(-VectX,-VectY,0);
        if horizontal then
        begin
            mrel(-m*X, -m*Y,0);
            mrel(0,0,stepH);
        end
        else
        begin
            mrel(0,0,-n*stepH);
            mrel(X,Y,0);
        end;        
    end;
    
begin
  alpha :=Arctan2(VectY, VectX);
  if planeOrientation > 0 then
    ChangePol( Round( radtodeg(alpha) ) ) // parallel polarization
  else if planeOrientation < 0 then
    ChangePol( Round( radtodeg(alpha) ) + 90 ); // perpendicular polarization
  horizontal := planeOrientation > 0;
  
  X:=-stepW*sin(Alpha); { calculate the step in the local coordinate frame }
  Y:=stepW*cos(Alpha);
  LOn(5);
  
  if horizontal then
  begin
    innerLoop := m;
    outerLoop := n;
  end
  else
  begin
    innerLoop := n;
    outerLoop := m;
  end;
  
  { outerLoop := n; }
  { innerLoop := m; }
  
  RepeatWhile(outerLoop);
  
  { for j := 0 to n-1 do }
  { begin }
      RepeatWhile(innerLoop);  
      { for I := 0 to m-1 do }
      { begin }
        doLineAndStep();   
      { end; }
      EndRepeat;
      endSectionOfPlane();
      { mrel(VectX,VectY,0);      { Each lines is double-exposed }  }
      { mrel(-VectX,-VectY,0); }
      { mrel(-m*X, 0,0); }
      { mrel(0,0,stepH); }
  { end; }
  EndRepeat;
  
  RepeatWhile(innerLoop);  
  { for I := 0 to m-1 do }
  { begin }
      doLineAndStep();
  EndRepeat;
  { end; }
  mrel(VectX,VectY,0);      { Each lines is double-exposed } 
  mrel(-VectX,-VectY,0);
          
  LOff(5);
  mrel(-m* X, -m*Y,- n*stepH);
end;




end.
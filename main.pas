program Main;
uses pbgStructureComponents, helperTLFS;
var
    startFromTop : boolean;
    i : integer;
    length, lattice_const, radius,  halfaxis_a, halfaxis_b, x_dist : double;
    angular_resolution, etching_angular_res, sign, etching_bulk_inlayer_res_x, etching_bulk_inlayer_res_z : integer;
    etching_rect_res_x, etching_rect_res_z, num_cells : integer; 
    width, gap_between_layers, y_dist : double;
    angular_resolution_hole : integer;
    step_size_x : double;
begin
  DEBUG := FALSE;
  x_l := 0.0;
  y_l := 0.0;
  z_l := 0. ;
  startFromTop := False;

  
  
  // draw_Hexagonal_Wvg(lattice_const, radius : double; num_cells: integer; halfaxis_a, halfaxis_b: double; angular_resolution: integer);
  { draw_Hexagonal_Wvg(100, 30, 2, 2,10, 4); }

    // Hexagonal_wvg_lower_etch_boundary(lattice_const, radius : double; num_cells: integer; halfaxis_a, halfaxis_b: double)
    { Hexagonal_wvg_lower_etch_boundary(100, 30, 2, 2, 10);  }
    { draw_Hexagonal_Wvg(100, 30, 2, 2,10, 4); }
    
    LOn(0);
    LOff(0);
    LOn(0);
    LOff(0);
    
    
    { Circle_in_plane(25, 2, 10, 4, False, False); }
    { mrel(100,0,0); }
    { Circle_in_plane(25, 2, 10, 4, False, False); }
    
    // cylinder2(length, radius, a_halfaxis, halfaxis_b : double; angular_res,  y_resolution : integer);
    { mrel(0,0,0); }
    { cylinder2(100, 25, 2, 10, 16, 10); }
    
        
    { Hexagonal_wvg_w_bulk_etching(length: double; lattice_const, radius : double; num_cells: integer; halfaxis_a, halfaxis_b: double; angular_resolution: integer; startFromTop : boolean) }
     { Hexagonal_wvg_w_bulk_etching(Double;Double;Double;SmallInt;Double;Double;SmallInt;SmallInt;Boolean); }
    { Hexagonal_wvg_w_bulk_etching(30, 100 , 25, 3, 2, 10, 4, True);  }
    { Circle_in_plane(25, 2, 10, 4, ); }
    
    { Circular_Hole_in_plane_filled(25, 1, 4, 64, False, False); }

    { halfCylinder_in_plane_w_bulk_etching(200, 100, 25, 2, 10, 16, 10, 1); }
    
    // RectRel(VectX, VectY:double; m : integer; stepW : double; n :integer; stepH: double)
    { RectRel(0,30, 5, 2, 10, 5); }
    { mrel(20,0,0); }
    
    { mrel(0, 0, -200); }
    { Hexagonal_wvg_w_bulk_etching(1000, 200, 50, 1, 1, 4, 5, 16, 2); }
    
    { HEXAGONALWVGBULKETCHED 2000, 100, 20, 3, 1, 4, 10, 32, 3;  }
    // This should be the configuration for the plattform
    { Hexagonal_wvg_w_bulk_etching(1000, 100, 25, 3, 1, 4, 50, 32, 2); }
    
    
     { procedure bulk_etching_between_bottom_half_cylinders_vertically(length: double; lattice_const, radius : double; halfaxis_a, halfaxis_b: double; angular_resolution: integer; plane_resolution : integer; sign_x_dir: integer;  location : String ); }

    // Testing new bulk etching procedure
    { cylinder2(1000, 25, 1, 4, 4);   }
    { bulk_etching_between_bottom_half_cylinders_vertically(1000, 100, 25, 1,4, 4, 1, 'bottom');  }
    { bulk_etching_between_bottom_half_cylinders_vertically(1000, 100, 25, 1,4, 4, 1, 'top');  }
    { bulk_etching_between_bottom_half_cylinders_vertically(1000, 100, 25, 1,4, 4, -1, 'bottom');  }
    { bulk_etching_between_bottom_half_cylinders_vertically(1000, 100, 25, 1,4, 4, -1, 'top');  }
    { bulk_etching_between_bottom_half_cylinders_vertically(length: double; lattice_const, radius : double; halfaxis_a, halfaxis_b: double; angular_resolution: integer; sign_x_dir: integer; location : String ); }
    
    
    
    length:=1000;
    lattice_const:=100;
    radius:=25;
    halfaxis_a:=1.0;
    halfaxis_b:= 4.0;
    etching_angular_res:= 4;
    sign:=1;
    etching_bulk_inlayer_res_x := 20;
    etching_bulk_inlayer_res_z := 3 ;
    num_cells:=2;
   
    x_dist := lattice_const;
    y_dist := lattice_const*sqrt(3.0)/2;
    gap_between_layers := y_dist - 2*(radius+halfaxis_b);
    width := num_cells*2*lattice_const+ 2*(radius+halfaxis_a);
    etching_angular_res := 4;
    etching_rect_res_z := 2;
    etching_rect_res_x := 120; //225
    etching_bulk_inlayer_res_x := 2; 
    etching_bulk_inlayer_res_z := 7;
    angular_resolution_hole := 16;
    step_size_x :=5;
    
   
   
    //mrel(-50,0,0);
    
    Cylindrical_Hole_in_plane_filled(length, lattice_const - radius, halfaxis_a, halfaxis_b, angular_resolution_hole, step_size_x);
    
    // Comp 3
    { RectRel(0,length, etching_rect_res_x, sign*Round(width/etching_rect_res_x), etching_rect_res_z, gap_between_layers / (etching_rect_res_z), 1 ); }
          
    
    
    // Comp 2
    { bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   sign, 'bottom'); }
    { bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   -sign, 'bottom'); }
    { { cylinder2(length, radius, halfaxis_a, halfaxis_b, angular_resolution); } }
   { bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   sign, 'top'); }
    { bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   -sign, 'top'); }
    
    { mrel(sign*(radius+halfaxis_a),0,0  ); }
  
    
    { { { RectRel(0, length,  } } }
        { { { Round((x_dist - 2*(radius+halfaxis_a) )/step_size_x ), -sign*step_size_x, etching_rect_res_z, 2*(radius + halfaxis_b) / (etching_rect_res_z), -1 ); } } }
    { RectRel(0,length, etching_bulk_inlayer_res_x,  }
        { -sign*(x_dist - 2*(radius+halfaxis_a) )/etching_bulk_inlayer_res_x, etching_bulk_inlayer_res_z,  2*(radius + halfaxis_b) / (etching_bulk_inlayer_res_z) , -1 }
    { );  }
    { mrel(sign*(lattice_const -(radius+halfaxis_a)),0,0  ); }
    
    { bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   sign, 'bottom'); }
    { bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   -sign, 'bottom'); }
    { { cylinder2(length, radius, halfaxis_a, halfaxis_b, angular_resolution); } }
    { bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   sign, 'top'); }
    { bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   -sign, 'top'); }
    
    { mrel(sign*(radius+halfaxis_a),0,0  ); }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //RectRel
    { RectRel(0, 100, 10, 4, 10, 8, -1); }
        
    
    { PMAC_Buffer := PMAC.create(); }
    { With PMAC_Buffer do }
    { begin }
        { Add('Hello WOrld 3'); }
    { end; }
    
    { new etch paths: a rectangle covering the circle }
    { i :=0; }
    { while i < 10 do }
    { begin }
    { { rectangle_bounding_circle(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; startFromTop : boolean; initialLOn : boolean) } }
    
        { rectangle_bounding_circle(50, 1, 4, 16, false, false); }
        { mrel(0,5,0); }
        { i := i+1; }
    { end; }
    { mrel(100, 0, 0); }
   
    
    { cylinder2(200, 25, 2, 10, 32); }
      
    { halfCylinder_in_plane_w_bulk_etching(200, 100, 25, 2, 10, 16, 10, -1); }
    
    { mrel(-100,0,0); }
    { cylinder2(Double;Double;Double;Double;SmallInt);  }
    { LOn(0); }
    { mrel(0,0,30); }
    { LOff(0); }
end.




# Intermediate Results
In this folder the lfs scripts are presented which were used to write the different structures into the glass sample.

All used pascal procedures are stored in [pbgStructureComponents](../../blob/master/pbgStructureComponents.pas) resp. in [pbgStructureComponents_PMAC](../../blob/master/pbgStructureComponents_PMAC.pas) for the corresponding PMAC versions.

## Three free standing cylinders 
`Three_cylinders_cut_from_sample.lfs`
Here, three cylinders of 1 mm length and 100 µm diameter were written into a 1 mm thick sample. The glass cylinders are free standing except for one side at which they remain attached to the substrate. 
Additional planes and walls were added to make the written areas accessible for the acid and to remove the glass parts between the cylinders.

The same structure was written in the sample with a different strategy in which the bulk volume between the cylinders was completely exposed. This is not considered here for the moment. But the section exists in the lfs file and should be removed to have only one structure of three rods.

The lfs code
```
PERPCYLINDER2 1000, 50, 1, 4, 180;
```
uses the pascal procedure 
```pascal
procedure cylinder2(length, radius, halfaxis_a, halfaxis_b : double; angular_res : integer);
```
to draw a 1000 µm long cylinder with a radius of 50 µm, where the laser focal ellipse has halfaxes of 1 µm and 4 µm and the discretization of the circular cross section is two times 180.

Figure 1 and 2 show the simulated writing output without the vertical planes to cut the structure out of the sample. The third image is a photo of the actual structure after etching, taken with the microscope.

![3D_view_simulated](../img/three_cyl_figure_1.png)
![Crosssectional_view_simulated](../img/three_cyl_figure_2.png)
![Edgeview_photo](../img/three_cyl_edgeview_photo.jpg)


## Single cell waveguide structure/ Six rod hexagon
A hexagon of six rods with radius $r=25 \mu m$ was written in a thin sample ($500 \mu m$ thickness using the `HEXAGONALWVGBULKETCHED` LFS command. The file used is [six_rod_hexagon_in_thin_sample.lfs](../../blob/master/lfs/six_rod_hexagon_in_thin_sample.lfs), including also paths for the etchant.

The status of the `HEXAGONALWVGBULKETCHED` command is the one from the branch *bulk_etching_vertically* on the 30.08.2017. This means it didn't include the new writing procedure for the bulk between the rods. For reproducability the [pmc file](../../blob/master/six_rod_hexagon_in_thin_sample.pmc) is provided as well.

This is the resulting structure in cross sectional view after etching:
![Side_view_six_rods](../img/six_rods_wvg_sideview.jpg)


## Two cell waveguide structure
- Thin sample used: $500 \mu m$
- $ 50 \mu m $ radius
A hexagon of 18 rods with a center whole for waveguiding. The used LFS command was `HEXAGONALWVGBULKETCHED` from the branch *bulk_etching_vertically* on 30.08.2017. At that day, the bulk etching procedure for the glass between the rods within a layer was not implemented yet.

The LFS file  [2_cells_wvg_structure.lfs](../../blob/master/lfs/2_cells_wvg_structure.lfs) and the [pmc file](2_cells_wvg_structure.pmc) for reproducibility can be found in the *lfs* directory.

The resulting structure looks like the following:
![Side_view_2_cells_structure](../img/two_cell_wvg_sideview.jpg)


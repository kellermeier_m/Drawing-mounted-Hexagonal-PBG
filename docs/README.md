# Overview of implemented functions
This is a documentation of the functions implemented on the TLFS Converter used to write geometric patterns via Direct Laser Writing into a glass sample. Each function is given with its Pascal signature, the LFS command and an LFS example.


# Functions
## Circle
```pascal
procedure Circle_in_plane(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; startFromTop : boolean; initialLOn : boolean)
```
```
PERPCIRCLE radius, halfaxis_a, halfaxis_b, angular_res;
```
The circle function writes an approximated circle in the xz-plane, starting from the bottom with drawing each halfcircle separately. The routine compensates for the ellipsoidal shape of the focal spot.
The half circle is approximated by discrete angular steps.

**Parameters**
- *radius*: Radius of the circle
- *halfaxix_a*: assumed halfaxis of the focal spot in the direction transverse to the beam, the x-axis.
- *halfaxis_b*: assumed halfaxis of the focal spot in the longitudinal direction, the z-axis.
- *angular_res*: number of discrete angular steps over the half circle arc.
- *startFromTop*: 

Example:
```
PERPCIRCLE 50, 1, 4, 90;
```
draws a circle with a radius of 50 micro meter and a discretization of 90 steps for the half circular arc. The focal spot is assumed to have a 2 microns width and 8 microns length.

![Example image 'Circle in plane'](../img/docs_Circle.png)

Number in LFS Converter: 74

## Cylinder based on circle function
```pascal
procedure cylinder(length, radius, a_halfaxis, halfaxis_b : double;angular_res,  y_resolution : integer);
```
```
PERPCYLINDER length, radius, halfaxis_a, halfaxis_b, angular_res, y_resolution;
```
TODO
(Rather usefull for disks than long cylinders; combining steps over circle and linear steps along y very time consuming)

**Parameters**
- *length*: Length of the cylinder.
- *radius*: Radius of the cross sectional circle.
- *halfaxix_a*: assumed halfaxis of the focal spot in the direction transverse to the beam, the x-axis.
- *halfaxis_b*: assumed halfaxis of the focal spot in the longitudinal direction, the z-axis.
- *angular_res*: number of discrete angular steps over the half circle arc in xz.
- *y_resolution*: Number of steps into which the length along the y-axis shall be divided

Example:
```
PERPCYLINDER 20, 50, 1, 4, 90, 20;
```
The circle from the previous example is drawn repeatedly 20 times, linearly distributed over a distance of 20 microns. This forms a 20 micro meter thick disk of 100 micro meter width.

![../img/docs_Circle_based_Cylinder.png](../img/docs_Circle_based_Cylinder.png)

Number in LFS Converter: 75

## Circular hole
```pascal
procedure Circular_Hole_in_plane(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; startFromTop : boolean = False; initialLOn : boolean = False);
```
```
PERPCIRCULARHOLE radius, halfaxis_a, halfaxis_b, angular_res;
```
This function works almost like the cirlce function, but it draws a circle with the outer envelope of the illuminated pixels (ellipses). 

**Parameters**
The parameters are identical to the ones from the circle function.

```
PERPCIRCULARHOLE 50, 1, 4, 90;
```
draws a circle of 50 micro meter radius, which encloses the drawing ellipses. By removal of the remaining inner bulk a circular hole in the glass sample remains.

![../img/docs_Circular_Hole.png](../img/docs_Circular_Hole.png)

Number in LFS Converter: 79

## Hexagonal Pattern in 2D
```pascal
procedure draw_Hexagonal_Wvg(lattice_const, radius : double; num_cells: integer; halfaxis_a, halfaxis_b: double; angular_resolution: integer);
```
```
HEXAGONALWVG2D lattice_const, radius, num_cells, halfaxis_a, halfaxis_b, angular_resolution;
```
Starting from the center the full hexagon with hole is printed layer by layer in the x-z-plane. It starts at the bottom left corner if num_cells is even and from the bottom right corner otherwise. The center rod is written by the circular hole function. 
The result is only two dimensional.

**Parameters**
- *lattice_const*: distance between the cylindrical rods
- *radius*: radius of the each cylinder
- *num_cells*: Repetitions of the cylinders along the lattice basis vectors in the transverse direction.
            It is counted from the center of the structure in positive x- and y-direction as well as
            the negative directions
All other Parameters are given by the previous functions.

Example:
(TODO)


(Number in LFS Converter: 77)

## Hexagonal pattern stacked along y axis
```pascal
procedure hexagon_of_cylinders(length, lattice_const, radius, a_halfaxis, halfaxis_b : double; num_cells: integer; angular_res,  y_resolution : integer);
```
```
HEXAGONOFCYLINDERS ...;
```

In analogy of the `cylinder` function using the `circle` function, this function draws the hexagonal 2D pattern from `draw_Hexagonal_Wvg` repeatedly, stepped along the y-axis. Similar to the cylinder from stacked circles, the routine is very **inefficient**. 


(Number in LFS Converter: 78)

## Cylinder with lines along the zylinder axis
```pascal
procedure cylinder2(length, radius, halfaxis_a, halfaxis_b : double; angular_res: integer);
```
```
PERPCYLINDER2 length, radius, halfaxis_a, halfaxis_b, angular_res;
```
This cylinder routine does not use the `circle` function. Similar to the circle it steps over the half circular arc but after each step it draws a line along the y-axis.

Draw a cylinder with its axis perpendicular to the laser beam. The cylinder axis is oriented along the y-direction while the laser beam goes along the z-axis. The routine steps over the circular arc, draws a line along the y-direction, and continues with the next angular step.

The laser beam should be positioned on the bottom.

**Parameters**
- *length*: Length of the cylinder along y
- *radius*: Radius of the base circle
- *halfaxis_a*: halfaxis of ellipsoidal laser beam in transverse plane (x and y components)
- *halfaxis_b*: halfaxis of ellipsoidal laser beam in longitudinal direction (z component)
- *angular_res*: angular resolution for a half circle. The interval [-Pi/, Pi/2 ] is partitioned in
    *angular_res* subintervals. The laser beam moves linearly from one angle to the other.

Used Q-Variables:
Q0   - for Arctan2 function
Q211 - loop iterator
Q215-Q217 - constant Q variables, don't change while running the loops
Q218-Q227 - dynamic Q variables, change while stepping over the arc
variables for calculating the next step along the circle


Example:
```
PERPCYLINDER2 1000, 50, 1, 5, 16;
```
draws a cylinder of 1000 micro meter length and with a cross section of 50 micro meter radius, using a ellipse size of halfaxes 1 and 5 micro meter and a discretization of 16 angular steps.

![../img/docs_Cylinder2.png](../img/docs_Cylinder2.png)

Number in LFS Converter: 80

## Vertical Plane
```pascal
procedure verticalPlane(length, height : double; orientationX, orientationY: double; vertical_res: integer);
```
```
VERTPLANE length, height, orientationX, orientationY, vertical_res;
```

A vertical plane is written using horizontal lines. The plane is spanned by the z-direction and a horizontal vector *(x,y)* given as parameter. The routine draws a line in the xy-plane, steps in z, and continues with the next line until the desired height is reached.

It is assumed that the initial laser position is the bottom left corner. Of course this becomes the bottom right corner if the orientation is 180 degree. But it's important to keep in mind.

In difference to the existing *vertical plane* function not the step size is specified but rather the overall size. This allows non-integral stepsizes.

**Parameters**
- *length*: length of the lines in the horizontal plane
- *height*: height of the stacked lines in the vertical direction
- *orientationX*, *orientationY*: a 2d vector parallel to the plane. Only its direction is considered, not its magnitude.
- *vertical_res*: number of lines in the vertical direction.

Example:
```
VERTPLANE 120, 50, 1, 0, 5;
```
writes a vertical plane of 120 microns width and 50 microns height in the xz-plane. The wall consists of 5 lines, each separated from each other by 10 microns.

By changing the function call to `VERTPLANE 120, 50, -1, 0, 5;` the plane is placed to the left of the initial position.


![../img/docs_vertPlane.png](../img/docs_vertPlane.png)

Number in LFS Converter: 82

## Horizontal Plane
```pascal
procedure horizontalPlane(width_x, width_y : double; resolution_x: integer; sign_x_dir : integer = 1)
```
```
HORIZONTALPLANE width_x, width_y, resolution_x;
```
writes a plane in xy of given size using lines parallel to the y-axis. The sides are parallel to the axes. The function computes the stepsize based on the desired size and the number of lines.
It is assumed that the laser is initially positioned at the bottom left corner when projecting on the xy-plane. After finishing the plane the focus is moved back to the initial position.

**Parameters**:
- *width_x*: length of the plane in x-direction
- *width_y*: length of the plane in y-direction
- *resolution_x*: number of lines parallel to the y-axis, counted along the x-direction.

Example:
```
HORIZONTALPLANE 100, 250, 67;
```
draws 100 microns by 250 microns large plane parallel to the sample surface. The linespacing is `100/67=1.5` microns.

![../img/docs_horizontalPlane.png](../img/docs_horizontalPlane.png)
![../img/docs_horizontalPlane.png](../img/docs_horizontalPlane_zoomed.png)

Number in LFS Converter: 83

## Rectangle bounding Circle (TODO)
```pascal
procedure rectangle_bounding_circle(radius, halfaxis_a, halfaxis_b : double; angular_res: integer);
```
```
RECTANGLEENCLOSINGCIRCLE radius, halfaxis_a, halfaxis_b, angular_res);
```
The function "Circle_in_plane" writes the contour of a circle in the x-z-plane.
But for detaching the surrounding bulk from the substrate a larger area has to be written.
This function fills the area between the circle and an enveloping rectangle.
Since the y coordinate remains unchanged it's an effectively two-dimensional routine. The code for moving along y is implemented but commented out.
 
The resulting rectangle is *2 (radius+halfaxis_a)* wide in x-direction and *2 (radius+halfaxis_b)* tall in z-direction.
It is assumed that the laser is initially switched off.

 
REMARK: Make sure that the laser is initially placed correctly,  namely at the bottom of the circle.

**Parameters**
The Parameters are the same as for "Circle_in_plane".


 This is an effectively 2d functions. Stepping over the third dimension and looping over this function is time consuming.
 
*WARNING*: During last test the function did not run as intended. In the y direction it was tilted.

(Number in LFS: 86)

## Halfcylinder bulk sampling (horizontally)
```pascal
procedure halfCylinder_in_plane_w_bulk_etching(length: double; lattice_const, radius : double; halfaxis_a, halfaxis_b: double; angular_resolution: integer; plane_resolution : integer; sign_x_dir: integer);
```
```
HALFCYLINDERBULKETCHED length, lattice_const, radius, halfaxis_a, halfaxis_b, angular_resolution, plane_resolution, sign_x_dir;
```
Starting from the bottom in the xz-plane of one cylinder, the routine samples the volume between two parallel cylinders in the xy-plane. The routine steps along the circular arc, computes the distance to the neighboring cylinder along the x-axis, and draws a horizontal plane between the cylinders.


**Parameters**:
- *length*: Length of the cylinders along the y-axis. This is also the size of the inter-cylinder planes along y.
- *lattice_const*: distane between the two cylinders along the x-axis. Needed to compute the distance at each point along the circular arc.
- *radius*: radius of both cylinders.
- *halfaxis_a*, *halfaxis_b*: halfaxes of the focal ellipse according to the transverse and the longitudinal direction.
- *angular_resolution*: discretization of the half circular arc.
- *plane_resolution*: number of points along the x-direction discretizing the horizontal plane


**Remark**
```pascal
procedure initializeQVariable(halfaxis_a, halfaxis_b, radius : double; angular_res: integer);
procedure horizontalPlane_w_QVars(width_x, width_y : AnsiString; resolution_x: integer; sign_x_dir : AnsiString);
```
are required helper functions for `halfCylinder_in_plane_w_bulk_etching`.

![../img/docs_half_cyl_bulk_horizontally_2d.png](../img/docs_half_cyl_bulk_horizontally_2d.png)
![../img/docs_half_cyl_bulk_horizontally_2d.png](../img/docs_half_cyl_bulk_horizontally_3d.png)

Number in LFS: 87

## Half cylinder
```pascal
procedure halfcylinder(length, radius, halfaxis_a, halfaxis_b : double; angular_res : integer; circleSide : integer);
```
```
HALFCYLINDER length, radius, halfaxis_a, halfaxis_b, angular_res, circleSide;
```
This function does almost the same as `cylinder2`, but it draws only a half circle. This is especially useful in combination with `halfCylinder_in_plane_w_bulk_etching`. In case where the surface roughness of the cylinder does not have to be very good the bulk etching routine `halfCylinder_in_plane_w_bulk_etching` may be sufficient combined with the `halfcylinder` function. If the surface roughness has to be good the cylinders should be drawn with `cylinder2`, using a large angular resolution. In principle, the angular resolution in `halfCylinder_in_plane_w_bulk_etching` should be small to lower the bulk stress due to energy deposition.


**Parameters**
- All parameters from `cylinder2`
- *circleSide*: either +1 or -1, specfifying which half of the cylinder is drawn. If 'circleSide' is +1, the arc is drawn over [-pi/2, pi/2] in mathematically positive direction. If 'circleSide' is -1 it is drawn in mathematically negative direction, meaning that the half circle covers the angular range from [pi/2, 3/2 pi].


Example:
```
HALFCYLINDER 1000, 25, 1, 5, 36, -1;
```
writes a 1000 microns long halfcylinder of radius 25 micro meter with a discretization of the halfcircle by 36 steps. In the xz-cross section the halfcircle spans over the second and the thrid quadrant, meaning the open side of the half cylinder points towards the positive x-direction.

(number in LFS: 88)

## Bulk etching of bulk material on the bottom of the cylinder
```pascal
procedure bulk_etching_between_bottom_half_cylinders(length: double; lattice_const, radius : double; halfaxis_a, halfaxis_b: double; angular_resolution: integer; plane_resolution : integer; sign_x_dir: integer; location : integer);
```
```
HALFBULKETCHINGBETWEENCYLINDERS length, lattice_const, radius, halfaxis_a, halfaxis_b, angular_resolution, sign_x_dir, location;
```
This routine samples the quarter volume between the cylinder and the enclosing rectangular box. There are two different starting points. In the first case, the routine starts from the bottom of the cylinder and steps along the circular arc over a range of 90 degree. In the *xz*-plane the angular range from [3/2 pi, 0] is covered.  In the second case the routine also starts from the bottom, but moves to 0 degree before the laser is switched on. The range from [0, pi/2] is covered.

After each angular step the vertical plane in *yz* is sampled with lines along the *y*-axis.

**Parameters**
- *length*: length of the cylinders and the enclosing box along the *y*-direction
- *lattice_const*: lattice constant of the intended photnic crystal. distance between the rods.
- *radius*: radius of the cylinder
- *halfaxis_a*, *halfaxis_b*: halfaxes of the focal ellipse
- *angular_resolution*: angular resolution of the stepping along the circular arc 
- *sign_x_dir*: Either the right side or the left side of the bulk next to the cylinder is rastered. If *sign_x_dir*= 1, the right side in the *xz*-plane is sampled. If *sign_x_dir*= -1, the left side is written.
- *location*: If *location*=1 the volume below the cylinder is exposed to the laser. If *location*=-1 the bulk above the cylinder is sampled.

![../img/docs_bulk_etching_between_bottom_half_cylinders.png](../img/docs_bulk_etching_between_bottom_half_cylinders.png)

**Remark**
This function is used in the latest routine for writing the hexagonal pattern

Number in LFS: 89


## Cylindrical vacuum channel, bulk sampled
```pascal
procedure Cylindrical_Hole_in_plane_filled(length, radius, halfaxis_a, halfaxis_b : double; angular_res : integer; step_size_x: double);
```
```
CYLINDRICALHOLEFILLED length, radius, halfaxis_a, halfaxis_b, angular_res, step_size_x;
```
The function not only writes the contour of a hollow core cylinder but also samples the bulk silica in the core. Horizontal planes are used between the bottom and the top of the circular cross section in the *xz*-plane. After each step of the circular arc such a horizontal plane is drawn from one side of the cylinder to the opposite side. Another step is done on the opposite side and another horizontal plane is written.

The line samping of each horizontal plane is determined by the step size parameter. The routine corrects for the residual step to complete the overall width. For example if the plane on the current height has a width of 9 microns and a step size of 2 microns is used, the last step is only 1 microns wide such that the last line is written at 9 microns.

**Parameters**
- *length*: length of the hollow core cylinder
- *radius*: Size of the cross section specified as radius
- *halfaxis_a*,*halfaxis_b*: halfaxes of the focal spot of the laser
- *angular_res*: angular resolution of the stepping of the half circle
- *step_size_x*:  step size in x-direction between the lines along the y-axis to form the horizontal plane at each angular point.

Example:
```
CYLINDRICALHOLEFILLED 1000, 50, 1, 4, 36, 1.5;
CYLINDRICALHOLEFILLED 1000, 50, 1, 4, 16, 3;
```
The volume of a hollow core cylinder is written with 1000 microns length and 50 micron radius. The focal ellipse is assumed to have halfaxes of 1 and 4 microns. 36 horizontal planes, aligned along 36 angular points, are used to fill the core volume. A spacing of 1.5 microns between the lines in each horizontal plane is applied.
For illustration the second example uses less sampling points.

![../img/docs_cylindrical_hole_filled.png](../img/docs_cylindrical_hole_filled.png)

Number in LFS: 91

## TODO 
```pascal
procedure Hexagonal_wvg_w_bulk_etching(length: double; lattice_const, radius : double;
                              num_cells: integer; halfaxis_a, halfaxis_b: double; angular_resolution: integer; step_size_x : double;
                              etching_angular_res,
                              etching_rect_res_x, etching_rect_res_z,
                              etching_bulk_inlayer_res_x, etching_bulk_inlayer_res_z,
                              angular_resolution_hole : integer);
```
```
HEXAGONALWVGBULKETCHED length, lattice_const, radius, num_cells, halfaxis_a, halfaxis_b, angular_resolution, step_size_x, etching_angular_res, etching_rect_res_x, etching_rect_res_z, etching_bulk_inlayer_res_x, etching_bulk_inlayer_res_z, angular_resolution_hole;
```

This function writes the hexagonal pattern of a waveguide extended along the y-direction, including the bulk sampling routine. Most of the parameters refer the discretization of the bulk volume.

**Parameters**
- *length*: length of each cylinder and the overall structure
- *lattice_const*: distance between the cylinders, lattice constant of the underlying hexagonal lattice.
- *radius*: size of the cylinders specified by the radius of the circular cross section
- *num_cells*: number of cells, counted from the center of the hollow core to the outside in the cross sectional plane
- *halfaxis_a*, *halfaxis_b*: Ellipse parameters of the focal spot
- *angular_resolution*: angular resolution used to discretize the circle arc of each cylinder
- *step_size_x*: refers to the sampling step size in the [cylindrical hollow core routine](#cylindrical-vacuum-channel,-bulk-sampled)
- *etching_angular_res*: angular resolution of the bulk sampling routine, refers to the [vertical bulk sampling](#bulk-etching-of-bulk-material-on-the-bottom-of-the-cylinder). In principle it is different from the previous angular resolution to deposit less energy in the bulk volume
- *etching_rect_res_x*, *etching_rect_res_z*: Discretization parameters for the rectangular block between two layers of rods. Along the vertical direction a layer of rods and a rectangular block for bulk sampling alternate. The parameters refer to TODO
- *etching_bulk_inlayer_res_x*, *etching_bulk_inlayer_res_z*: Discretization parameters for the rectangular block between two cylinders in the same layer. The parameters refer to TODO. 
- *angular_resolution_hole*: Angular resolution of the cylindrical hollow core 

Example:
```
HEXAGONALWVGBULKETCHED 1000, 100.000, 25.000, 2, 1, 4, 36, 3.000,  4, 225,2, 20, 7, 5 ; 
```
(Number in LFS: 92)

## Rectangular Box
```pascal
procedure RectRel_2(VectX, VectY:double; m : integer; stepW : double; n :integer; stepH: double; planeOrientation : integer);
```
```
RectRel2 VectX, VectY, m, stepW, n, stepH, planeOrientation; 
```
draws a Rectangle with size $`\sqrt(VectX^2 + VectY^2) \times (m stepW) \times (n stepH)`$. The edges of the box are the z-axis, (VectX,VectY, 0) and (-VectY, VectX, 0). The routine uses floating point numbers to achieve the desired width and depth.
Two different ways are implemented to sample the box volume. One uses vertical planes of lines, which are stacked horizontally, while the second one uses horizontal planes stacked vertically.

**Parameters**
- *VectX*: x-component of the main edge of the box in the *xy*-plane. It is also the y-component of the second box edge for spanning the box
- *VectY*: y-component of the main edge of the box in th *xy*-plane
- *m*: number of lines/vertical planes in the horizontal plane
- *stepW*: step size in the horizontal plane along the (VectX, VectY)-direction
- *n*: number of horizontal planes/lines along the z-axis
- *stepH*: step size along the z-axis
- *planeOrientation*: only the sign matters. If positive, the planes are drawn horizontally. If negative, they are vertically aligned. 0 is not a valid parameter.


Example
```
RectRel2 4, 0, 10, 2.5, 6, 7.5, -1; 
```
writes a rectangular box of size 4 x 25 x 45, aligned parallel to the x-,y- and the z-axis. A line of 4 microns length is drawn along the x-axis. 6 such lines are stacked on top of each other to form a vertical plane. In 2.5 microns distance along y the next vertical plane is placed. In total 10 walls are written.

(Number in LFS: 93)


# Code
The images were created with the commented routines in the following code section:
```pascal
program Main;
uses pbgStructureComponents, helperTLFS;
begin
    DEBUG := FALSE;
    x_l := 0.0;
    y_l := 0.0;
    z_l := 0. ;
    LOn(1);
    LOff(1);
    // Cylindrical_Hole_in_plane_filled(1000, 50, 1, 4, 16, 3);
    // bulk_etching_between_bottom_half_cylinders(1000, 200, 50, 1, 5, 16, 3, -1, 1 );
    // halfCylinder_in_plane_w_bulk_etching(1000, 200, 50, 1, 5, 16, 3, -1);
    // horizontalPlane(100, 250, 67);
    // verticalPlane(120, 50, 1, 0, 5);
    // cylinder2(1000, 50, 1, 5, 16);
    // *Skipped*
    // Circular_hole_in_plane(50, 1, 4, 90);
    // cylinder(20, 50, 1, 4, 90, 20);
    // Circle_in_plane(50, 1, 4, 90);
end.
```

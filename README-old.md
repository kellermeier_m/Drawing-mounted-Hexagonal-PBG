This library implements functions for sampling 3D geometries specific to the setup for the Laser machining at GALATEA lab. 


Basic setup principle of the working platform
===
The laser is controlled by a program written in Delphi Pascal. When running it reads in a LFS file which describes what should be drawn in which order and with which parameters. The LFS file is interpreted by the Pascal program and converted to machine code for the laser controller, a PMAC motion controller by Delta Tau Systems.
To define new geometries a make them available via LFs they have to be implemented in the Pascal program directly. For isntance in LFS a circle can be drawn by
```
FlexCircle 40, 1000, 360 ;   //Diameter, Speed, iterations
```
When the controller reads this file, it calls a function `FlexCircle` with those parameters which generates the machine code.

The *FlexCircle* is a circle in the plane perpendicular to the laser beam direction. 

Requirements
===
To use the free available software [Free Pascal](https://www.freepascal.org/) is used instead of Delphi. As not all parts of the controller program are available the software presented here tries to simulate the behavior by writing the laser positions to the standard output. Additional software can be used to plot the points. Plotting is currently done with a python/matplotlib script.
- [Free Pascal 3.04](https://www.freepascal.org/)
- [python 3.6.4](https://www.python.org/)
- numpy 1.14.0
- matplotlib 2.1.1

Using the function and the simulation
===
The package consists of three parts. 

*helperTLFS* is a library providing wrapper functions as helpers which are available on the main code for controlling the stage. It can be imported by
```pascal
unit pbgStructureComponents;
```

*pbgStructureComponents* is a library in which geometries are defined as functions. Those functions provide the sampling strategy to achieve the desired geometry. This library is pure pascal code, meaning that it's not compatible with PMAC specific code. For example, it is necessary to define variables on the controller while pascal variables are not available from the controller.
  The pure pascal code is used for testing the semantic of the pattern defining functions. 

*pbgSructureComponents_PMAC* implements the corresponding Pascal-PMAC translation functions. As the PMAC output can not be tested on the computer, but only on the platform, it is recommended to check the writing strategy as a plain pascal function in the previous library.

For illustration, consider the samping of a square of variable size. This is done by four lines, a horizontal one, a vertical one, a horizontal one in opposite direction and an additional vertical line in opposite direction. If the size is implemented as a pascal varialbe it will be a static number when translating it to PMAC code. To have the size as a variable in the PMAC code, the pascal code has to output the PMAC variable definition.

#### Pascal only:
```pascal
procedure square(length: double)
begin
    mrel(length,0,0);
    mrel(0,length,0);
    mrel(-length, 0, 0);
    mrel(0, -length,0);
end
```
Calling `square(100)` will output a PMAC code similar to 
```
X100 Y0 Z0 ;
X0 Y100 Z0 ;
X-100 Y0 Z0 ;
X0 Y-100 Z0 ;
```

### Pascal-PMAC:
```pascal
procedure square(length: double)
begin
    Add('Q215='+ FloatToStr(length));
    add('X(Q215) Y0 Z0;');
    add('X0 Y(Q215) Z0;');
    add('X(-Q215) Y0 Z0;');
    add('X0 Y(-Q215) Z0;');
end
```
Calling `square(100)` will output a PMAC code like 
```
Q215=100
X(Q215) Y0 Z0;
X0 Y(Q215) Z0;
X(-Q215) Y0 Z0;
X0 Y(-Q215) Z0;
```

To test the functions a main file *main.pas* is provided.


Geometries specific to the 2D PBG Waveguide in glass
===
For a cylinder perpendicular to the beam axis a circle parallel to the beam axis is needed. 

1. A new Pascal function for the controller program will be able to write circles in the plane of the beam axis. 
2. This circle is used to write a cylinder.
3. Multiple parallel cylinders are arranged in a hexagonal manner to form a photonic band gap structure.
4. An additional hole for the waveguide is needed.

It has to be taken into account that a geometry function in pascal only outputs the PMAC commands. This means that a loop will be unrolled in the result. For efficiency it is better to output the PMAC Commands which describe a loop on machine code basis. As a first step Pascal loop are used.

Helper functions
---
The pascal functions are created outside the controller program. The Controller uses functions like `mrel(x,y,z)` to change the relative position of the beam by the given arguments. To test and debug the semantics of the drawing functions those control functions are implemented as helpers which output the hypothetical current beam position.

Those helpers are used:
- `mrel(x,y,z)`
- `LOn(s)`
- `LOff(s)`
- `Line(x,y,z)`
- `RepeatWhile(n)`
- `EndRepeat`
- `ChangePol(angle)`


Example
===
If you want to test visually the writing strategy for `Circle_in_plane(...)` you import the libraries, set the DEBUG level to *FALSE*, set the initial position and call the function:
```pascal
program Main;
uses pbgStructureComponents, helperTLFS;
begin
	DEBUG := FALSE;
	x_l := 0.0;
	y_l := 0.0;
	z_l := 0. ;
	LOn(1);
	LOff(1);
	Circle_in_plane(25, 2, 10, 4, False, False);
end.
```
The initial On-Off switch is needed here for the detection of the laser state in the python code.

To compile the code, run the program and pass the output to the plotting script use
```bash
fpc main2.pas ; ./main2.exe | python Plot_pas_output.py
```
The ouptut from the program is table of the laser positions over time. The python script reads in the positions from the standard input via the pipe.

Currently, to switch from 2D to 3D plotting the python script has to be changed manually. `bool_2D = True` draws the cross section at $`y=0`$, while `bool_2D = False` draws the 3D outcome. For comparision, the result from the example code is shown here:  
![2D Example](img/README_example_2d.png)
![3D Example](img/README_example_3d.png)



Branches (OUTDATED)
===
 - *air-cylinder*: The hollow cylinder - aligned along the y-axis - is reimplemented in such a way that the number of lines per plane changes accorging to its size. Before, the number of lines per plane was given as a fixed parameter to the function. Now, the step size along x in such a plane is given such that all steps but the last one have the same size. The last step corrects for the remainder of the rounding.
- *cut-from-bulk*: For testing purposes three rods shall be written and accessed from the top of cylinders. Since the rods are burried within the glass they have to be cut out of the sample. They have to remain on a smaller sample but cut in such a way that the rods are accessible from the side.
  - Step 1: Correct the function 'verticalPlane' for polarisation. The polarisation  is always given as an angle with the x-axis as a reference.
  - Step 2: Write lfs section for the "cut cuboid" resp. start with pascal code
  - Step 3: Put section for cuboid and section for the three rods together
  
  













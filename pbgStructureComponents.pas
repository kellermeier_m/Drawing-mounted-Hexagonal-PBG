// Library of functions written for the laser writing process 
// 06.07.2017
// Max Kellermeier
// TODO when copying: use default argument
{$mode objfpc}

unit pbgStructureComponents;

interface
uses sysutils, math, helperTLFS;          // exponent operator **

    function step_angle_ellipse(current_pos, a, b, radius, step : double ): double;
    { procedure Circle_in_plane(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; startFromTop : boolean = False); }
    procedure Circle_in_plane(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; startFromTop : boolean = False; initialLOn : boolean = False);
    procedure cylinder(length, radius, a_halfaxis, halfaxis_b : double; angular_res,  y_resolution : integer);
    procedure cylinder2(length, radius, halfaxis_a, halfaxis_b : double; angular_res : integer);
    procedure draw_Hexagonal_Wvg(lattice_const, radius : double; num_cells: integer; halfaxis_a, halfaxis_b: double; angular_resolution: integer; startFromTop : boolean);
    procedure hexagon_of_cylinders(length, lattice_const, radius, a_halfaxis, halfaxis_b : double; num_cells: integer;angular_res,  y_resolution : integer);
    {no finished yet: }
    procedure draw_Mounted_Hexagonal_Wvg(lattice_const, radius: double; num_cells : integer; halfaxis_a, halfaxis_b, mounting_dist, mounting_thickness: double; num_mountings, angular_res, y_resolution: integer);              
    {---}
    function step_angle_ellipse_for_hole(current_pos, a, b, radius, step : double ): double;
    { procedure Circular_Hole_in_plane(radius, halfaxis_a, halfaxis_b : double; angular_res: integer); }
    procedure Circular_Hole_in_plane(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; startFromTop : boolean = False; initialLOn : boolean = False );
    procedure Circular_Hole_in_plane_filled(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; startFromTop, initialLOn : boolean );
    procedure Hexagonal_wvg_half_etch_boundary(lattice_const, radius : double; num_cells: integer; halfaxis_a, halfaxis_b: double; boolBottom, circlesStartedFromTop : boolean);
    
    procedure horizontalPlane(width_x, width_y : double; resolution_x: integer; sign_x_dir : integer = 1);
    procedure verticalPlane(length, height : double; orientationX, orientationY: double; vertical_res: integer);
    procedure verticalPlaneByStep(length, height : double; orientationX, orientationY: double; stepsize_z : double);
    
    procedure halfCylinder_in_plane_w_bulk_etching(length: double; lattice_const, radius : double; halfaxis_a, halfaxis_b: double; angular_resolution: integer; plane_resolution : integer; sign_x_dir: integer);
    procedure halfcylinder(length, radius, halfaxis_a, halfaxis_b : double; angular_res : integer; circleSide : integer);

    procedure Hexagonal_wvg_w_bulk_etching(length: double; lattice_const, radius : double; num_cells: integer; halfaxis_a, halfaxis_b: double; angular_resolution: integer; step_size_x : double;
    etching_angular_res, etching_rect_res_x,  etching_rect_res_z, etching_bulk_inlayer_res_x, etching_bulk_inlayer_res_z, angular_resolution_hole : integer);
    {-----}
    procedure rectangle_bounding_circle(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; startFromTop : boolean; initialLOn : boolean);
    procedure cylinder2_w_boundary_etching(length, radius, halfaxis_a, halfaxis_b : double; angular_res : integer);
    { procedure Cylindrical_Hole_in_plane_filled(length, radius, halfaxis_a, halfaxis_b : double; angular_res, plane_res: integer); }
    procedure Cylindrical_Hole_in_plane_filled(length, radius, halfaxis_a, halfaxis_b : double; angular_res: integer; step_size_x: double);
    
    {---}
    procedure RectRel(VectX, VectY:double; m : integer; stepW : double; n :integer; stepH: double; planeOrientation: integer);

    {---}
    procedure bulk_etching_between_bottom_half_cylinders(length: double; lattice_const, radius : double; halfaxis_a, halfaxis_b: double; angular_resolution: integer; plane_resolution : integer; sign_x_dir: integer;  location : integer );
    
     procedure bulk_etching_between_bottom_half_cylinders_vertically(length: double; lattice_const, radius : double; halfaxis_a, halfaxis_b: double; angular_resolution: integer; sign_x_dir: integer;  location : String );
     
     
implementation
{ Draw a pilar in a direction perpendicular to the laser }
{ Added 15/06/2017 / Project Max Kellermeier }
{ 
  current_pos : angular position of the focal point w.r.t. the resulting circle
  a: halfaxis of ellipsoidal laser beam in transverse plane (x and y components)
  b: halfaxis of ellipsoidal laser beam in longitudinal direction (z component)   
  radius: radius of the resulting circle
  step: change of the angular position of the focal point
}
function step_angle_ellipse(current_pos, a, b, radius, step : double ): double;
var
   (* local variable declaration *)
   dx : double;
   dz : double;
   
   {--- helper functions---}
   { The parametrization of a ellipse is (a*cos(s) , b*sin(s) ) where the parameter s goes from 0 to 2 pi. 
    This function returns the parameter s at the contact point between circle and ellipse which is used to 
    determine the center points K_x(s) and K_y(s) of the ellipse
    Parameter:
        phi: angle of the vector pointing from the origin/center of the circle to the contact point
   }
   function ellipse_s(phi: double) : double;
   begin ellipse_s := Arctan2(b* Sin(phi), a* Cos(phi) ) end;
   
   {x component of the ellipse's center. Expressed in terms of the angle to the contact point phi}
   function K_x(phi : double) : double ;
   begin K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) end;
   
   {y component of the ellipse's center. Expressed in terms of the angle to the contact point phi}
   function K_y(phi : double) : double ;
   begin K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;
   {--- end helper ---} 
begin
   dx := K_x(current_pos + step) - K_x(current_pos);
   dz := K_y(current_pos + step) - K_y(current_pos);
   mrel(dx, 0, dz);
   
   step_angle_ellipse := current_pos + step;
end;

{
    Function for writing a circle oriented with its normal vector perpendicular to the writing axis. This means the laser direction is "in plane" w.r.t. the written circle.
    The writing axis is the direction of the incoming laser beam, denoted as z here.
    Initially, the laser's focus has to be positioned to the top of the circle. It will then move to the bottom and 
    start writting following the circle's arc.
    
    Parameters:
        radius: Radius of the resulting circle
        halfaxis_a: size of ellipsoidal laser beam in transverse plane (x and y components)
        halfaxis_b: size of ellipsoidal laser beam in longitudinal direction (z component)
        angular_res: resolution in angular direction -> Steps in x-z-plane
        startFromTop: If this is true, the laser is focused is on the top of the resulting      circle. The spot will be moved to the bottom before starting writing. If false, it will directly start writting, assuming that the current position is the bottom point.
        initialLOn: Describes whether the Laser is on or off when starting the function. The function takes care of switching on and off the laser. The final status of the laser is equal to the initial one.
       
        
    Since no correction for the discreteness of the steps is considered the effective radius may be larger (average of distance from the center point to the surface)
}
{ procedure Circle_in_plane(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; startFromTop : boolean = False); }
procedure Circle_in_plane(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; startFromTop : boolean = False; initialLOn : boolean = False );
var
    { local declarations}
    { M_x : double;      // center coordinates of the ellipse  }
    { M_z : double; }
    angular_step : double;  //
    cur_angle : double;
    i : integer;    // loop variable
    z_fused_silica : double;
begin
    {procedure body}
    // Logic structure
    // 1. place ellipse at bottom, so at (0, -2)*radius
    // 2. Switch on laser
    // 3. step along the full circular arc counterclockwise
    // 4. repeat steps from [-pi/2, pi/2] (inclusively) via phi=0
    // 5. switch off laser
    // 6. place ellipse at bottom
    // 7. switch on laser
    // 8. step along circular arc clockwise
    // 9. repeat steps over [pi 3/2, pi/2[ (exclusively)
    // 10. Stop at point in front of top point
    // 11. Switch off laser
    // 12. Move to top point, initial position 
    
    // 1.
    if startFromTop then
    begin
        if initialLOn then
        begin
            LOff(5);
            mrel(0,0, - 2 * radius - 2* halfaxis_b);    // move to bottom    
            LOn(5);
        end
        else
            mrel(0,0, - 2 * radius - 2* halfaxis_b);    // move to bottom   
            LOn(5);
    end
    else if not(initialLOn) then    // if starting at the bottom and the laser is switched off, switch it on
        { Writeln('Laser is off, switch on.'); }
        LOn(5);   
        
    cur_angle := -PI/2;
  
    angular_step := Pi/angular_res;
    // 2. + 3.
    // for i:=1 to angular_res do
    //  begin
    //    cur_angle := step_angle_ellipse(cur_angle, halfaxis_a, halfaxis_b , radius,  angular_step);
    // end;
    i:= 0;
    While i< angular_res do
    begin
    	cur_angle := step_angle_ellipse(cur_angle, halfaxis_a, halfaxis_b , radius,  angular_step);
    	i:= i+1;
    end;
    
    
    LOff(5);
    
    If DEBUG then
        Writeln('Repositioning');
    
    // 4.
    mrel(0,0, - 2 * radius - 2* halfaxis_b);    // move to bottom    
    cur_angle := -PI/2;

    LOn(5);
    //for i:=1 to angular_res-1 do    // final point not included since written in second half process
    //begin
    //    cur_angle := step_angle_ellipse(cur_angle, halfaxis_a, halfaxis_b , radius,  -angular_step);
    //end;
    
    i:= 0;
    While i< angular_res-1 do
    begin
    	cur_angle := step_angle_ellipse(cur_angle, halfaxis_a, halfaxis_b , radius,  -angular_step);
    	i:= i+1;
    end;
    
    LOff(5);
    // reposition to initial point
    cur_angle := step_angle_ellipse(cur_angle, halfaxis_a, halfaxis_b , radius,  -angular_step);

    if not(startFromTop) then
        mrel(0,0, - 2 * radius - 2* halfaxis_b);  // move back to bottom
    
end; 

{ 
  current_pos : angular position of the focal point w.r.t. the resulting circle
  a: halfaxis of ellipsoidal laser beam in transverse plane (x and y components)
  b: halfaxis of ellipsoidal laser beam in longitudinal direction (z component)   
  radius: radius of the resulting circle
  step: change of the angular position of the focal point
}
function step_angle_ellipse_for_hole(current_pos, a, b, radius, step : double ): double;
var
   (* local variable declaration *)
   dx : double;
   dz : double;
   
   {--- helper functions---}
   { The parametrization of a ellipse is (a*cos(s) , b*sin(s) ) where the parameter s goes from 0 to 2 pi. 
    This function returns the parameter s at the contact point between circle and ellipse which is used to 
    determine the center points K_x(s) and K_y(s) of the ellipse
    Parameter:
        phi: angle of the vector pointing from the origin/center of the circle to the contact point
   }
   function ellipse_s(phi: double) : double;
   begin ellipse_s := Arctan2(b* Sin(phi), a* Cos(phi) ) end;
   
   {x component of the ellipse's center. Expressed in terms of the angle to the contact point phi}
   function K_x(phi : double) : double ;
   begin K_x := -a*Cos( ellipse_s(phi) ) + radius * Cos(phi) end;
   
   {y component of the ellipse's center. Expressed in terms of the angle to the contact point phi}
   function K_y(phi : double) : double ;
   begin K_y := -b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;
   {--- end helper ---} 
begin
   dx := K_x(current_pos + step) - K_x(current_pos);
   dz := K_y(current_pos + step) - K_y(current_pos);
   mrel(dx, 0, dz);
   
   step_angle_ellipse_for_hole := current_pos + step;
end;

{
    Function for writing a circular hole oriented with its normal vector perpendicular to the writing axis. This means the laser direction is "in plane" w.r.t. the written circle.
    The writing axis is the direction of the incoming laser beam, denoted as z here.
    Initially, the laser's focus has to be positioned to the top of the circle. It will then move to the bottom and 
    start writting following the circle's arc.
    
    Parameters:
        radius: Radius of the resulting circle
        halfaxis_a: size of ellipsoidal laser beam in transverse plane (x and y components)
        halfaxis_b: size of ellipsoidal laser beam in longitudinal direction (z component)
        angular_res: resolution in angular direction -> Steps in x-z-plane
        startFromTop: If this is true, the laser is focused is on the top of the resulting      circle. The spot will be moved to the bottom before starting writing. If false, it will directly start writting, assuming that the current position is the bottom point.
        initialLOn: Describes whether the Laser is on or off when starting the function. The function takes care of switching on and off the laser. The final status of the laser is equal to the initial one.
        
    Since no correction for the discreteness of the steps is considered the effective radius may be larger (average of distance from the center point to the surface)
}
procedure Circular_Hole_in_plane(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; startFromTop : boolean = False; initialLOn : boolean = False);
var
    { local declarations}
    { M_x : double;      // center coordinates of the ellipse  }
    { M_z : double; }
    angular_step : double;  // 
    cur_angle : double;
    i : integer;    // loop variable
    
begin
    {procedure body}
    // Logic structure
    // 1. place ellipse at bottom, so at (0, -2)*radius
    // 2. Switch on laser
    // 3. step along the full circular arc counterclockwise
    // 4. repeat steps from [-pi/2, pi/2] (inclusively) via phi=0
    // 5. switch off laser
    // 6. place ellipse at bottom
    // 7. switch on laser
    // 8. step along circular arc clockwise
    // 9. repeat steps over [pi 3/2, pi/2[ (exclusively)
    // 10. Stop at point in front of top point
    // 11. Switch off laser
    // 12. Move to top point, initial position 
    

    // 1.     
    if startFromTop then
    begin
        if initialLOn then  LOff(0);
        mrel(0,0, -2*radius);
        LOn(5);        
    end
    else  // starts from bottom
    begin 
        mrel(0,0, +2*halfaxis_b);
        if not(initialLOn) then LOn(5);
    end; 
    
    cur_angle := -PI/2;
    angular_step := Pi/angular_res;
    // 2. + 3.
    // for i:=1 to angular_res do
    //  begin
    //    cur_angle := step_angle_ellipse(cur_angle, halfaxis_a, halfaxis_b , radius,  angular_step);
    // end;
    i:= 0;
    While i< angular_res do
    begin
    	cur_angle := step_angle_ellipse_for_hole(cur_angle, halfaxis_a, halfaxis_b , radius,  angular_step);
    	i:= i+1;
    end;
    
    
    LOff(5);
    
    If DEBUG then
        Writeln('Repositioning');
    
    // 4.
    mrel(0,0, - 2 * radius + 2* halfaxis_b);    // move to bottom    
    cur_angle := -PI/2;

    LOn(5);
    //for i:=1 to angular_res-1 do    // final point not included since written in second half process
    //begin
    //    cur_angle := step_angle_ellipse(cur_angle, halfaxis_a, halfaxis_b , radius,  -angular_step);
    //end;
    
    i:= 0;
    While i< angular_res-1 do
    begin
    	cur_angle := step_angle_ellipse_for_hole(cur_angle, halfaxis_a, halfaxis_b , radius,  -angular_step);
    	i:= i+1;
    end;
    
    LOff(5);
    // reposition to initial point
    cur_angle := step_angle_ellipse_for_hole(cur_angle, halfaxis_a, halfaxis_b , radius,  -angular_step);
    
    if startFromTop then mrel(0,0, 2* halfaxis_b)
    else
        mrel( 0, 0, -2*radius);
    
    { if not(startFromTop) then }
        { mrel(0,0, - 2 * radius - 2* halfaxis_b);  // move back to bottom }
end; 

{ Max Kellermeier, 19.7.2017 
    This function does the same as "Circular_Hole_in_plane" but additionally the area enclosed by the circle is written.
    
    It is assumed that the laser is initialLOn positioned on the bottom of the circle
    For the parameters refer to the "Circular_Hole_in_plane" function.
}
procedure Circular_Hole_in_plane_filled(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; startFromTop, initialLOn : boolean );
var
    angular_step : double;  // 
    cur_angle : double;
    i : integer;    // loop variable
    sign: integer;  // specifying whether on pos. or neg. side along the x-axis
    K_x_old : double;
    K_y_old : double;
    K_x_cur : double;
    K_y_cur : double;
    
    function ellipse_s(phi: double) : double;
    begin ellipse_s := Arctan2(halfaxis_b* Sin(phi), halfaxis_a* Cos(phi) ) end;

    // x component of the ellipse's center. Expressed in terms of the angle to the contact point phi
    function K_x(phi : double) : double ;
    begin K_x := - halfaxis_a * Cos( ellipse_s(phi) ) + radius * Cos(phi) end;

    // y component of the ellipse's center. Expressed in terms of the angle to the contact point phi
    function K_y(phi : double) : double ;
    begin K_y := - halfaxis_b * Sin( ellipse_s(phi) ) + radius * Sin(phi) end;

begin     
    // start from bottom
    mrel(0,0, +2*halfaxis_b);
    if not(initialLOn) then LOn(5);
    
    sign := 1;
    cur_angle := -PI/2;
    angular_step := Pi/angular_res;
    K_x_old := K_x(cur_angle);
    K_y_old := K_y(cur_angle);
    
    cur_angle := cur_angle + angular_step;
    K_x_cur := K_x(cur_angle);
    K_y_cur := K_y(cur_angle);
    mrel(sign*(K_x_cur - K_x_old), 0, K_y_cur - K_y_old );
    
    sign := -1 *sign;
    { --- TODO --- }
    
    i:= 1;
    While i< angular_res do
    begin
        // move along x across the circle axis
        mrel(sign * 2* K_x_cur ,0, 0);
        // update variables
        K_x_old := K_x_cur;
        K_y_old := K_y_cur;
        cur_angle := cur_angle + angular_step;
        K_x_cur := K_x(cur_angle);
        K_y_cur := K_y(cur_angle);
        // step over angle on other side
        mrel(sign*(K_x_cur - K_x_old), 0, K_y_cur - K_y_old );
        
        sign := -1 * sign;
    	i:= i+1;
    end;
    LOff(0);
    
    mrel(0,0, +2*halfaxis_b);
    mrel(0,0, -2*(radius+halfaxis_b));
    
    if initialLOn then LOn(5);
    
end;


{
    Make a cylinder with its axis perpendicular to the laser beam. The cylinder axis is oriented 
    along the y-direction while the laser beam goes along the z-axis.
    The laser beam has an ellipsoidal shape.
    Make sure, the laser beam is correctly positioned at the beginning.
    
    Parameters:
        length: Length of the cylinder along y
        radius: Radius of the base circle
        y_resolution: Number of written circles aligned along the y direction. To have at least 
            a result disconnected from the bulk, y_resolution should be at minimum
            length/(2* a_halfaxis). For a real cylindrical shape a higher resolution is recommended.
        a_halfaxis: halfaxis of ellipsoidal laser beam in transverse plane (x and y components)
        halfaxis_b: halfaxis of ellipsoidal laser beam in longitudinal direction (z component)
        angular_res: angular resolution for a half circle. The interval [-Pi/, Pi/2 ] is partitioned in 
            "angular_res" subintervals. The laser beam moves linearly from one angle to the other.
}
procedure cylinder(length, radius, a_halfaxis, halfaxis_b : double;
    angular_res,  y_resolution : integer);
var
    step_y: double;     // step size between neighboring circles; should be at least as large as the "transverse diameter" of the ellipse
    j : integer;     // loop variable for stepping along y-direction
begin
    step_y := length/y_resolution;
    for j:= 1 to y_resolution do
    begin
        // signature: Circle_in_plane(radius, halfaxis_a, halfaxis_b, angular_res)
        Circle_in_plane(radius, a_halfaxis, halfaxis_b, angular_res, True, False);
        If DEBUG then
            Writeln('Circle finished. Moving to position along cylinder axis.');
            
        mrel(0, step_y, 0);
    end;
    If DEBUG then
        Writeln('Finished Writing.');
end;


{
    Make a cylinder with its axis perpendicular to the laser beam. The cylinder axis is oriented     along the y-direction while the laser beam goes along the z-axis.
    The laser beam has an ellipsoidal shape.
    Make sure, the laser beam is correctly positioned at the beginning. It should be positioned on the bottom and one end of the cylinder. 
    
    Parameters:
        length: Length of the cylinder along y
        radius: Radius of the base circle
        y_resolution: Number of written circles aligned along the y direction. To have at least 
            a result disconnected from the bulk, y_resolution should be at minimum
            length/(2* a_halfaxis). For a real cylindrical shape a higher resolution is recommended.
        a_halfaxis: halfaxis of ellipsoidal laser beam in transverse plane (x and y components)
        halfaxis_b: halfaxis of ellipsoidal laser beam in longitudinal direction (z component)
        angular_res: angular resolution for a half circle. The interval [-Pi/, Pi/2 ] is partitioned in 
            "angular_res" subintervals. The laser beam moves linearly from one angle to the other.
    
    REMARK: In comparison to the other cylinder function this one does one step over the circle arc and draw a line along y. The other function steps over the y direction and then draws a circle.
}
procedure cylinder2(length, radius, halfaxis_a, halfaxis_b : double; angular_res : integer);
var
    i : integer;     // loop variable for stepping along y-direction
    sign_y_side: integer;   // either +1 or -1 for determining whether to draw in positive or negative y direction
    angular_step, cur_angle : double;
begin
    // Logic structure
    // 1. draw a line along y-direction
    // 2. Do a step over the circle arc in x-z-plane and draw a line in the other direction
    // 3. Continue with stepping over [-pi/2, pi/2] in positive direction until the top point is reached. Depending whether the angular resolution is odd or even the laser is positioned on the initial side of the cylinder or the opposite.
    // 4. Switch off laser
    // 5. Reposition from top to bottom of the circle
    // 6. Switch on laser
    // 7. Step over [-pi/2, pi/2] in negative direction (actually going from 3/2 pi to pi/2)
    // 8. After each step draw a line
    // 9. When done, switch off laser and make sure the laser is positioned at the end of the cylinder, not at the beginning
    
    LOn(0);        
    cur_angle := -PI/2;
    angular_step := Pi/angular_res;
    // 2. + 3.
    // for i:=1 to angular_res do
    //  begin
    //    cur_angle := step_angle_ellipse(cur_angle, halfaxis_a, halfaxis_b , radius,  angular_step);
    // end;
    i:= 0;
    sign_y_side := 1;
    // 3.
    While i< angular_res do
    begin
        mrel(0, sign_y_side*length, 0);  // 1.
    	cur_angle := step_angle_ellipse(cur_angle, halfaxis_a, halfaxis_b , radius,  angular_step); //2.
    	i:= i+1;
        sign_y_side := -1*sign_y_side;
    end;
    LOff(5);    //4.
    
    mrel(0,0, - 2 * radius - 2* halfaxis_b);    // 5.    

    LOn(5);  //6.
    cur_angle := -PI/2;
        
    i:= 0;
    While i< angular_res do   // 7.
    begin
    	cur_angle := step_angle_ellipse(cur_angle, halfaxis_a, halfaxis_b , radius,  -angular_step);
        mrel(0, sign_y_side*length, 0);  // 8. 
        // (cur_angle and mrel are switched since the first loop draws the bottom line and the second loop draws the top line)
    	i:= i+1;
        sign_y_side := -1*sign_y_side;
    end;
    //9.
    LOff(5);
    mrel(0,0, - 2 * radius - 2* halfaxis_b);  // move back to bottom
    if sign_y_side = -1 then mrel(0, length, 0);
end;

{ Max Kellermeier, maxk@student.ethz.ch, 17.07.2016

    Draw a half cylinder with its cylinder axis along the y direction. This function is based on "cylinder2" and does exactly the same except that it only runs one of the two loops.
    The initial and final beam position is at the bottom of the circle.
    
    Parameters:
        circleSide: either +1 or -1, specfifying which half of the cylinder is drawn. If 'circleSide' is +1, the arc is drawn over [-pi/2, pi/2] in mathematically positive direction. If 'circleSide' is -1 it is drawn in mathematically negative direction, meaning that the half circle covers the angular range from [pi/2, 3/2 pi].
}
procedure halfcylinder(length, radius, halfaxis_a, halfaxis_b : double; angular_res : integer ; circleSide : integer);
var
    i : integer;     // loop variable for stepping along y-direction
    sign_y_side: integer;   // either +1 or -1 for determining whether to draw in positive or negative y direction
    angular_step, cur_angle : double;
begin
    // Logic structure
    // 1. draw a line along y-direction
    // 2. Do a step over the circle arc in x-z-plane and draw a line in the other direction
    // 3. Continue with stepping over [-pi/2, pi/2] in positive direction until the top point is reached. Depending whether the angular resolution is odd or even the laser is positioned on the initial side of the cylinder or the opposite.
    // 4. Switch off laser
    // 5. Reposition to initial point
    
    LOn(0);        
    cur_angle := -PI/2;
    angular_step := Pi/angular_res;
    
    i:= 0;
    sign_y_side := 1;
    // 3.
    While i< angular_res do
    begin
        mrel(0, sign_y_side*length, 0);  // 1.
    	cur_angle := step_angle_ellipse(cur_angle, halfaxis_a, halfaxis_b , radius,  circleSide*angular_step); //2.
    	i:= i+1;
        sign_y_side := -1*sign_y_side;
    end;
    { mrel(0, sign_y_side*length, 0);  // 1. }
    LOff(5);    //4.
    
    //5.
    mrel(0,0, - 2 * radius - 2* halfaxis_b);    // move back to bottom   
    if sign_y_side = -1 then mrel(0, -length, 0); // if at the end of the cylinder move back to the beginning of the cylinder
end;

{Max Kellermeier, maxk@student.ethz.ch

    Draw circles in the xz-plane arranged such that they form a regular hexagon. Additionally the center circle is replaced by a hole which will act as a waveguide.
    
    Update 19.7.2017: The circles are covered by a rectangle which is completely written.
    
    Parameters:
        lattice_const: lattice constant, distance between the circles
        radius: radius of the circles
        num_cells: number of cells from the lattice. This is counted from the center to the boundary
        halfaxis_a: Halfaxis of the focal ellipse in the transverse direction
        halfaxis_b: Halfaxis of the focal ellipse in the longitudinal direction, meaning the beam direction
        angular_resolution: number for discretization of the half circular arc. E.g. 180 means one step is 1 degree
        startFromTop : boolean

}
procedure draw_Hexagonal_Wvg(lattice_const, radius : double; num_cells: integer; halfaxis_a, halfaxis_b: double; angular_resolution: integer; startFromTop : boolean);
var 
    n_layers : integer;
    x_dist : double;
    y_dist : double;
    sign : integer;
    i : integer; 
    x_space_between : double;   // distance between the circles measured from the outer most points
    gap_between_layers : double;
    
    procedure x_step_with_etching(sign : integer; initialLOn : boolean);
    { --- TODO: This description is not correct anymore --- }
    {   Instead of stepping one cell along the x direction a 
        1) half line is written
        2) a line as high as the layer heigth is written from the layer below to the current height
        3) the second half of the line is written to complete the cell step
         -----WARNING ----
        Keep in mind, that it only works correctly if the circles are drawn starting from bottom.
        
        Before, it was just:
        mrel(sign*x_dist, 0, 0);          
    }
    begin
        mrel(sign*(radius+halfaxis_a),0,0);
        LOn(0);
        verticalPlane(x_space_between, 2* (radius+halfaxis_b), sign*1, 0, Round(2* (radius+halfaxis_b) /halfaxis_b));
        
        mrel(sign * (x_space_between+radius+halfaxis_a), 0, 0);
        { --- TODO --- }
        { mrel(sign*x_dist, 0,0); }
        { if not(initialLOn) then LOn(0); }
        { mrel(0.5*sign*x_dist, 0, 0);           }
        { mrel(0,0, y_dist); }
        { LOff(0); }
        { mrel(0,0, -y_dist); }
        { LOn(0); }
        { mrel(0.5*sign*x_dist, 0, 0); }
        { if not(initialLOn) then LOff(0); }
    end;
    
    procedure process_layer(i: integer; direction: integer);
    var
    	j : integer;
    	sign : integer;
        heigtCorrectionForHole : double;
    begin
        if direction > 0 then sign := 1 
        else if direction < 0 then sign := -1
        else Writeln('Direction can not take the value "0".');
        { Circle_in_plane(radius, halfaxis_a, halfaxis_b, angular_resolution, startFromTop, False); }
        rectangle_bounding_circle(radius, halfaxis_a, halfaxis_b, angular_resolution, startFromTop, False);
        x_step_with_etching(-sign, False);
        mrel(sign*x_dist, 0,0);
        // mrel(0,0,0);
        // not in the central layer
        if i <> 0 then
        begin
            for j:=1 to ( n_layers-abs(i) - 1) do    // minus 2 due to inclusive loop
            begin
                x_step_with_etching(sign, False);
                { Circle_in_plane(radius, halfaxis_a, halfaxis_b, angular_resolution, startFromTop, False); }
                rectangle_bounding_circle(radius, halfaxis_a, halfaxis_b, angular_resolution, startFromTop, False);
            end;
        end
        // central layer
        else
        begin
            for j:=1 to num_cells - 1 do
            begin
                x_step_with_etching(sign, False);
                { Circle_in_plane(radius, halfaxis_a, halfaxis_b, angular_resolution, startFromTop, False); }
                rectangle_bounding_circle(radius, halfaxis_a, halfaxis_b, angular_resolution, startFromTop, False);
            end;
            mrel(sign*x_dist, 0, 0);
            { Circular_Hole_in_plane(radius, halfaxis_a, halfaxis_b, angular_resolution,  True, False); }
            if startFromTop then
                heigtCorrectionForHole := -( 2*radius-lattice_const)
            else
                heigtCorrectionForHole := 2*radius-lattice_const;
            mrel(0,0,heigtCorrectionForHole);  // 1.
            Circular_Hole_in_plane_filled(lattice_const - radius, halfaxis_a, halfaxis_b, angular_resolution, startFromTop, False);
            { Circular_Hole_in_plane(lattice_const - radius, halfaxis_a, halfaxis_b, angular_resolution, startFromTop, False); // 2. }
            mrel(0,0,-heigtCorrectionForHole); // 3.
            mrel(sign*x_dist, 0, 0); //4.
            
            rectangle_bounding_circle(radius, halfaxis_a, halfaxis_b, angular_resolution, startFromTop, False);
	        { Circle_in_plane(radius, halfaxis_a, halfaxis_b, angular_resolution,  startFromTop, False); }
            for j:=1 to num_cells - 1 do
            begin
                x_step_with_etching(sign, False);
                
                { Circle_in_plane(radius, halfaxis_a, halfaxis_b, angular_resolution, startFromTop, False); }
                rectangle_bounding_circle(radius, halfaxis_a, halfaxis_b, angular_resolution, startFromTop, False);
            end;
        end;
        x_step_with_etching(sign, False);
        mrel(-sign*x_dist, 0,0);
    end;
begin
     // Initialize variable
    n_layers := num_cells*2 +1;
    x_dist := lattice_const;
    y_dist := lattice_const*sqrt(3.0)/2;
    x_space_between := x_dist - 2*(radius + halfaxis_a);
    gap_between_layers := y_dist - 2*(radius+halfaxis_b);
    // sign = 1 if num_cells%2==0 else -1 
    if num_cells mod 2 =0 then sign := 1 else sign := -1;
    
    if not(startFromTop) then mrel (0,0, -y_dist);
    
    // 1.
    mrel(-sign*x_dist*num_cells*0.5, 0, - num_cells*y_dist*2 );
    
    // 5.
    for i:=-num_cells to num_cells do
    begin
        if i mod 2 =0 then sign := 1 else sign := -1;
        // 2. + 4.
        process_layer(i,sign);
        // 3.
        if i < 0 then 
        begin
            mrel(sign*0.5*x_dist ,0, 0);
            mrel(sign*(radius+halfaxis_a),0, 2*(radius+halfaxis_b))    ;
            verticalPlane(
            (n_layers-abs(i))*x_dist+2*(radius+halfaxis_a),
            gap_between_layers, -sign,0, 10);
            mrel(-sign*(radius+halfaxis_a), 0, 0);
        end
        else
        begin
            mrel(-sign*0.5*x_dist ,0, 0);
            
            mrel(sign*(0.5*x_dist+ radius+halfaxis_a),0, 2*(radius+halfaxis_b))    ;
            verticalPlane(
            (n_layers-abs(i)-1)*x_dist+2*(radius+halfaxis_a),
            gap_between_layers, -sign,0, 10);
            mrel(-sign*(radius+halfaxis_a + 0.5*x_dist), 0, 0);
        end;
        { mrel(0,0, y_dist); }
        
        mrel(0,0, gap_between_layers);
        { sign := -sign; }
    end;
   
   	// 6. In the end, place at the center of the waveguie
    if num_cells mod 2 =0 then sign := 1 else sign := -1;
    mrel(-sign*x_dist*(num_cells-1)* 0.5 , 0, -y_dist );
    if not(startFromTop) then mrel (0,0, y_dist);
end;

{ This function draws a structure of parallel cylinders which are arranged in a hexagonal lattice in the transverse plane.
 This structure obtains a photonic band gap for TM modes.
To get a vacuum waveguide the central cylinder is removed. In a regular distance along the cylinder axis round plates are added to mount the freestanding cylinders.
The structure has several parameters: (all spatial parameters are given in um)
    lattice_const: distance between the cylinders. Due to the reference of a photonic crystal this gives the periodicity of such a lattice
    radius: radius of the cylinder
    num_cells: number of lattice periods. For details see function "draw_Hexagonal_Wvg"
    halfaxis_a: transverse halfaxis of the ellipsoid of the laser's focal spot, perpendicular to the beam axis
    halfaxis_b: longitudinal halfaxis of the ellipsoid of the laser's focal spot, parallel to the beam axis
    
    mounting_dist: distance between the mounting plates
    mounting_thickness: thickness of the mounting plates along the cylinder axis. The radius of the mounting plates is already defined via the number of lattice cells and the lattice constant
    num_mountings: number of mounting plates
        
    angular_res: Discretization of the half circle arc
    y_resolution: discretization along the cylinder axis    
    
}
{ ---------- REMARK: Not implemented finally --------------- }
procedure draw_Mounted_Hexagonal_Wvg(lattice_const, radius: double; num_cells : integer; halfaxis_a, halfaxis_b, mounting_dist, mounting_thickness: double; num_mountings, angular_res, y_resolution: integer);
var
    step_y: double;     // step size between neighboring circles; should be at least as large as the "transverse diameter" of the ellipse
    k : integer;     // loop variable for stepping along y-direction
    j : integer;
    length: double;
    length_section : double;
    mounting_radius: double;
    num_steps_section : integer;
    num_steps_mounting : integer;
begin
    // Logic
    // TODO: Rewrite 'draw_Hexagonal_Wvg' such that it starts with the initial focus on the top instead of the center
    // 1. Start loop for first section of rods (2. - 5.)
    // 2. draw the hexagonal structure in the x-z plane
    // 3. Make sure the laser beam is correctly positioned
    // 4. Make a Step in y-direction while Laser is turned off
    // 5. Repeat from 2. until the first section is finished. This means when y has changed by 'mounting_dist - mounting_dist'
    // 6. Draw a cylinder of height 'mounting_thickness' and radius '( num_cells + 1) * lattice-const'
    // 7. Draw second section of rods by repeating from 2. until the second section is finished and draw the mounting from 6. again
    // 8. Repeat from 2. to 7. 'num_mountings' times
    // 9. Draw last section of rods by finally doing steps 2.-5.
    
    // TODO: make sure the routine "draw_Hexagonal_Wvg" starts from the surface and not from the center
    // TODO: Replace loop with PMAC Code
    // TODO: Check lengths
    length_section :=  mounting_dist - mounting_thickness;
    length := (num_mountings + 1)*mounting_dist;    
    mounting_radius := ( num_cells + 1) * lattice_const;
    
    step_y := length/y_resolution;
    num_steps_section := Round(length_section/step_y); // TODO
    num_steps_mounting := Round(mounting_thickness/step_y);  //TODO
    for k:= 1 to num_mountings do
    begin  // 7. / 8.
        // 1. 
        for j:= 1 to num_steps_section do
        begin
            // 2.
            // draw_Hexagonal_Wvg_plane(lattice_const: integer; radius : double; num_cells: integer; halfaxis_a, halfaxis_b: double; angular_resolution: integer)
            draw_Hexagonal_Wvg(lattice_const, radius, num_cells, halfaxis_a, halfaxis_b, angular_res, False);

            If DEBUG then
                Writeln('Circle finished. Moving to position along cylinder axis.');    
            // 4.
            mrel(0, step_y, 0);
        // 5.
        end;
        // 6. 
        for j:= 1 to num_steps_mounting do
        begin
            // signature: Circle_in_plane(radius, halfaxis_a, halfaxis_b, angular_res)
            Circle_in_plane(mounting_radius, halfaxis_a, halfaxis_b, angular_res, True, False);
            If DEBUG then
                Writeln('Circle finished. Moving to position along cylinder axis.');
                
            mrel(0, step_y, 0);
        end;
    // 8.
    end; 
    // 9.
    // TODO
end;


{ Temporary structure, cylinders arragned in a hexagon. Mounting is missing. PMAC based}
procedure hexagon_of_cylinders(length, lattice_const, radius, a_halfaxis, halfaxis_b : double; num_cells: integer;
                                  angular_res,  y_resolution : integer);
var
    step_y: double;     // step size between neighboring circles; should be at least as large as the "transverse diameter" of the ellipse
    j : integer;     // loop variable for stepping along y-direction

begin
    step_y := length/y_resolution;
    for j:= 1 to y_resolution do
    begin
        draw_Hexagonal_Wvg(lattice_const, radius, num_cells, a_halfaxis, halfaxis_b, angular_res, False);
        mrel(0, step_y, 0);
    end;
end;

{   With the function 'draw_Hexagonal_Wvg' the hexagonal array of circles is printed, but it requires a connected boundary to make it accessible for the etchant. 
    The initial position is on top of the final hexagonal structure
    Parameters:
        Most are the same as from the function of a hexagonal waveguide. They are needed for computing the correct dimensions of the boundary paths
        boolBottom: If True the etch path is drawn on the bottom, if false it is drawn on top off the hexagon. Both together form a closed hexagonal path
        circlesStartedFromTop : if the circles are drawn with the initial position at the top, the bounding hexagon has to be shifted.
}
procedure Hexagonal_wvg_half_etch_boundary(lattice_const, radius : double; num_cells: integer; halfaxis_a, halfaxis_b: double; boolBottom, circlesStartedFromTop : boolean);
var 
    { n_layers : integer; }
    x_dist : double;
    y_dist : double;
    sign : integer;
    halfheight : double;    
begin
    { n_layers := num_cells*2 +1; }
    x_dist := lattice_const;
    y_dist := lattice_const*sqrt(3.0)/2;
    if boolBottom then sign := 1 else sign := -1;
    { if circlesStartedFromTop then signTop := 1 else signTop := -1; }
    halfheight := (num_cells + Integer(not(circlesStartedFromTop)) ) *y_dist;
   
    mrel(-(num_cells+1)*x_dist, 0, - halfheight); // move to left corner of center layer
    { mrel(0, 0, -signTop*(radius+ halfaxis_b)); }
    LOn(0);
    mrel(0.5*x_dist*(num_cells + 1) ,0, -sign*y_dist*(num_cells + 1) ); // move along one side towards bottom
    mrel(x_dist*(num_cells+1), 0, 0);  // move along the bottom side to the right side 
    mrel(0.5*x_dist*(num_cells + 1) ,0, sign*y_dist*(num_cells + 1) ); // move to the right corner of the center layer
    LOff(0);
    mrel(-(num_cells+1)*x_dist, 0, halfheight ); // move back to initial position
    { mrel(0,0, signTop*(radius+ halfaxis_b) ); }
end;


{ Draw a vertical plane, parallel to z. The second direction of the plane is specified as a parameter.
Max Kellermeier, maxk@student.ethz.ch

    It is assumed that the initial laser position is the bottom left corner. Of course this becomes the bottom right corner if the orientation is 180 degree. But it's important to keep in mind.
    The final plane is spanned by (orientationX, orientationY, 0) and (0,0,1).

    Parameters: 
        length: length of the plane in um. This is the length of the intersection line of this plane with the xy-plane
        height: height of the plane in the z-direction
        orientationX, orientationY: a 2d vector parallel to the plane. Only its direction is considered, not its magnitude. 
        vertical_res: number of lines in the vertical direction.
        
}
procedure verticalPlane(length, height : double; orientationX, orientationY: double; vertical_res: integer);
var
    sign : integer;     // used for specifying the current writing direction
    angle_in_xy : double;
    stepsize_z : double;
    i : integer; // loop var
begin
    // angle of the plane w.r.t. the x-axis
    angle_in_xy := Arctan2(orientationY, orientationX );
    sign := 1; //TODO
    stepsize_z := height/vertical_res;
   
    ChangePol( Round( radtodeg(angle_in_xy) ) + 90); 
    //first line, back and forth
    LOn(0);
    mrel(length*cos(angle_in_xy), length*sin(angle_in_xy), 0);
    mrel(-length*cos(angle_in_xy), -length*sin(angle_in_xy), 0);
    
    //loop
    i := 0;
    While i < vertical_res do
    begin
        mrel(0,0,stepsize_z);
        mrel(length*cos(angle_in_xy), length*sin(angle_in_xy), 0);
        mrel(-length*cos(angle_in_xy), -length*sin(angle_in_xy), 0);
        i := i +1 ;
    end;
    LOff(0);
    
    //move back to initial position
    mrel(0,0, -stepsize_z*vertical_res);
end;

{ Draw a horizontal plane in the xy-plane. The sides are parallel to the axes.
Max Kellermeier, maxk@student.ethz.ch

    It is assumed that the laser is initially positioned at one of the bottom corners. Whether this point will be the left or the right corner depends on the writing direction along x, specified by 'sign_x_dir'. From this point the plane is written in positive y direction. The di

    Parameters: 
        width_x: length of the plane in x direction
        width_y: length of the plane in y direction
        height: height of the plane in the z-direction
        orientationX, orientationY: a 2d vector parallel to the plane. Only its direction is considered, not its magnitude. 
        resolution_x: number of lines in the x-direction.
        
}
procedure horizontalPlane(width_x, width_y : double; resolution_x: integer; sign_x_dir : integer = 1);
var
    sign : integer;     {used for specifying the current writing direction}
    stepsize_x : double;
    i : integer;    {loop variable}
begin
    // TODO: Check that sign_x_dir only +1 or -1 is
    stepsize_x := sign_x_dir*width_x/resolution_x;

    ChangePol(90); // parallel polarization
    
    //first line
    LOn(0);
    mrel(0,width_y,0);
    { LOff(0); }
    mrel(0, -width_y, 0);
    
    //loop
    i:=0;
    While i < resolution_x do
    begin
//    RepeatWhile(resolution_x);
        mrel(stepsize_x, 0, 0);
        { LOn(0); }
        mrel(0,width_y,0);
        { LOff(0); }
        mrel(0, -width_y, 0);
        i := i+1;
    end;
    LOff(0);
    
    //move back to initial position
    mrel(-resolution_x*stepsize_x, 0, 0)
end;

{ Max Kellermeier, 16.07.2017, maxk@student.ethz.ch

    TODO: desc
    3D
    
    Parameters:
        plane_resolution: number of lines to form a plane in the xy-plane
        sign_x_dir: either +1 or -1, describes whether the current printing direction of the layer is along the positive or negative x-axis. The planes for the bulk between circles is is printed to the right of the circle in the first case, and to the left in the second case.
}
procedure halfCylinder_in_plane_w_bulk_etching(length: double; lattice_const, radius : double; halfaxis_a, halfaxis_b: double; angular_resolution: integer; plane_resolution : integer; sign_x_dir: integer);
var
    n_layers : integer;
    x_dist : double;
    y_dist : double;
    cur_angle : double;
    angular_step : double;
    i : integer;
    cur_plane_x_width: double;      // current width of the plane along the x direction
    
    function ellipse_s(phi: double) : double;
    begin ellipse_s := Arctan2(halfaxis_b* Sin(phi), halfaxis_a* Cos(phi) ) end;

    {x component of the ellipse's center. Expressed in terms of the angle to the contact point phi}
    function K_x(phi : double) : double ;
    begin K_x := halfaxis_a*Cos( ellipse_s(phi) ) + radius * Cos(phi) end;

    {y component of the ellipse's center. Expressed in terms of the angle to the contact point phi}
    function K_y(phi : double) : double ;
    begin K_y := halfaxis_b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;
    
begin
    // Logic structure
    // 1. Start from bottom of one circle
    // 2. Switch on laser
    // 3. step along circular arc in positive direction over [-pi/2, pi/2]
    // 4. Calculate the size of the plane between cylinder at the current height
    // 5. draw plane between the cylinders: pos. x, pos. y, neg. x, neg. y
    // 6. repeat until the top is reached
    // 7. Switch off laser
    // 8. Move to bottom
    // 9. Switch on laser
    //
    
    LOn(0);     // 2.
    cur_angle := -PI/2;
    angular_step := Pi/angular_resolution;
    
    i:= 0;
    // 3.
    While i< angular_resolution do
    begin
        { mrel(0, sign_y_side*length, 0);  // 1. }
        // 4.
        cur_plane_x_width := lattice_const - 2* Abs(K_x(cur_angle) ) ;
        //5.
        horizontalPlane(cur_plane_x_width, length, plane_resolution , sign_x_dir );
        { horizontalPlane(width_x, width_y : double; resolution_x: integer; sign_x_dir : integer); }
        { LOn(0); }
    	cur_angle := step_angle_ellipse(cur_angle, halfaxis_a, halfaxis_b , radius,  sign_x_dir*angular_step); //2.
    	i:= i+1;
    end;
    cur_plane_x_width := lattice_const - 2* Abs(K_x(cur_angle) ) ;    
    horizontalPlane(cur_plane_x_width, length, plane_resolution , sign_x_dir );
    
    LOff(5);    //7.
    mrel(0,0, - 2 * radius - 2* halfaxis_b);    // 8.    
       
end;


{ Max Kellermeier, 30.07.2017, maxk@student.ethz.ch

    TODO: desc
    3D
    
    Parameters:
        plane_resolution: number of lines to form a plane in the xy-plane
        sign_x_dir: either +1 or -1, describes whether the current printing direction of the layer is along the positive or negative x-axis. The planes for the bulk between circles is is printed to the right of the circle in the first case, and to the left in the second case.
        location: Either the bulk etching is done on the bottom or on the top of the circle. For the first case the argument should be "bottom", in the second case "top"
        
}
procedure bulk_etching_between_bottom_half_cylinders(length: double; lattice_const, radius : double; halfaxis_a, halfaxis_b: double; angular_resolution: integer; plane_resolution : integer; sign_x_dir: integer; location : integer );
var
    n_layers : integer;
    x_dist : double;
    y_dist : double;
    cur_angle : double;
    angular_step : double;
    i : integer;
    cur_plane_x_width: double;      // current width of the plane along the x direction
    
    function ellipse_s(phi: double) : double;
    begin ellipse_s := Arctan2(halfaxis_b* Sin(phi), halfaxis_a* Cos(phi) ) end;

    {x component of the ellipse's center. Expressed in terms of the angle to the contact point phi}
    function K_x(phi : double) : double ;
    begin K_x := halfaxis_a*Cos( ellipse_s(phi) ) + radius * Cos(phi) end;

    {y component of the ellipse's center. Expressed in terms of the angle to the contact point phi}
    function K_y(phi : double) : double ;
    begin K_y := halfaxis_b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;
    
begin
    // Logic structure
    // 1. Start from bottom of one circle
    // 2. Switch on laser
    // 3. step along circular arc in positive direction over [-pi/2, pi/2]
    // 4. Calculate the size of the plane between cylinder at the current height
    // 5. draw plane between the cylinders: pos. x, pos. y, neg. x, neg. y
    // 6. repeat until the top is reached
    // 7. Switch off laser
    // 8. Move to bottom
    // 9. Switch on laser
    //
    
    // make sure the beam is positioned correctly
    { CompareText(cad1, cad2) }
    if location = 1 then
    begin
        cur_angle := -PI/2;
        angular_step := 0.5 *Pi/angular_resolution;
    end
    else if location = -1 then
    begin
        mrel(sign_x_dir*(radius+halfaxis_a), 0, radius+ halfaxis_b );
        if sign_x_dir=1 then cur_angle := 0 else cur_angle:= pi;
        angular_step := 0.5 *Pi/angular_resolution;
    end;
    LOn(0);     // 2.
    
    i:= 0;
    // 3.
    While i< angular_resolution do
    begin
        { mrel(0, sign_y_side*length, 0);  // 1. }
        // 4.
        cur_plane_x_width := lattice_const - 2* Abs(K_x(cur_angle) ) ;
        //5.
        horizontalPlane(cur_plane_x_width, length, plane_resolution , sign_x_dir );
        { horizontalPlane(width_x, width_y : double; resolution_x: integer; sign_x_dir : integer); }
        { LOn(0); }
    	cur_angle := step_angle_ellipse(cur_angle, halfaxis_a, halfaxis_b , radius,  sign_x_dir*angular_step); //2.
    	i:= i+1;
    end;
    cur_plane_x_width := lattice_const - 2* Abs(K_x(cur_angle) ) ;    
    horizontalPlane(cur_plane_x_width, length, plane_resolution , sign_x_dir );
    
    LOff(5);    //7.
    if location = 1 then
    begin 
        mrel(0,0, - radius -  halfaxis_b);    // 8.    
        mrel(-sign_x_dir*(radius+halfaxis_a), 0,0 );
    end
    else if location = -1 then
    begin 
        mrel(0,0, - 2*(radius +  halfaxis_b));    // 8.    
    end
end;



{ Max Kellermeier, 16.07.2017, maxk@student.ethz.ch
  
    Draw a hexagon of cylinders where the bulk between the rods is completely written for etching. The function is based on the mathematical basis of 'circle_in_plane' and on "draw_Hexagonal_Wvg". While thos functions are only working in 2d this function uses the third dimension from the beginning by drawing lines at each point from the two-dimensional scenario.
    
    Initial and final laser position is the top of the 
    Parameters:
        length: length of the cylinders along y-dir.
        lattice_const: lattice const. of the photonic crystal, distance between the cylinders
        radius: radius of cylinders
        num_cells: number of periods of the photonic crystal
        halfaxis_a: halfaxis of the laser focal ellipse in the transverse plane
        halfaxis_b: halfaxis of the laser focal ellipse in the longitudinal direction, the z-direction
        angular_resolution: number of points used in the discretization of the circle.
        plane_resolution: Discretization of the horizontal planes along the x direction. Each plane in xy consists of lines along the y direction. plane_resolution: specifies the number of those line. It determines the spacing between the lines for each plane separately, depending on its width.
        step_size_x: step size in x direction when drawing the planes to fill the air cylinder
        
        resolution parameters:
        etching_angular_res: 
            angular steps for vertical planes below and above cylinder
        etching_rect_res_z:
            vertical resolution of rectangular bulk between layers of rods ;
        etching_rect_res_x:
            horizontal resolution of rectangular bulk between layers of rods;
        etching_bulk_inlayer_res_x: 
            horizontal resolution of rectangular bulk between two rods within a layer;
        etching_bulk_inlayer_res_z:
            vertical resolution of rectangular bulk between two rods within a layer;
        angular_resolution_hole:
            angular steps within the waveguide hole.
            
    Example for resolution parameters:
    etching_angular_res := 4;
    etching_rect_res_z := 2;
    etching_rect_res_x := 225;
    etching_bulk_inlayer_res_x := 2; 
    etching_bulk_inlayer_res_z := 7;
    angular_resolution_hole := angular_resolution;
        
}
procedure Hexagonal_wvg_w_bulk_etching(length: double; lattice_const, radius : double; num_cells: integer; halfaxis_a, halfaxis_b: double; angular_resolution: integer; step_size_x : double;
    etching_angular_res,
    etching_rect_res_x, etching_rect_res_z, etching_bulk_inlayer_res_x, etching_bulk_inlayer_res_z, angular_resolution_hole : integer);
var
    n_layers : integer;
    x_dist : double;
    y_dist : double;
    cur_angle : double;
    angular_step : double;
    i : integer;
    sign : integer;
    cur_plane_x_width: double;      // current width of the plane along the x direction
    gap_between_layers : double;
    x_width_rect : double;
    width : double;
    
    { etching_angular_res : integer; }
    { etching_rect_res_x, etching_rect_res_z : integer; }
    { etching_bulk_inlayer_res_x, etching_bulk_inlayer_res_z : integer; }
    { angular_resolution_hole : integer; }
    
    function ellipse_s(phi: double) : double;
    begin ellipse_s := Arctan2(halfaxis_b* Sin(phi), halfaxis_a* Cos(phi) ) end;

    // x component of the ellipse's center. Expressed in terms of the angle to the contact point phi
    function K_x(phi : double) : double ;
    begin K_x := halfaxis_a*Cos( ellipse_s(phi) ) + radius * Cos(phi) end;

    // y component of the ellipse's center. Expressed in terms of the angle to the contact point phi
    function K_y(phi : double) : double ;
    begin K_y := halfaxis_b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;
    
    procedure process_layer(i: integer; direction: integer);
    var
    	j : integer;
    	sign : integer;
        heigtCorrectionForHole : double;
    begin
        // Logic structure
        // 1. Draw half circle at the outer side without the bulk etching
        // 2. Draw half circle on the inside with bulk etching
        // 3. Repeat until end is reached
        // 4. Draw half circle on the outside
    
        if direction > 0 then sign := 1 
        else if direction < 0 then sign := -1
        else Writeln('Direction can not take the value "0".');
        
        { halfcylinder(length, radius, halfaxis_a, halfaxis_b, angular_resolution, -sign); }
         
        { x_step_with_etching(-sign, False); }
        { mrel(sign*x_dist, 0,0); }
        
        
        if i <> 0 then
        begin            
            for j:=1 to ( n_layers-abs(i)-1) do    // minus 2 due to inclusive loop
            begin
                bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   sign, 'bottom');
                bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   -sign, 'bottom');
                cylinder2(length, radius, halfaxis_a, halfaxis_b, angular_resolution);
                { halfCylinder_in_plane_w_bulk_etching(length, lattice_const, radius,  halfaxis_a, halfaxis_b, angular_resolution, plane_resolution, sign); }
                bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   sign, 'top');
                bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   -sign, 'top');
                
                mrel(sign*(radius+halfaxis_a),0,0  );
                
                { RectRel(0, length,  }
                    { Round((x_dist - 2*(radius+halfaxis_a) )/step_size_x ), -sign*step_size_x, etching_rect_res_z, 2*(radius + halfaxis_b) / (etching_rect_res_z), -1 ); }
                RectRel(0,length, etching_bulk_inlayer_res_x, 
                    -sign*(x_dist - 2*(radius+halfaxis_a) )/etching_bulk_inlayer_res_x, etching_bulk_inlayer_res_z,  2*(radius + halfaxis_b) / (etching_bulk_inlayer_res_z) , -1
                ); 
                
                mrel(sign*(x_dist - 2*(radius+halfaxis_a) ),0,0 );
                mrel(sign*(radius+halfaxis_a),0,0  );                

            end;
            bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   sign, 'bottom');
            bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   -sign, 'bottom');
            cylinder2(length, radius, halfaxis_a, halfaxis_b, angular_resolution);
            { halfCylinder_in_plane_w_bulk_etching(length, lattice_const, radius,  halfaxis_a, halfaxis_b, angular_resolution, plane_resolution, sign); }
            bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   sign, 'top');
            bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   -sign, 'top');
            
        end
        // central layer
        else
        begin        
            for j:=1 to num_cells-1  do    // minus 2 due to inclusive loop
            begin
                bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   sign, 'bottom');
                bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   -sign, 'bottom');
                cylinder2(length, radius, halfaxis_a, halfaxis_b, angular_resolution);
                bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   sign, 'top');
                bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   -sign, 'top');
                { mrel(sign*x_dist, 0,0); }
                mrel(sign*(radius+halfaxis_a),0,0  );
                { RectRel(0, length,  }
                    { Round((x_dist - 2*(radius+halfaxis_a) )/step_size_x ), -sign*step_size_x, etching_rect_res_z, 2*(radius + halfaxis_b) / (etching_rect_res_z), -1 ); }
                RectRel(0,length, etching_bulk_inlayer_res_x, 
                    -sign*(x_dist - 2*(radius+halfaxis_a) )/etching_bulk_inlayer_res_x, etching_bulk_inlayer_res_z,  2*(radius + halfaxis_b) / (etching_bulk_inlayer_res_z) , -1
                ); 
                mrel(sign*(x_dist - 2*(radius+halfaxis_a) ),0,0 );
                mrel(sign*(radius+halfaxis_a),0,0  );
            end;
            
            bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   sign, 'bottom');
            bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   -sign, 'bottom');
            cylinder2(length, radius, halfaxis_a, halfaxis_b, angular_resolution);
            bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   sign, 'top');
            bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   -sign, 'top');
            mrel(sign*x_dist, 0,0);
            
            
            { mrel(-sign*x_dist, 0, 0); }

          
        // ------------- TODO ----------
            { // TODO }
            { // 1. Correct position to bottom of large circle }
            { // 2. Write the Circular Hole, assuming the initial position is at the top }
            { // 3. Correct position back to the top of the glass circles }
            { // 4. step aside along x for the position of the next glass circle }
            { Circular_Hole_in_plane(radius, halfaxis_a, halfaxis_b, angular_resolution,  True, False); }
            heigtCorrectionForHole := 2*radius-lattice_const;
            mrel(0,0,heigtCorrectionForHole);  // 1.
            Cylindrical_Hole_in_plane_filled(length, lattice_const - radius, halfaxis_a, halfaxis_b, angular_resolution_hole, step_size_x);
            mrel(0,0,-heigtCorrectionForHole); // 3.
            mrel(sign*x_dist, 0, 0); //4.
            
            
            for j:=1 to num_cells-1 do    // minus 2 due to inclusive loop
            begin
                bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   sign, 'bottom');
                bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   -sign, 'bottom');
                cylinder2(length, radius, halfaxis_a, halfaxis_b, angular_resolution);
                bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   sign, 'top');
                bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   -sign, 'top');
                { mrel(sign*x_dist, 0,0); }
                
                mrel(sign*(radius+halfaxis_a),0,0  );
                { RectRel(0, length,  }
                    { Round((x_dist - 2*(radius+halfaxis_a) )/step_size_x ), -sign*step_size_x, etching_rect_res_z, 2*(radius + halfaxis_b) / (etching_rect_res_z), -1 ); }
                RectRel(0,length, etching_bulk_inlayer_res_x, 
                    -sign*(x_dist - 2*(radius+halfaxis_a) )/etching_bulk_inlayer_res_x, etching_bulk_inlayer_res_z,  2*(radius + halfaxis_b) / (etching_bulk_inlayer_res_z) , -1
                );   
                
                mrel(sign*(x_dist - 2*(radius+halfaxis_a) ),0,0 );
                mrel(sign*(radius+halfaxis_a),0,0  );
                
            end;
            bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   sign, 'bottom');
            bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   -sign, 'bottom');
            cylinder2(length, radius, halfaxis_a, halfaxis_b, angular_resolution);
            bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   sign, 'top');
            bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   -sign, 'top');
            mrel(sign*x_dist, 0,0);
            mrel(-sign*x_dist,0,0);
            
            { for j:=1 to num_cells - 1 do    // minus 2 due to inclusive loop }
            { begin }
                { bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   sign, 'bottom'); }
                { mrel(sign*x_dist, 0,0); }
            { end; }
            { for j:=1 to num_cells - 1 do    // minus 2 due to inclusive loop }
            { begin }
                { cylinder2(length, radius, halfaxis_a, halfaxis_b, angular_resolution); }
                { { halfCylinder_in_plane_w_bulk_etching(length, lattice_const, radius,  halfaxis_a, halfaxis_b, angular_resolution,   sign); } }
                { mrel(-sign*x_dist, 0,0); }

            { end; }
            { cylinder2(length, radius, halfaxis_a, halfaxis_b, angular_resolution); }
            { for j:=1 to num_cells - 1 do    // minus 2 due to inclusive loop }
            { begin }
                { bulk_etching_between_bottom_half_cylinders_vertically(length, lattice_const, radius,  halfaxis_a, halfaxis_b, etching_angular_res,   sign, 'top'); }
                { mrel(sign*x_dist, 0,0); }
            { end; }
        end;
        
        { halfcylinder(length, radius, halfaxis_a, halfaxis_b, angular_resolution, sign); }
        
        { x_step_with_etching(sign, False); }
        { mrel(-sign*x_dist, 0,0); }
    end;
    
begin
     // Initialize variable
    width := num_cells*2*lattice_const+ 2*(radius+halfaxis_a);
    n_layers := num_cells*2 +1;
    x_dist := lattice_const;
    y_dist := lattice_const*sqrt(3.0)/2;
    gap_between_layers := y_dist - 2*(radius+halfaxis_b);
    // not in the central layer
    etching_angular_res := (angular_resolution div 4) +1;
    etching_rect_res_z := angular_resolution div 8 + 1;
    
    { etching_angular_res := 4; }
    { etching_rect_res_z := 2; }
    { etching_rect_res_x := 225; }
    { etching_bulk_inlayer_res_x := 2;  }
    { etching_bulk_inlayer_res_z := 7; }
    { angular_resolution_hole := angular_resolution; }
    
    
    if num_cells mod 2 =0 then sign := 1 else sign := -1;
    
    mrel (0,0, -y_dist);
    
    // 1.
    mrel(-sign*x_dist*num_cells*0.5, 0, - num_cells*y_dist*2 );
    
    //
    mrel(0,0,-gap_between_layers);
    mrel(-sign*abs(num_cells)*0.5*x_dist, 0, 0);          
    mrel(-sign*(radius+halfaxis_a),0, 0)    ;
    
    { RectRel(0,length, Round(width/step_size_x), -sign* step_size_x, etching_rect_res_z, gap_between_layers / (etching_rect_res_z), 1 ); }
    RectRel(0,length, etching_rect_res_x, -sign*Round(width/etching_rect_res_x), etching_rect_res_z, gap_between_layers / (etching_rect_res_z), 1 );
    
    
    
    mrel(sign*abs(num_cells)*0.5*x_dist, 0, 0);          
    mrel(sign*(radius+halfaxis_a),0, 0)    ;
    mrel(0,0,gap_between_layers);
    
    // 5.
    for i:=-num_cells to num_cells do
    begin
        if i mod 2 =0 then sign := 1 else sign := -1;
        // 2. + 4.
        process_layer(i,sign);
        // 3.
        mrel(sign*abs(i)*0.5*x_dist, 0, 0);          
        
        mrel(sign*(radius+halfaxis_a),0, 2*(radius+halfaxis_b))    ;
        { RectRel(0,length, Round(width/step_size_x), sign* step_size_x, etching_rect_res_z, gap_between_layers / (etching_rect_res_z), 1 ); }
        RectRel(0,length, etching_rect_res_x, sign*Round(width/etching_rect_res_x), etching_rect_res_z, gap_between_layers / (etching_rect_res_z), 1 );
        
        
        if i < 0 then
        begin
            mrel(-sign* (abs(i)-1)*0.5*x_dist, 0, 0);
        end
        else
        begin
            mrel(-sign* (abs(i)+1) *0.5*x_dist, 0, 0);
        end;
        mrel(-sign*(radius+halfaxis_a), 0, 0);
        
        mrel(0,0, gap_between_layers);
    end;
    
    // 6. In the end, place at the center of the waveguie
    if num_cells mod 2 =0 then sign := 1 else sign := -1;
    mrel(-sign*x_dist*(num_cells-1)* 0.5 , 0, -y_dist );
    mrel (0,0, y_dist);
    
end;


{ Max Kellermeier, 18.07.2017
    ---- function not implemented yet --------
    This function is based on "cylinder2". It creates a cylinder with its axis perpendicular to the laser beam. The cylinder axis is oriented along the y-direction while the laser beam goes along the z-axis.
    The laser beam has an ellipsoidal shape.
    Make sure, the laser beam is correctly positioned at the beginning. It should be positioned on the bottom and one end of the cylinder. 
    Apart from the cylinder itself an enveloping rectangle is written at the top and the bottom. The bulk between this rectangle and the circle in the x-z-plane is fully written to have a large volume for etching.
    
    Parameters:
        length: Length of the cylinder along y
        radius: Radius of the base circle
        y_resolution: Number of written circles aligned along the y direction. To have at least 
            a result disconnected from the bulk, y_resolution should be at minimum
            length/(2* a_halfaxis). For a real cylindrical shape a higher resolution is recommended.
        a_halfaxis: halfaxis of ellipsoidal laser beam in transverse plane (x and y components)
        halfaxis_b: halfaxis of ellipsoidal laser beam in longitudinal direction (z component)
        angular_res: angular resolution for a half circle. The interval [-Pi/, Pi/2 ] is partitioned in 
            "angular_res" subintervals. The laser beam moves linearly from one angle to the other.
    
    REMARK: In comparison to the other cylinder function this one does one step over the circle arc and draw a line along y. The other function steps over the y direction and then draws a circle.
}
procedure cylinder2_w_boundary_etching(length, radius, halfaxis_a, halfaxis_b : double; angular_res : integer);
var
    i : integer;     // loop variable for stepping along y-direction
    sign_y_side: integer;   // either +1 or -1 for determining whether to draw in positive or negative y direction
    angular_step, cur_angle : double;
begin
    // Logic structure
    // 1. draw a line along y-direction
    // ------- TODO ------
    // 2. Do a step over the circle arc in x-z-plane and draw a line in the other direction
    // 3. Continue with stepping over [-pi/2, pi/2] in positive direction until the top point is reached. Depending whether the angular resolution is odd or even the laser is positioned on the initial side of the cylinder or the opposite.
    // 4. Switch off laser
    // 5. Reposition from top to bottom of the circle
    // 6. Switch on laser
    // 7. Step over [-pi/2, pi/2] in negative direction (actually going from 3/2 pi to pi/2)
    // 8. After each step draw a line
    // 9. When done, switch off laser and make sure the laser is positioned at the end of the cylinder, not at the beginning
    
    LOn(0);        
    cur_angle := -PI/2;
    angular_step := Pi/angular_res;
    // 2. + 3.
    // for i:=1 to angular_res do
    //  begin
    //    cur_angle := step_angle_ellipse(cur_angle, halfaxis_a, halfaxis_b , radius,  angular_step);
    // end;
    i:= 0;
    sign_y_side := 1;
    // 3.
    While i< angular_res do
    begin
        mrel(0, sign_y_side*length, 0);  // 1.
    	cur_angle := step_angle_ellipse(cur_angle, halfaxis_a, halfaxis_b , radius,  angular_step); //2.
    	i:= i+1;
        sign_y_side := -1*sign_y_side;
    end;
    LOff(5);    //4.
    
    mrel(0,0, - 2 * radius - 2* halfaxis_b);    // 5.    

    LOn(5);  //6.
    cur_angle := -PI/2;
        
    i:= 0;
    While i< angular_res do   // 7.
    begin
    	cur_angle := step_angle_ellipse(cur_angle, halfaxis_a, halfaxis_b , radius,  -angular_step);
        mrel(0, sign_y_side*length, 0);  // 8. 
        // (cur_angle and mrel are switched since the first loop draws the bottom line and the second loop draws the top line)
    	i:= i+1;
        sign_y_side := -1*sign_y_side;
    end;
    //9.
    LOff(5);
    mrel(0,0, - 2 * radius - 2* halfaxis_b);  // move back to bottom
    if sign_y_side = 1 then mrel(0, length, 0);
end;

{ Max Kellermeier, 18.07.2017

   The function "Circle_in_plane" writes the contour of a circle in the x-z-plane. But for detaching the surrounding bulk from the substrate a larger area has to be written. This function fills the area between the circle and an enveloping rectangle. Since the y coordinate remains unchanged it's an effectively two-dimensional routine. The resulting rectangle is 2*(radius+halfaxis_a) wide in x-direction and 2*(radius+halfaxis_b) tall in z-direction.
   REMARK: Make sure that the laser is initially placed correctly,  namely at the bottom of the circle.
   
   The Parameters are the same as for "Circle_in_plane".
   
   This is an effectively 2d functions. Stepping over the third dimension and looping over this function is time consuming
}
procedure rectangle_bounding_circle(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; startFromTop : boolean; initialLOn : boolean);
var
    cur_angle : double;
    angular_step : double;
    rect_bound : double;
    K_x_old : double;
    K_y_old : double;
    K_x_cur : double;
    K_y_cur : double;
    sign : integer;
    i : integer;    // loop variable
    
    function ellipse_s(phi: double) : double;
    begin ellipse_s := Arctan2(halfaxis_b* Sin(phi), halfaxis_a* Cos(phi) ) end;

    // x component of the ellipse's center. Expressed in terms of the angle to the contact point phi
    function K_x(phi : double) : double ;
    begin K_x := halfaxis_a*Cos( ellipse_s(phi) ) + radius * Cos(phi) end;

    // y component of the ellipse's center. Expressed in terms of the angle to the contact point phi
    function K_y(phi : double) : double ;
    begin K_y := halfaxis_b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;

begin
    // Logic structure:
    // Start from bottom
    // 1. Move to the bottom left corner of the rectangle
    // 2. Draw bottom line and update dynammic variables
    // 3. Start iterating over angles of the circle
    // 4. Calculate new K coordinates
    // 5. Step in z-dir by difference between new and old K_z
    // 6. Draw line on one side of the circle, switch off laser, move across the circle, switch on laser, and draw the line on the other side of the circle
    // 7. change sign for the drawing direction along x and save old K coordinates
    // 8. Loop until the top end is reached
    // 9. Switch off laser and move back to initial position 

    
    cur_angle := -PI/2;
    rect_bound := radius+halfaxis_a;
    angular_step := Pi/angular_res;
    K_x_cur := K_x(cur_angle);
    K_y_cur := K_y(cur_angle);
    sign := 1;
    
    // 1.
    mrel(-rect_bound, 0, 0);
    // 2.
    LOn(5);
    mrel(sign * (rect_bound-K_x_cur), 0, 0);
    LOff(5);
    mrel(sign * 2*K_x_cur, 0, 0);
    LOn(5);
    mrel(sign * (rect_bound - K_x_cur), 0, 0);
    
    sign := -1 * sign;
    K_x_old := K_x_cur;
    K_y_old := K_y_cur;
    
    // 3. start loop
    i:= 0;
    While i< angular_res do
    begin
        // 4.
        cur_angle := cur_angle + angular_step;
        K_x_cur := K_x(cur_angle);
        K_y_cur := K_y(cur_angle);
        // 5.
        mrel(0, 0, K_y_cur - K_y_old);
        // 6.
        mrel(sign * (rect_bound-K_x_cur), 0, 0);
        LOff(5);
        mrel(sign * 2*K_x_cur, 0, 0);
        LOn(5);
        mrel(sign * (rect_bound - K_x_cur), 0, 0);
        // 7.
        sign := -1 * sign;
        K_x_old := K_x_cur;
        K_y_old := K_y_cur;
        // 8.
        i := i+1;
    end;
    // 9.
    LOff(5);
    mrel(sign * rect_bound, 0 , -2*(radius+halfaxis_b));
    
end;

{ Max Kellermeier, 19.7.2017 
    This function is based on "Circular_Hole_in_plane_filled". While "Circular_Hole_in_plane_filled" writes a two-dimensional object, the circle, this function creates a cylinder, including the third dimension right from the beginning.
    The parameters are similar to those from "cylinder2" since this is basically a cylinder of air instead of glass.
    
    Assumptions: 
        The initial position is at the bottom of the resulting cylinder.
        The laser is off before start.
    
    Parameters:
        ...
        plane_res: resolution of the plane along the x-axis, the stepping direction in the xy-plane. If the resolution is too low the result will be a lattice of parallel lines along the y-axis
}
{ procedure cylinder2(length, radius, halfaxis_a, halfaxis_b : double; angular_res : integer); }
procedure Cylindrical_Hole_in_plane_filled(length, radius, halfaxis_a, halfaxis_b : double; angular_res: integer; step_size_x: double);
var
    angular_step : double;  // 
    cur_angle : double;
    residual_step : double;
    plane_res : integer;    // number of lines in the current plane - 1
    i, j : integer;    // loop variable
    sign: integer;  // specifying whether on pos. or neg. side along the x-axis
    K_x_old : double;
    K_y_old : double;
    K_x_cur : double;
    K_y_cur : double;
    
    function ellipse_s(phi: double) : double;
    begin ellipse_s := Arctan2(halfaxis_b* Sin(phi), halfaxis_a* Cos(phi) ) end;

    // x component of the ellipse's center. Expressed in terms of the angle to the contact point phi
    function K_x(phi : double) : double ;
    begin K_x := - halfaxis_a * Cos( ellipse_s(phi) ) + radius * Cos(phi) end;

    // y component of the ellipse's center. Expressed in terms of the angle to the contact point phi
    function K_y(phi : double) : double ;
    begin K_y := - halfaxis_b * Sin( ellipse_s(phi) ) + radius * Sin(phi) end;

begin     
    // start from bottom
    mrel(0,0, +2*halfaxis_b);
    { if not(initialLOn) then LOn(5); }
    LOn(5);
    
    sign := 1;
    cur_angle := -PI/2;
    angular_step := Pi/angular_res;
    mrel(0, length, 0);
    mrel(0, -length, 0);
    K_x_old := K_x(cur_angle);
    K_y_old := K_y(cur_angle);
    
    cur_angle := cur_angle + angular_step;
    K_x_cur := K_x(cur_angle);
    K_y_cur := K_y(cur_angle);
    mrel(sign*(K_x_cur - K_x_old), 0, K_y_cur - K_y_old );
    
    sign := -1 *sign;
    
    i:= 1;
    While i< angular_res do
    begin
        // calculate number of steps along x for writing the plane
        plane_res := Floor(2*K_x_cur/step_size_x );
        residual_step :=2* K_x_cur - step_size_x*plane_res;
    
        // move along x across the circle axis
        { mrel(sign * 2* K_x_cur ,0, 0); }
        
        { horizontalPlane(2* K_x_cur, length, plane_res, -sign ); }
        mrel(0, length, 0);
        mrel(0, -length, 0);
        j:=0;
        while j< plane_res do
        begin
            mrel(sign*step_size_x, 0,0);
            mrel(0, length, 0);
            mrel(0, -length, 0);
            j:= j+1;
        end;
        mrel(sign* residual_step, 0,0);
        mrel(0, length, 0);
        mrel(0, -length, 0);    
        { mrel( sign*2* K_x_cur, 0,0); }
        
        // update variables
        K_x_old := K_x_cur;
        K_y_old := K_y_cur;
        cur_angle := cur_angle + angular_step;
        K_x_cur := K_x(cur_angle);
        K_y_cur := K_y(cur_angle);
        // step over angle on other side
        mrel(sign*(K_x_cur - K_x_old), 0, K_y_cur - K_y_old );
        sign := -1 * sign;
    	i:= i+1;
    end;
    mrel(0, length, 0);
    mrel(0, -length, 0);
    LOff(0);
    
    mrel(0,0, +2*halfaxis_b);
    mrel(0,0, -2*(radius+halfaxis_b));    
    { if initialLOn then LOn(5); }
end;


{Copied from Converter_LFS_PMAC}
{
    Draw a Rectangle with size 
            sqrt(VectX^2 + VectY^2)
         x (m*stepW) 
         x (n*stepH)
    The vector (VectX, VectY) gives also the orientation. It describes the side from the bottom left corner to the bottom right corner.
    
    planeOrientation: integer
        Only the sign matters. If positive, the planes are drawn horizontally. If negative, they are vertically aligned. 0 is not a valid parameter.
}
procedure RectRel(VectX, VectY:double; m : integer; stepW : double; n :integer; stepH: double; planeOrientation: integer);
var
 i,j     : integer;
 X,Y : double;
 alpha   : double;
 horizontal: boolean;
 outerLoop : integer; 
 innerLoop: integer;

    procedure doLineAndStep();
    begin
        Line(VectX,VectY,0);      //Each lines is double-exposed 
        Line(-VectX,-VectY,0);
        if horizontal then Line(X,Y,0) else Line(0,0,stepH);
    end;
    procedure endSectionOfPlane();
    begin
        Line(VectX,VectY,0);      // Each lines is double-exposed 
        Line(-VectX,-VectY,0);
        if horizontal then
        begin
            Line(-m*X, -m*Y,0);
            Line(0,0,stepH);
        end
        else
        begin
            Line(0,0,-n*stepH);
            Line(X,Y,0);
        end;
        
    end;
    
begin
  alpha :=Arctan2(VectY, VectX);
  if planeOrientation > 0 then
    ChangePol( Round( radtodeg(alpha) ) ) // parallel polarization
  else if planeOrientation < 0 then
    ChangePol( Round( radtodeg(alpha) ) + 90 ); // perpendicular polarization
  horizontal := planeOrientation > 0;
  
  X:=-stepW*sin(Alpha); // calculate the step in the local coordinate frame
  Y:=stepW*cos(Alpha);
  LOn(5);
  
  if horizontal then
  begin
    innerLoop := m;
    outerLoop := n;
  end
  else
  begin
    innerLoop := n;
    outerLoop := m;
  end;
  
  
  { for j := 0 to outerLoop do }
  j:=0;
  while j < outerLoop do
  begin
      i := 0;
      while i< innerLoop do
      { for I := 0 to innerLoop do }
      begin
        doLineAndStep();
        i := i+1;
      end;
      endSectionOfPlane();
      j := j+1;
  end;
  { for I := 0 to innerLoop do }
  i:=0;
  while i< innerLoop do
  begin
      doLineAndStep();
      i := i+1;
  end;
  mrel(VectX,VectY,0);      // Each lines is double-exposed 
  mrel(-VectX,-VectY,0);
          
  LOff(5);
  mrel(-m*X, -m*Y,- n*stepH);
end;



{ Max Kellermeier, 22.08.2017, maxk@student.ethz.ch
    TODO: desc
    3D
    
    Parameters:
        plane_resolution: number of lines to form a plane in the xy-plane
        sign_x_dir: either +1 or -1, describes whether the current printing direction of the layer is along the positive or negative x-axis. The planes for the bulk between circles is is printed to the right of the circle in the first case, and to the left in the second case.
        location: Either the bulk etching is done on the bottom or on the top of the circle. For the first case the argument should be "bottom", in the second case "top"
        
}
procedure bulk_etching_between_bottom_half_cylinders_vertically(length: double; lattice_const, radius : double; halfaxis_a, halfaxis_b: double; angular_resolution: integer; sign_x_dir: integer; location : String );
var
    n_layers : integer;
    x_dist : double;
    y_dist : double;
    cur_angle : double;
    angular_step : double;
    i : integer;
    cur_plane_x_width: double;      // current width of the plane along the x direction
    cur_wall_height : double;
    
    function ellipse_s(phi: double) : double;
    begin ellipse_s := Arctan2(halfaxis_b* Sin(phi), halfaxis_a* Cos(phi) ) end;

    {x component of the ellipse's center. Expressed in terms of the angle to the contact point phi}
    function K_x(phi : double) : double ;
    begin K_x := halfaxis_a*Cos( ellipse_s(phi) ) + radius * Cos(phi) end;

    {y component of the ellipse's center. Expressed in terms of the angle to the contact point phi}
    function K_y(phi : double) : double ;
    begin K_y := halfaxis_b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;
    
begin
    // Logic structure
    // 1. Start from bottom of one circle
    // 2. Switch on laser
    // 3. step along circular arc in positive direction over [-pi/2, pi/2]
    // 4. Calculate the height of the wall between current angular point and bottom of circle 
    // 5. draw plane between the cylinders: pos. x, pos. y, neg. x, neg. y
    // 6. repeat until the top is reached
    // 7. Switch off laser
    // 8. Move to bottom
    // 9. Switch on laser
    //
    
    // make sure the beam is positioned correctly
    { CompareText(cad1, cad2) }
    if location = 'bottom' then
    begin
        cur_angle := -PI/2;
        angular_step := 0.5 *Pi/angular_resolution;
    end
    else if location = 'top' then
    begin
        mrel(sign_x_dir*(radius+halfaxis_a), 0, radius+ halfaxis_b );
        if sign_x_dir=1 then cur_angle := 0 else cur_angle:= pi;
        angular_step := 0.5 *Pi/angular_resolution;
    end;
    LOn(0);     // 2.
    
    i:= 0;
    // 3.
    While i< angular_resolution do
    begin
        // 4.
        cur_wall_height := (radius+halfaxis_b) - Abs(K_y(cur_angle));
        
       
        if location = 'bottom' then mrel(0,0,-cur_wall_height);
        verticalPlaneByStep(length,cur_wall_height, 0, 1, halfaxis_b);
        if location = 'bottom' then mrel(0,0,cur_wall_height);

    	cur_angle := step_angle_ellipse(cur_angle, halfaxis_a, halfaxis_b , radius,  sign_x_dir*angular_step); //2.
    	i:= i+1;
    end;
    cur_wall_height := (radius+halfaxis_b) - Abs(K_y(cur_angle));
    if location = 'bottom' then mrel(0,0,-cur_wall_height);
    verticalPlaneByStep(length,cur_wall_height, 0, 1, halfaxis_b);
    if location = 'bottom' then mrel(0,0,cur_wall_height);
    
    
    LOff(5);    //7.
    if location = 'bottom' then
    begin 
        mrel(0,0, - radius -  halfaxis_b);    // 8.    
        mrel(-sign_x_dir*(radius+halfaxis_a), 0,0 );
    end
    else if location = 'top' then
    begin 
        mrel(0,0, - 2*(radius +  halfaxis_b));    // 8.    
    end
end;



{ Draw a vertical plane, parallel to z. The second direction of the plane is specified as a parameter.
Max Kellermeier, maxk@student.ethz.ch, 22.08.2017

    TODO---------------
    
    It is assumed that the initial laser position is the bottom left corner. Of course this becomes the bottom right corner if the orientation is 180 degree. But it's important to keep in mind.
    The final plane is spanned by (orientationX, orientationY, 0) and (0,0,1).

    Parameters: 
        length: length of the plane in um. This is the length of the intersection line of this plane with the xy-plane
        height: height of the plane in the z-direction
        orientationX, orientationY: a 2d vector parallel to the plane. Only its direction is considered, not its magnitude. 
        vertical_res: number of lines in the vertical direction.
        
}
procedure verticalPlaneByStep(length, height : double; orientationX, orientationY: double; stepsize_z : double);
var
    sign : integer;     // used for specifying the current writing direction
    angle_in_xy : double;
    i : integer; // loop var
    residual_step : double;
    vertical_res : integer;
begin
    // angle of the plane w.r.t. the x-axis
    angle_in_xy := Arctan2(orientationY, orientationX );

    vertical_res := Floor(height/stepsize_z );
    residual_step :=height - stepsize_z*vertical_res;
   
    ChangePol( Round( radtodeg(angle_in_xy) ) + 90);
    // MotionStyle(True,True);
    //first line, back and forth
    LOn(0);
    line(length*cos(angle_in_xy), length*sin(angle_in_xy), 0);
    line(-length*cos(angle_in_xy), -length*sin(angle_in_xy), 0);
    
    //loop
    i := 0;
    While i < vertical_res do
    begin
        line(0,0,stepsize_z);
        line(length*cos(angle_in_xy), length*sin(angle_in_xy), 0);
        line(-length*cos(angle_in_xy), -length*sin(angle_in_xy), 0);
        i := i +1 ;
    end;
    mrel(0,0, residual_step);
    line(length*cos(angle_in_xy), length*sin(angle_in_xy), 0);
    line(-length*cos(angle_in_xy), -length*sin(angle_in_xy), 0);
    LOff(0);
    
    //move back to initial position
    mrel(0,0, -stepsize_z*vertical_res- residual_step);
end;

{ General TODOS: 
1. PMAC version of new "Circular_Hole_in_plane_filled"
2. PMAC Version of new "draw_Hexagonal_Wvg" if necessary
3. PMAC Version of halfcylinder: implementation and testing on machine
4. PMAC Version of halfCylinder_in_plane_w_bulk_etching: implementation and testing on machine
5. PMAC: Copy over the new replaced "Rect"
6. Testing Hexagonal_wvg_w_bulk_etching on machine
4. Checking function "rectangle_bounding_circle" on machine since it's not working properly

- "horizontalPlane_w_QVars" needs testing


TODOS Today:
1. Copy new RectRel Function
2. Copy Hexagonal_wvg_w_bulk_etching
  - halfcylinder
  - halfCylinder_in_plane_w_bulk_etching
  - RectRel
  - horizontalPlane
3. 

}


end.
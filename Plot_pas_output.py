'''
Plotting function taken from jupyter notebook 'Testing Pascal output'
Max Kellermeier
'''

import numpy as np
import io
import os.path
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.patches import Ellipse
import matplotlib
from matplotlib.collections import LineCollection
from mpl_toolkits.mplot3d.art3d import Line3DCollection

import sys


def set_axes_equal(ax):
    '''Make axes of 3D plot have equal scale so that spheres appear as spheres,
    cubes as cubes, etc..  This is one possible solution to Matplotlib's
    ax.set_aspect('equal') and ax.axis('equal') not working for 3D.

    Input
      ax: a matplotlib axis, e.g., as output from plt.gca().
    '''

    x_limits = ax.get_xlim3d()
    y_limits = ax.get_ylim3d()
    z_limits = ax.get_zlim3d()

    x_range = abs(x_limits[1] - x_limits[0])
    x_middle = np.mean(x_limits)
    y_range = abs(y_limits[1] - y_limits[0])
    y_middle = np.mean(y_limits)
    z_range = abs(z_limits[1] - z_limits[0])
    z_middle = np.mean(z_limits)

    # The plot bounding box is a sphere in the sense of the infinity
    # norm, hence I call half the max range the plot radius.
    plot_radius = 0.5*max([x_range, y_range, z_range])

    ax.set_xlim3d([x_middle - plot_radius, x_middle + plot_radius])
    ax.set_ylim3d([y_middle - plot_radius, y_middle + plot_radius])
    ax.set_zlim3d([z_middle - plot_radius, z_middle + plot_radius])

def plot_pas_output(filename, output_png_path=None, bool_2D = True ):
    points_to_plot = np.loadtxt(filename, delimiter=",", skiprows=1)
    #seperate points with laser on and laser off
    laser_switches_index= np.where(points_to_plot[:, 3] == 0 )[0]
    # 2d or 3d case?
    # if np.min(points_to_plot, axis=0)[1] == np.max(points_to_plot, axis=0)[1]:
    if bool_2D:
        # fig_pas = plt.figure(figsize= 5* plt.figaspect(1))
        fig_pas = plt.figure(figsize= plt.figaspect(1))
        ax= fig_pas.add_subplot(111)
        # drawing ellipses at the additionally
        a=1
        b=4
        ells = [Ellipse(xy=point, width=2*a, height=2*b, zorder=2)
            for point in points_to_plot[:,np.array([0,2])]]
        for e in ells:
            ax.add_artist(e)
            e.set_clip_box(ax.bbox)
            e.set_alpha(0.5)
        # draw points
        # ----
        # ax.plot(points_to_plot[:,0], points_to_plot[:,2] , "r.", zorder=3)
        # ax.set_xlim((-400, 400))
        # ax.set_ylim((-1100, 50))
        
        # draw lines where the laser status is on
        segments= np.array([[[0,0],[0,0]]])
        for i,j in zip(laser_switches_index[:-1], laser_switches_index[1:]) :
            line_points=points_to_plot[i+1:j, np.array([0,2])].reshape(-1,1,2)
            segments_one_circle = np.concatenate((line_points[:-1], line_points[1:]), axis=1)
            segments = np.concatenate( (segments, segments_one_circle), axis=0 )
        lc=LineCollection(segments)
        fig_pas.gca().add_collection(lc)
        # ax.set_aspect('equal', adjustable='box')
        ax.set_aspect('equal', adjustable='datalim')
        ax.set_xlabel("x")
        ax.set_ylabel("z")
        # ax.set_xticks([])
        # ax.set_yticks([])
        # ax.set_xticklabels([])
        # ax.set_yticklabels([])
        # ax.set_ylim( (ax.get_ylim()[0] - 3, 1.08*ax.get_ylim()[1] ))
        # ax.set_ylim((-120,120))
        # ax.set_xlim((-550,0))
        ax.set_ylim((-20, 120))
        ax.set_xlim((-70, 70))
        
        ax.grid(True)
        ax.set_aspect('equal', adjustable='box')
    else:
        fig3D_hexagon = plt.figure()
        ax = fig3D_hexagon.add_subplot(111, projection='3d')
        # ax.scatter(points_to_plot[:,0], points_to_plot[:,1], points_to_plot[:,2], c='r', marker='o', zorder=2)
        # set_axes_equal(ax)
        # plot lines where the laser status is on
        # https://stackoverflow.com/questions/22078256/matplotlib-line3dcollection-for-time-varying-colors
        segments= np.array([[[0,0,0],[0,0,0]]])
        for i,j in zip(laser_switches_index[:-1], laser_switches_index[1:]) :
            line_points=points_to_plot[i+1:j, 0:3].reshape(-1,1,3)
            segments_one_circle = np.concatenate((line_points[:-1], line_points[1:]), axis=1)
            segments = np.concatenate( (segments, segments_one_circle), axis=0 )
        ax.add_collection(Line3DCollection(segments) )
        # ax.scatter(points_to_plot[:,0], points_to_plot[:,1], points_to_plot[:,2], c='r', marker='o', zorder=2)
        ax.set_xlim(-400, 200)
        ax.set_ylim(0, 600)
        ax.set_zlim(-100,500)
        set_axes_equal(ax)
        
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        ax.set_zlabel("z")
        # fig3D_hexagon.show()
    if not output_png_path is None:
        plt.savefig(output_png_path)
    else:
        plt.show()
    
if __name__=='__main__':
    plt.ioff()
    # matplotlib.interactive(True)
    import argparse
    parser = argparse.ArgumentParser('test_argparse.py')
    parser.add_argument(
                'document', nargs='?', default=None,
                help='document file which should be extracted')
    parser.add_argument(
                '-3d', '--3d', dest='enable_3d', action='store_const',
                const=True, default=False,
                help='Switch to the 3D plot. By default a 2D plot is shown.')
    parser.add_argument(
                '-sp', '--savepng', dest='path_to_png', default=None,
                help='Directly writing the figure to a file, specified by the path. by default, the figure is displayed but not saved.')

    args=parser.parse_args()
    print(args.enable_3d)

    if not args.document is None:
        # do something with the file
        try:
            plot_pas_output(args.document, args.path_to_png, not args.enable_3d)
        # except IOError:
        except:
            print("Either wrong file format or file does not exists.")
        # os.path.isfile(sys.argv[1])
    else:
        lines = []
        while True:
            try:
                line = input()
                lines.append(line)
            except EOFError:
                break
        text = '\n'.join(lines)
        # print(text)
        cylinder_values= io.StringIO(text)
        plot_pas_output(cylinder_values, args.path_to_png, not args.enable_3d)
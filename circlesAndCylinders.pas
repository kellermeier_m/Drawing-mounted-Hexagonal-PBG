// Library of circle and cylinder functions perpendicular to the laser axis
// 20.08.2018
// Max Kellermeier
{$mode objfpc}

unit circlesAndCylinders;
interface
uses sysutils, math, helperTLFS;          // exponent operator **
    // 2D
    procedure Circle_in_plane(radius, halfaxis_a, halfaxis_b : double; angular_res: integer);
    procedure Circular_Hole_in_plane(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; startFromTop : boolean = False; initialLOn : boolean = False );
    // TODO:
    // procedure Circular_Hole_in_plane_filled(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; startFromTo : boolean = False; initialLOn : boolean = False);
    
	// 3D
	procedure cylinder(length, radius, a_halfaxis, halfaxis_b : double; angular_res,  y_resolution : integer);
    procedure cylinder2(length, radius, halfaxis_a, halfaxis_b : double; angular_res : integer);
    procedure halfcylinder(length, radius, halfaxis_a, halfaxis_b : double; angular_res : integer; circleSide : integer);
	procedure Cylindrical_Hole_in_plane_filled(length, radius, halfaxis_a, halfaxis_b : double; angular_res: integer; step_size_x: double);
    
	// Bulk etching strategies
    procedure halfCylinder_in_plane_w_bulk_etching(length: double; lattice_const, radius : double; halfaxis_a, halfaxis_b: double; angular_resolution: integer; plane_resolution : integer; sign_x_dir: integer);
    {-----}
    procedure rectangle_bounding_circle(radius, halfaxis_a, halfaxis_b : double; angular_res: integer; startFromTop : boolean; initialLOn : boolean);
    procedure cylinder2_w_boundary_etching(length, radius, halfaxis_a, halfaxis_b : double; angular_res : integer);
    procedure bulk_etching_between_bottom_half_cylinders(length: double; lattice_const, radius : double; halfaxis_a, halfaxis_b: double; angular_resolution: integer; plane_resolution : integer; sign_x_dir: integer;  location : integer );
    procedure bulk_etching_between_bottom_half_cylinders_vertically(length: double; lattice_const, radius : double; halfaxis_a, halfaxis_b: double; angular_resolution: integer; sign_x_dir: integer;  location : String );
        
implementation
{ Max Kellermeier, 21.07.2017,
    Desc. needed
    Sets the variables Q215-Q227
}
procedure initializeQVariable(halfaxis_a, halfaxis_b, radius : double; angular_res: integer);
begin
    with PMAC_Buffer do
    begin
      Add('Q215='+ FloatToStr(halfaxis_a));   {Q215: halfaxis a of the writing ellipse}
      Add('Q216='+ FloatToStr(halfaxis_b));   {Q216: halfaxis b of the writing ellipse}
      Add('Q217='+ FloatToStr(radius));       {Q217: radius of the resulting, written circle}
      Add('Q218='+ FloatToStr(Pi/angular_res)); {Q218: angular step size, depends on resolution}
      Add('Q219='+ FloatToStr(-PI/2));        {Q219: current angular position w.r.r. circle center, changed in each step}
      Add('Q220=0');                          {Q220: ellipse parameter s, read at the beginning of the secion}
      Add('Q221=0');                          {Q221: x comp. of new ellipse center w.r.t. circle center}
      Add('Q222=0');                          {Q221: y comp. of new ellipse center w.r.t. circle center}
      Add('Q223=0');                          {Q223: x comp. of previous ellipse center w.r.t. circle center, called Kx}
      Add('Q224=0');                          {Q224: y comp. of previous ellipse center w.r.t. circle center, called Ky}
      Add('Q225=0');                          {Q225: Dummy variable to store a value temporarily in a different variable }
      Add('Q226='+ FloatToStr(Z_correct));    {Q226: Correction factor for the material when moving in z-direction}
      Add('Q227=1;');                         {Q227: sign, specifying whether on pos. or neg. side along the x-axis}   
    end;
end;

procedure TLFS_Converter.Circle_in_plane(radius, halfaxis_a, halfaxis_b : double; angular_res: integer);
var
    angular_step : double;
    cur_angle : double;
    i : integer;
    z_fused_silica : double;
begin
    {procedure body}
    // Logic structure
    // 1. place ellipse at bottom, so at (0, -2)*radius
    // 2. Switch on laser
    // 3. step along the full circular arc counterclockwise
    // 4. repeat steps from [-pi/2, pi/2] (inclusively) via phi=0
    // 5. switch off laser
    // 6. place ellipse at bottom
    // 7. switch on laser
    // 8. step along circular arc clockwise
    // 9. repeat steps over [pi 3/2, pi/2[ (exclusively)
    // 10. Stop at point in front of top point
    // 11. Switch off laser
    // 12. Move to top point, initial position
	MotionStyle(True, True);  { Motion is set to linear relative }
    with PMAC_Buffer do
    begin
      initializeQVariable(halfaxis_a, halfaxis_b, radius, angular_res);
	  Add('I15=1'); {Switch from degree to radian}
	  // 1.
	  add('X0 Y0 Z(2*Q226*(Q216+Q217) );');     
      // 2.
      LOn(0);

      Add('Q0=Q215*COS(Q219)');  {Used for ATAN2}
      Add('Q220=ATAN2(Q216*SIN(Q219))');         {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
      Add('Q223=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
      Add('Q224=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
      Add('P223=Q223');
      Add('P224=Q224');
      // 4.
      Add('Q211=0'); 
      Add('WHILE(Q211<'+IntToStr(angular_res)+')');
        Add('Q225=Q219+Q218');
        Add('Q219=Q225');
        // Set new ellipse parameter, s = Arctan2(b* Sin(phi), a* Cos(phi) )
        Add('Q0=Q215*COS(Q219)');
        Add('Q220=ATAN2(Q216*SIN(Q219))');
        Add('Q221=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
        Add('Q222=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
        // 3.
        add('X(Q227*(Q221-Q223)) Y0 Z(-Q226*(Q222-Q224));');        
        Add('Q223=Q221'); {set new (Kx,Ky) to old (Kx, Ky)}
        Add('Q224=Q222');
        Add('P223=Q223');
        Add('P224=Q224');
        Add('Q211=Q211+1;'); { increment counter }
      Add('ENDWHILE;'); { end while loop }
      // 5.
      LOff(0);
      add('X0 Y0 Z(2*Q226*(Q216+Q217) );');
      // 7.
      LOn(0);
      cur_angle := -PI/2;
      angular_step := Pi/angular_res;
      
	  initializeQVariable(halfaxis_a, halfaxis_b, radius, angular_res);
	  Add('Q227=-1;');                         {Q227: sign, neg. side along the x-axis}
      Add('Q0=Q215*COS(Q219)');
      Add('Q220=ATAN2(Q216*SIN(Q219))');
      // Set Kx and Ky to initial value
      Add('Q223=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
      Add('Q224=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
      Add('P223=Q223');
      Add('P224=Q224');
     // 9.
      Add('Q211=0');
      Add('WHILE(Q211<'+IntToStr(angular_res)+')');
        Add('Q225=Q219+Q218');
        Add('Q219=Q225');
        Add('Q0=Q215*COS(Q219)');
        Add('Q220=ATAN2(Q216*SIN(Q219))');
        Add('Q221=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
        Add('Q222=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
        // 8.
        add('X(Q227*(Q221-Q223)) Y0 Z(-Q226*(Q222-Q224));');        
	    Add('Q223=Q221');
        Add('Q224=Q222');
        Add('P223=Q223');
        Add('P224=Q224');
        Add('Q211=Q211+1;'); { increment counter }
     // 10.
     Add('ENDWHILE;'); { end while loop }
    end;
    // 11.
    LOff(5);
end;


{
  Like the function 'Circle in plane' but instead of cutting a circle of glass embedded in 'air', a circle of air embedded in glass is drawn.
  The parameters are the same as in the 'circle_in_plane' function.

  TODO: Since the two functions only differ by a sign the should be merged into a single functtion with a parameter switching between both.
}
procedure TLFS_Converter.Circular_hole_in_plane(radius, halfaxis_a, halfaxis_b : double; angular_res: integer);
var
    angular_step : double;  //
    cur_angle : double;
    i : integer;    // loop variable
    z_fused_silica : double;
begin
    {procedure body}
    // Logic structure
    // 1. place ellipse at bottom, so at (0, -2)*radius
    // 2. Switch on laser
    // 3. step along the full circular arc counterclockwise
    // 4. repeat steps from [-pi/2, pi/2] (inclusively) via phi=0
    // 5. switch off laser
    // 6. place ellipse at bottom
    // 7. switch on laser
    // 8. step along circular arc clockwise
    // 9. repeat steps over [pi 3/2, pi/2[ (exclusively)
    // 10. Stop at point in front of top point
    // 11. Switch off laser
    // 12. Move to top point, initial position
	MotionStyle(True, True);  { Motion is set to linear relative }
	
    with PMAC_Buffer do
    begin
	  Add('I15=1'); {Switch from degree to radian}
	  initializeQVariable(halfaxis_a, halfaxis_b, radius, angular_res);
      
      add('X0 Y0 Z(2*Q226*Q217 );');
      // 2.
      LOn(0);

      Add('Q0=Q215*COS(Q219)');
      Add('Q220=ATAN2(Q216*SIN(Q219))');         {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
      // Set Kx and Ky to initial value
      Add('Q223=-Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := -a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
      Add('Q224=-Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := -b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
      Add('P223=Q223');
      Add('P224=Q224');

      // 4.
      Add('Q211=0'); { Q211 is an Iterative variable }
      Add('WHILE(Q211<'+IntToStr(angular_res)+')');
        Add('Q225=Q219+Q218');
        Add('Q219=Q225');
        // Set new ellipse parameter, s = Arctan2(b* Sin(phi), a* Cos(phi) )
        Add('Q0=Q215*COS(Q219)');
        Add('Q220=ATAN2(Q216*SIN(Q219))');
        Add('Q221=-Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := -a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
        Add('Q222=-Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := -b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
        // 3.
        add('X(Q227*(Q221-Q223)) Y0 Z(-Q226*(Q222-Q224));');        {Q226 is the correction due to material effect}
        Add('Q223=Q221');
        Add('Q224=Q222');
        Add('P223=Q223');
        Add('P224=Q224');
        Add('Q211=Q211+1;'); { increment counter }
      Add('ENDWHILE;'); { end while loop }

      LOff(0);
      add('X0 Y0 Z(2*Q226*(Q217-Q216) );');       {Q226 is the correction due to material effect}
      LOn(0);
	  initializeQVariable(halfaxis_a, halfaxis_b, radius, angular_res);
      Add('Q227=-1;');                         {Q227: sign, neg. side along the x-axis}
	 
      Add('Q0=Q215*COS(Q219)');
      Add('Q220=ATAN2(Q216*SIN(Q219))');
      Add('Q223=-Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := -a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
      Add('Q224=-Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := -b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
      Add('P223=Q223');
      Add('P224=Q224');
     // 9.
      Add('Q211=0');
      Add('WHILE(Q211<'+IntToStr(angular_res)+')');
        Add('Q225=Q219+Q218');
        Add('Q219=Q225');
        Add('Q0=Q215*COS(Q219)');
        Add('Q220=ATAN2(Q216*SIN(Q219))');
        Add('Q221=-Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := -a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
        Add('Q222=-Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := -b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
        // 8.
        add('X(Q227*(Q221-Q223)) Y0 Z(-Q226*(Q222-Q224));');       {Q226 is the correction due to material effect}
        Add('Q223=Q221');
        Add('Q224=Q222');
        Add('P223=Q223');
        Add('P224=Q224');
        Add('Q211=Q211+1;'); { increment counter }
     // 10.
     Add('ENDWHILE;'); { end while loop }
    end;
    // 11.
    LOff(5);
    mrel(0,0, 2* halfaxis_b);
end;


procedure TLFS_Converter.cylinder(length, radius, a_halfaxis, halfaxis_b : double;
                                  angular_res,  y_resolution : integer);
var
    step_y: double;
begin
    MotionStyle(True, True);
    step_y := length/y_resolution;
    PMAC_Buffer.Add('Q210=0'); { Q210 is an Iterative variable }
    with PMAC_Buffer do
    begin
     Add('WHILE(Q210<'+IntToStr(y_resolution+1)+')');
     Circle_in_plane(radius, a_halfaxis, halfaxis_b, angular_res);
     mrel(0, step_y, 0);
     Add('Q210=Q210+1;'); { increment counter }
     Add('ENDWHILE;'); { end while loop }
    end;
    mrel(0,-(y_resolution+1)*step_y, 0);
end;

procedure TLFS_Converter.cylinder2(length, radius, halfaxis_a, halfaxis_b : double;
    angular_res: integer);
var
    sign_y_side: double;
    cur_angle : double;
    angular_step : double;
begin
    // Logic structure
    // 1. Initialize Q-Variables. Those initialized with 0 are dynamic variables, meaning that they change during the execution of the function
    // 2. Switch on laser
    // 3. Set the dynamic Q-Variables to their initial values
    // 4. Start loop for stepping over [-pi/2, pi/2] in positive direction until the top point is reached. Depending whether the angular resolution is odd or even the laser is positioned on the initial side of the cylinder or the opposite.
    // In the loop:
        // 5. draw a line along y-direction
        // 6. Calculate new laser position on the circle
        // 7. Move laser by difference of new and old laser point
        // 8. Set new position to old position
    // 9. Switch off Laser, move to bottom, switch on again
    // 10. Set the dynamic Q-Variables to their initial values for second loop
    // 11. Start looping over [-pi/2, pi/2] in negative direction until top point is reached
    // In the loop:
        // 12. Calculate new laser position on the circle
        // 13. Move laser by difference of new and old laser point
        // 14. Set new position to old position
        // 15. draw a line along y-direction
    // 16. Switch off laser
    // (17. ensure that the laser is positioned at the end and at the bottom of the cylinder) TBD
    MotionStyle(True, True);
    with PMAC_Buffer do
    begin
      initializeQVariable(halfaxis_a, halfaxis_b, radius, angular_res);
      Add('Q232=1'); {Q232: sign, specifying whether on pos. or neg. side along the y-axis}
      Add('I15=1'); {Switch from degree to radian}
      
      //2.
      LOn(5);
      //3.
      Add('Q0=Q215*COS(Q219)');
      Add('Q220=ATAN2(Q216*SIN(Q219))');         {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
      Add('Q223=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
      Add('Q224=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
      Add('P223=Q223');
      Add('P224=Q224');

      // 4.
      Add('Q211=0'); { Q211 is an Iterative variable }
      Add('WHILE(Q211<'+IntToStr(angular_res)+')');
        //5.
        Add('X0 Y(Q232*'+FloatToStr(length)+') Z0' );
        // 6.  new angle position: current_pos + step
        Add('Q225=Q219+Q218');
        Add('Q219=Q225');
        // Set new ellipse parameter, s = Arctan2(b* Sin(phi), a* Cos(phi) )
        Add('Q0=Q215*COS(Q219)');
        Add('Q220=ATAN2(Q216*SIN(Q219))');
        // Calculate new (Kx,Ky) values for new angle position
        Add('Q221=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
        Add('Q222=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
        // 7.
        MotionStyle(True, True);  { Motion is set to linear relative }
        add('X(Q227*(Q221-Q223)) Y0 Z(-Q226*(Q222-Q224));');        {Q226 is the correction due to material effect}
        // 8. set new (Kx,Ky) to old (Kx, Ky)
        Add('Q223=Q221');
        Add('Q224=Q222');
        Add('Q211=Q211+1;'); { increment counter }
        Add('Q232=-Q232');  {switch direction of writing along y}
      Add('ENDWHILE;'); { end while loop }

      // 9.
      LOff(0);
      add('X0 Y0 Z(2*Q226*(Q216+Q217) );');
      LOn(0);

      // 10.
      initializeQVariable(halfaxis_a, halfaxis_b, radius, angular_res);
      Add('Q227=-1');
      // Set initial ellipse parameter
      Add('Q0=Q215*COS(Q219)');
      Add('Q220=ATAN2(Q216*SIN(Q219))');
      Add('Q223=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
      Add('Q224=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
     // 11.
      Add('Q211=0'); { Q211 is an Iterative variable }
      Add('WHILE(Q211<'+IntToStr(angular_res)+')');
        //12.
        // new angle position: current_pos + step
        Add('Q225=Q219+Q218');
        Add('Q219=Q225');
        // Set new ellipse parameter
        Add('Q0=Q215*COS(Q219)');
        Add('Q220=ATAN2(Q216*SIN(Q219))');      {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
        // Calculate new (Kx,Ky) values for new angle position
        Add('Q221=Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
        Add('Q222=Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}

        // 13.
        add('X(Q227*(Q221-Q223)) Y0 Z(-Q226*(Q222-Q224));');       {Q226 is the correction due to material effect}
        // 14.
        Add('Q223=Q221');
        Add('Q224=Q222');
        // 15.
        Add('X0 Y(Q232*'+FloatToStr(length)+') Z0' );
        Add('Q211=Q211+1;'); { increment counter }
        Add('Q232=-Q232');  {switch direction of writing along y}
    Add('ENDWHILE;'); { end while loop }
    end;
    // 16.
    LOff(5);
    // 17.
    mrel(0,0, - 2 * radius - 2* halfaxis_b);    // move to bottom
end;

{ Max Kellermeier, 21.7.2017
    This function is based on "Circular_Hole_in_plane_filled". While "Circular_Hole_in_plane_filled" writes a two-dimensional object, the circle, this function creates a cylinder, including the third dimension right from the beginning.
    The parameters are similar to those from "cylinder2" since this is basically a cylinder of air instead of glass.

    Assumptions:
        The initial position is at the bottom of the resulting cylinder.
        The laser is off before start.

    Parameters:
        ...
        plane_res: resolution of the plane along the x-axis, the stepping direction in the xy-plane. If the resolution is too low the result will be a lattice of parallel lines along the y-axis

    Q211, Q212: loop variables
    Q228: Number of steps along x for the plane in xy at a fixed height (Maybe changed to M49)
    Q229: Residual to step to achieve the plane width after stepping by step_size_x. Used at a fixed angle.
        -> plane_width = Q228*step_size_x + Q229

}
procedure TLFS_Converter.Cylindrical_Hole_in_plane_filled(length, radius, halfaxis_a, halfaxis_b : double; angular_res : integer; step_size_x: double);
begin
{procedure body}
    {--- TODO ---}
    // Logic structure
    // 1. Initially, the spot is positioned outside the circle. Move it inside
    // 2. Switch on laser
    // 3. step along the full circular arc counterclockwise
    // 4. repeat steps from [-pi/2, pi/2] (inclusively) via phi=0
    // 5. switch off laser
    // 6. place ellipse at bottom
    // 7. switch on laser
    // 8. step along circular arc clockwise
    // 9. repeat steps over [pi 3/2, pi/2[ (exclusively)
    // 10. Stop at point in front of top point
    // 11. Switch off laser
    // 12. Move to top point, initial position

    MotionStyle(True, True);  { Motion is set to linear relative }
    
    with PMAC_Buffer do
    begin
      Add('I15=1'); {Switch from degree to radian}
      initializeQVariable(halfaxis_a, halfaxis_b, radius, angular_res);
      Add('Q228=0;');        {plane_res: number of points to step along x of step width 'step_size_x' to construct the plane in xy}
      Add('Q229=0;');        {residual step to achieve the requested plane width}
      
      mrel(0,0, +2*halfaxis_b);
      LOn(5);
      
      Add('X0 Y'+ FloatToStr(length) +  ' Z0;');
      Add('X0 Y(-'+ FloatToStr(length) +  ') Z0;');
      // Set initial ellipse parameter
      Add('Q0=Q215*COS(Q219)');
      Add('Q220=ATAN2(Q216*SIN(Q219))');         {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
      Add('Q223=-Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
      Add('Q224=-Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
      Add('Q225=Q219+Q218');
      Add('Q219=Q225');             {cur_angle := cur_angle + angular_step}
      Add('Q0=Q215*COS(Q219)');
      Add('Q220=ATAN2(Q216*SIN(Q219))');         {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
      // Set new Kx and Ky to initial value
      Add('Q221=-Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x_cur := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
      Add('Q222=-Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y_cur := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) end;}
      Add('X(Q227*(Q221-Q223)) Y0 Z(-Q226*(Q222-Q224));');        {mrel(sign*(K_x_cur - K_x_old), 0, K_y_cur - K_y_old );}
      Add('Q227=-Q227;') ;                                {sign := -sign}


      // 4.
      Add('Q211=1'); { Q211 is an Iterative variable }
      Add('WHILE(Q211<'+IntToStr(angular_res)+')');
        Add('Q229=(2*Q221)%' + FormatFloat('0.000', step_size_x) + ';');
        // Add('Q228=FTOI(2*Q229/'+ FormatFloat('0.000', step_size_x) + ');');
        Add('M49=(2*Q221)/'+ FormatFloat('0.000', step_size_x)+';');
        Add('IF(Q229!< ('+ FormatFloat('0.000', step_size_x)+'/2) )');
        //Add('Q228=Q228-1;')
        Add('M49=M49-1;');
        Add('ENDIF;');

        { horizontalPlane(2* K_x_cur, length, plane_res, -sign ); }
        { -for horizontalPlane- }
        //first line
        Add('X0 Y('+ FloatToStr(length) +  ') Z0;');
        Add('X0 Y(-'+ FloatToStr(length) +  ') Z0;');
        Add('Q212=0;');
        // Add('WHILE(Q212<Q228)');
        Add('WHILE(Q212<M49)');
        { RepeatWhile(plane_res); }
            // stepsize_x := -sign *2* K_x_cur/resolution_x
            Add('X(Q227*'+ FormatFloat('0.000', step_size_x) +' ) Y0 Z0;');
            { mrel(stepsize_x, 0, 0); }
            Add('X0 Y'+ FloatToStr(length) +  ' Z0;');
            Add('X0 Y(-'+ FloatToStr(length) +  ') Z0;');
            Add('Q212=Q212+1;');
        Add('ENDWHILE;');
        Add('X(Q227*Q229) Y0 Z0;');
        Add('X0 Y'+ FloatToStr(length) +  ' Z0;');
        Add('X0 Y(-'+ FloatToStr(length) +  ') Z0;');
        { -end horizontalPlane- }

        // set new (Kx,Ky) to old (Kx, Ky)
        Add('Q223=Q221');
        Add('Q224=Q222');

        Add('Q225=Q219+Q218');                              { cur_angle := cur_angle + angular_step; }
        Add('Q219=Q225');
        // Set new ellipse parameter
        Add('Q0=Q215*COS(Q219)');
        Add('Q220=ATAN2(Q216*SIN(Q219))');                  {s = Arctan2(b* Sin(phi), a* Cos(phi) )}
        // Calculate new (Kx,Ky) values for new angle position
        Add('Q221=-Q215*COS(Q220) + Q217*COS(Q219) ');       { K_x := a*Cos( ellipse_s(phi) ) + radius * Cos(phi) }
        Add('Q222=-Q216*SIN(Q220) + Q217*SIN(Q219) ');       { K_y := b*Sin( ellipse_s(phi) ) + radius * Sin(phi) }

        // 3.
        Add('X(Q227*(Q221-Q223)) Y0 Z(-Q226*(Q222-Q224));');        {mrel(sign*(K_x_cur - K_x_old), 0, K_y_cur - K_y_old );}
        Add('Q211=Q211+1;'); { increment counter }
        Add('Q227=-Q227;') ;                                {sign := -sign}

      Add('ENDWHILE;'); { end while loop }
      Add('X0 Y'+ FloatToStr(length) +  ' Z0;');
      Add('X0 Y(-'+ FloatToStr(length) +  ') Z0;');
      // 5.
      LOff(0);
      mrel(0,0, +2*halfaxis_b);
      mrel(0,0, -2*(radius+halfaxis_b));
    end;
end;


end.
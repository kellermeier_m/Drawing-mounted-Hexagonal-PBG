DEPRECATED
===

Scripts for Drawing the 2D PBG Waveguide in glass with direct laser writing
===
The laser is controlled by a program written in Delphi Pascal. When running it reads in a LFS file which describes what should be drawn in which order and with which parameters. The LFS file is interpreted by the Pascal program and converted to machine code for the laser controller, a PMAC motion controller by Delta Tau Systems.
To define new geometries a make them available via LFs they have to be implemented in the Pascal program directly. For example in LFS a circle can be drawn by
```
FlexCircle 40, 1000, 360 ;   //Diameter, Speed, iterations
```
When the controller reads this file, it calls a function `FlexCircle` with those parameters which generates the machine code.

The *FlexCircle* is a circle in the plane perpendicular to the laser beam direction. For a cylinder perpendicular to the beam axis a circle parallel to the beam axis is needed. 

1. A new Pascal function for the controller program will be able to write circles in the plane of the beam axis. 
2. This circle is used to write a cylinder.
3. Multiple parallel cylinders are arranged in a hexagonal manner to form a photonic band gap structure.
4. An additional hole for the waveguide is needed.

It has to be taken into account that a geometry function in pascal only outputs the PMAC commands. This means that a loop will be unrolled in the result. For efficiency it is better to output the PMAC Commands which describe a loop on machine code basis. As a first step Pascal loop are used.

Helper functions
---
The pascal functions are created outside the controller program. The Controller uses functions like `mrel(x,y,z)` to change the relative position of the beam by the given arguments. To test and debug the semantics of the drawing functions those control functions are implemented as helpers which output the hypothetical current beam position.

Those helpers are used:
- `mrel(x,y,z)`
- `LOn(s)`
- `LOff(s)`

Circle in plane
---
In the following the beam axis is denoted with **z** while **x** and **y** are the transverse components. The focus of the laser has a elliptical shape, rotationally symmetric around the beam axis. Therefore the shape is described by an ellipse halfaxis along the z-axis, and a halfaxis in the transverse plane. 
- **a**: halfaxis of the ellipse extent in the transverse plane, the x-y-plane
- **b**: longitudinal halfaxis along the z-axis

The circle has to be cut with such an ellipse in the x-z-plane.

The mathematical description is done in a separate file. The function *Circle_in_plane* to draw such a circle is implemented in [Circle_in_plane.pas](../blob/master/Circle_in_plane.pas).

Perpdenicular Cylinder
---
The resulting cylinder is oriented along the y direction. It is constructed from discrete circles which should be as close as possible to have a smooth surface. The implementation is done in [Cylinder-w-circles.pas](../blob/master/Cylinder-w-circles.pas).
For shorter PMAC output a second version using PMAC loops is implemented in [Cylinder-w-circles_PMAC_Loops.pas](../blob/master/Cylinder-w-circles_PMAC_Loops.pas)

Hexagonal lattice with waveguide hole in 2D
---
After being able to draw a cylinder the arrangement of the cylinders is considered in two dimensions which is a hexagonal cut from a trigonal lattice, as shown in the following 
![Hexagon](img/eps-finite-2D.png)

As a first step an ocject oriented approach is considered. The implementation was done in [class_hexagonal_wvg.pas](../blob/master/class_hexagonal_wvg.pas). Since all geometric objects in the controller program are defined procedurally the object oriented code was translated to a function based code.
The beam is initially positioned at the surface of the glass substrate. The focus is moved to the bottom left/right corner and the circles are drawn layer by layer.

The following figure depicts the position movements of the laser for drawing such a honeycomb like cross section. For better clearness the resolution of the circle arc was set very low. The single dot at the top and the center are the last two positions of the focus but at those points the laser is swithced off already. Here, the hole for the waveguide is still missing.
<img src="img/fabrication_steps_2D.png" alt="Laser positions for a Hexagon" width="300">

Mounted Hexagon of Cylinders
---
TODO

Testing of the functions
===
TODO: NOT VALID ANYMORE

All pascal snippets are tested using the helper functions in the online pascal Interpreter [ideone.com](https://ideone.com).
- (https://ideone.com/2FW6nY)[https://ideone.com/2FW6nY]: Cylinder based on circles cutted from an elliptical focal points
- (https://ideone.com/jCRF9m)[https://ideone.com/jCRF9m]: Transverse Hexagon with positions of the ellipse to cut cicles


Visualization of Testing output
---
The test scripts print only the positions to the console. Plotting is done with matplotlib in a jupyter notebook.



Branches
===
 - *air-cylinder*: The hollow cylinder - aligned along the y-axis - is reimplemented in such a way that the number of lines per plane changes accorging to its size. Before, the number of lines per plane was given as a fixed parameter to the function. Now, the step size along x in such a plane is given such that all steps but the last one have the same size. The last step corrects for the remainder of the rounding.
- *cut-from-bulk*: For testing purposes three rods shall be written and accessed from the top of cylinders. Since the rods are burried within the glass they have to be cut out of the sample. They have to remain on a smaller sample but cut in such a way that the rods are accessible from the side.
  - Step 1: Correct the function 'verticalPlane' for polarisation. The polarisation  is always given as an angle with the x-axis as a reference.
  - Step 2: Write lfs section for the "cut cuboid" resp. start with pascal code
  - Step 3: Put section for cuboid and section for the three rods together
  
  













